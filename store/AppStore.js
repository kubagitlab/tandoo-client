import {action, computed, observable, reaction} from 'mobx';
import i18n from '../translate/i18n';
import storage from './LocalStorage';
import commonApi from '../apis/common';
import constants from '../common/constants';

let busy_counter = observable.box(0);

class AppStore {
  token;

  locales = [
    {code: 'kg', name: 'Kyrgyz'},
    {code: 'ru', name: 'Russian'},
    {code: 'en', name: 'English'},
  ];

  @observable locale = 'ru';

  @observable level = '';
  @observable message = '';
  @observable user;
  @observable company;

  @computed
  get isBusy() {
    return busy_counter.get() > 0;
  }

  @computed
  get isSupplier() {
    return !!(this.user && this.user.role_id === constants.ROLE_SUPPLIER);
  }

  @computed
  get isPurchaser() {
    return !!(this.user && this.user.role_id === constants.ROLE_PURCHASER);
  }

  @computed
  get isAdmin() {
    return !!(this.user && [constants.ROLE_ADMIN, constants.ROLE_SUPER_ADMIN, constants.ROLE_OPERATOR].includes(this.user.role_id));
  }

  @computed
  get isAdminOperator() {
    return !!(this.user && [constants.ROLE_OPERATOR].includes(this.user.role_id));
  }

  @computed
  get isAdminSuper() {
    return !!(this.user && [constants.ROLE_SUPER_ADMIN].includes(this.user.role_id));
  }

  @action
  setLocale(locale) {
    this.locale = locale;
  }

  @action
  setBusy(incr) {
    let count = busy_counter.get();
    if (!incr) {
      count = count > 0 ? count - 1 : 0;
    } else {
      count = count + 1;
    }
    busy_counter.set(count);
  }

  @action
  setToken(token) {
    this.token = token;
    if (token) {
      storage.save('token', token);
    } else {
      storage.remove('token');
    }
  }

  @action
  setUser(user) {
    this.user = user;
    if (user) {
      storage.save('user', user);
    } else {
      storage.remove('user');
    }
  }

  @action
  setCompany(company) {
    this.company = company;
    if (company) {
      storage.save('company', company);
    } else {
      storage.remove('company');
    }
  }

  @action
  async loadToken() {
    if (this.token) {
      return this.token;
    }
    const token = storage.get('token');
    this.token = token;
    return token;
  }

  @action
  async loadData() {
    await this.loadToken();
    await this.getUserInfo();
  }

  @action
  async getUserInfo() {
    if (this.token) {
      const r = await commonApi.getUserInfo();
      this.user = r.user;
    }
  }

  @action
  logout() {
    this.token = null;
    this.user = null;
    this.company = null;
    storage.clear();
    this.clearAlert();
    // commonApi.logout();
  }

  @action
  async login(username, password) {
    const response = await commonApi.login({username, password});
    this.setToken(response.token);
    this.setUser(response.user);
    return response;
  }

  @action
  async ecpLogin(token) {
    const response = await commonApi.ecpLogin(token);
    this.setToken(response.token);
    this.setUser(response.user);
    return response;
  }

  async keycloakLogin(token) {
    try {
      const response = await commonApi.keycloakLogin(token);
      this.setToken(response.token);
      this.setUser(response.user);
      this.setCompany(response.company);
      return response;
    } catch (e) {
      return Promise.reject(e);
    }
  }

  @action
  async register(data) {

  }

  @action
  clearAlert() {
    this.level = '';
    this.message = '';
  }

  @action
  setAlert(level, message) {
    this.level = level;
    this.message = message;
  }

  @action
  async loadUser() {
    this.user = storage.get('user');
    await this.loadToken();
    if (this.token) {
      try {
        const r = await commonApi.getUserInfo();
        const user = r.user;
        this.setUser(user);
      } catch (e) {
      }
    }
  }
}

const appStore = new AppStore();
reaction(
  () => appStore.locale,
  locale => {
    i18n.changeLanguage(locale);
    storage.save('locale', locale);
  },
);
export default appStore;
