import numeral from "numeral";
import moment from "moment";

import constants from "./constants";

export const _amountFormat = amount => {
  if (amount === undefined || amount === "" || amount === null)
    return numeral(0).format();
  return numeral(parseFloat(amount)).format();
};

export const formatMoney = (amount, options = {}) => {
  let format;

  if (amount === '' || amount === undefined || amount === null)
    return 'N/A';

  amount = parseFloat(amount);

  if (!isFinite(amount)) // NaN, Infinity, -Infinity
    return 'N/A';

  if (options.format)
    format = options.format;
  else if (options.currency || options.currency === undefined) // ON by default
    format = constants.FORMAT_MONEY_CURRENCY;
  else
    format = constants.FORMAT_MONEY;

  return numeral(amount).format(format, Math.round);
};


export const dateTimeFormat = dt => {
  if (dt === undefined || dt === "" || dt === null) return "";
  return moment(dt).format(constants.DATE_TIME_FORMAT);
};

export const formatDateTime = (date, format = constants.FORMAT_DATE_TIME) => {
  return moment(date).format(format);
};

export const formatDate = dt => {
  if (dt === undefined || dt === "" || dt === null) return "";
  return moment(dt).format(constants.DATE_FORMAT);
};

export const formatDateDB = dt => {
  if (dt === undefined || dt === "" || dt === null) return "";
  return moment(dt).format(constants.DB_DATE_FORMAT);
};

export const isEmptySting = str => {
  if (!str) return true;
  str = str.trim();
  return str.length === 0;
};

export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function tick() {
  return sleep(0)
}

export function isExternalUrl(url) {
  if (!url) return false;
  if (
    url.startsWith("blob:") ||
    url.startsWith("data:") ||
    url.startsWith("//")
  )
    return true;
  return /^(https?:\/\/|www\.)/.test(url);
}

export const isDevMode = () => {
  return process.env.NODE_ENV === "development";
};

export const fileToBase64 = async (file) => {
  return new Promise((resolve, reject) => {
    try {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const base64 = reader.result;
        resolve(base64);
      };
    } catch (e) {
      reject(e);
    }
  })
};

export function getFileExtension(filename) {
  let ext = /^.+\.([^.]+)$/.exec(filename);
  return ext == null ? "" : ext[1];
}
