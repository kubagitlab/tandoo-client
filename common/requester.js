import axios from "axios";

import appStore from "../store/AppStore";
import {isDevMode} from "./utils";

const WRONG_TOKEN = 64;

// const devApi = 'http://tandoo.university.kg/api/';
const devApi = 'http://localhost:1111/api/';
//const devApi = 'http://192.168.2.234:1111/api/';
const prodApi = '/api/';

const isDev = isDevMode();

export const BASE_URL = isDev ? devApi : prodApi;

axios.defaults.baseURL = BASE_URL;
axios.defaults.responseType = "json";
axios.defaults.timeout = isDev ? 90000 : 30000;

async function request(method, url, reqData, params, silent = false) {
  if (!silent)
    appStore.setBusy(true);
  const token = await appStore.loadToken();
  reqData = reqData || {};
  reqData.locale = appStore.locale;
  let headers = {"Content-Type": "application/json", 'Access-Control-Allow-Origin': '*'};
  if (token) {
    headers["api-token"] = token;
  }

  try {
    let {data} = await axios.request({method, url, data: reqData, params, headers});
    if (data.result === 0) {
      return data;
    }
    if (data.result === WRONG_TOKEN) {
      appStore.logout();
    } else if (!silent) {
      appStore.setAlert("warn", data.message);
    }
    return Promise.reject(data);

  } catch (error) {
    console.warn(error);
    if (!silent) {
      appStore.setAlert("error", error.message);
    }
    return Promise.reject(error);

  } finally {
    appStore.setBusy(false);
  }
}

const requester = {
  get(url, params = null, silent = false) {
    return request("get", url, null, params, silent);
  },
  post(url, data = null, silent = false) {
    return request("post", url, data, null, silent);
  },
  put(url, data = null, params = null, silent = false) {
    return request("put", url, data, params, silent);
  },
  delete(url, params = null, silent = false) {
    return request("delete", url, null, params, silent);
  },
};

export default requester;
