import requester from '../common/requester';
import storage from "../store/LocalStorage";

const commonApi = {
  getUserInfo(data = {}) {
    return requester.post('user_info', data);
  },
  login(data = {}) {
    return requester.post('login', data);
  },
  ecpLogin(token) {
    return requester.post('ecp_login', {token});
  },
  keycloakLogin(token) {
    return requester.post('keycloak_login', {token});
  },
  async getDir(name) {
    let data = storage.get(name);
    const found = data && Array.isArray(data);
    if (found) {
      requester.post('dir/listing', {name}, found).then(r => {
        storage.save(name, r.list);
      });
      return data;
    } else {
      const r = await requester.post('dir/listing', {name});
      data = r.list;
      storage.save(name, data);
      return data;
    }
  },
  getSectionOkgz(section_id) {
    return requester.post('section_okgz/list', {section_id});
  },
  getOkgzBySection(section_id) {
    return requester.post('okgz/by_section', {section_id});
  },
  upload(data) {
    return requester.post('upload', data);
  },
  getProductsByCategory(data = {}) {
    return requester.post('product/by_category', data);
  },
  getProduct(id) {
    return requester.post('product/get', {id});
  },
  okgzSearch(search) {
    return requester.post('okgz/search', {search});
  },
  deleteImage(id) {
    return requester.post('delete_file', {id});
  },
  getCompanyInfo(data) {
    return requester.post('company/info', data);
  },
  saveUserData(data) {
    return requester.post('user/save_data', {data});
  }
};

export default commonApi;
