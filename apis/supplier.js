import requester from '../common/requester';

const supApi = {
  register: function (data = {}) {
    return requester.post('sup/register', data);
  },
  getDebts: function (data = {}) {
    return requester.post('sup/debts', data);
  },
  sendDebtRequest: function (data = {}) {
    return requester.post('sup/debt/send_request', data);
  },
  getSupMainInfo: function (data = {}) {
    return requester.post('sup/main_info', data);
  },
  updateSupMainInfo: function (data = {}) {
    return requester.post('sup/update_info', data);
  },
  getCatalog: function (data = {}) {
    return requester.post('sup/catalog/list', data);
  },
  saveProduct: function (data = {}) {
    return requester.post('sup/product/save', data);
  },
  qualification(params) {
    return requester.post('sup/company/qualification', params);
  }
};

export default supApi;
