import requester from "../common/requester";
import storage from "../store/LocalStorage";

const adminApi = {
  register: function (data = {}) {
    return requester.post('admin/register', data);
  },
  async getDirs() {
    let data = storage.get('dirs');
    const found = data && Array.isArray(data);
    if (found) {
      requester.post('admin/dir/all', null, found).then(r => {
        storage.save('dirs', r.list);
      });
      return {list: data};
    } else {
      const r = await requester.post('admin/dir/all',);
      data = r.list;
      storage.save('dirs', data);
      return {list: data};
    }
  },
  saveDir: function (tableName, data) {
    return requester.post('admin/dir/save', {name: tableName, data});
  },
  getEntities: function (data) {
    return requester.post('con/entity/all', data);
  },
  getSectionOkgzEntities: function (data) {
    return requester.post('con/section_okgz_entities', data);
  },
  getEntityValues: function (data) {
    return requester.post('con/entityvalue/list', data);
  },
  saveEntity: function (section_okgz, entities) {
    return requester.post('con/entity/save', {section_okgz, entities});
  },
  getUsers: function (data = {}) {
    return requester.post('admin/user/all', data);
  },
  saveUser: function (data) {
    return requester.post('admin/user/save', {'user': data});
  },
  getProducts: function (data) {
    return requester.post('con/product/list', data);
  },
  saveProduct: function (data) {
    return requester.post('con/product/save', data);
  },
  deleteEntity(id) {
    return requester.post('con/entity/delete', {id});
  },
  deleteEntityValue(id) {
    return requester.post('con/entity_value/delete', {id});
  },
  updateDir(key) {
    return requester.post('admin/dir/update', {key})
  },
  getCompanies() {
    return requester.post('admin/company/list')
  },
  companyQualify(data) {
    return requester.post('admin/company/qualify', data);
  }
};

export default adminApi;
