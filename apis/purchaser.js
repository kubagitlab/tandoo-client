import requester from '../common/requester';

const purApi = {
  register(data = {}) {
    return requester.post('sup/register', data);
  },
  getSections() {
    return requester.post('section/by_products')
  },
  getProducts(params) {
    // params = {category_id, filters}
    return requester.post('pur/product/list', params)
  },
  getAnnounceList(params) {
    return requester.post('pur/advert/list', params)
  },
  saveAnnounce(params) {
    /*let example = {
      "announce": {
        "section": 2,
        "procurement": 14,
        "guarantee": 5,
        "concession": 4,
        "tehadress": "test",
        "guarant_day": 10,
        "defect_day": 3,
        "deadline": "2020-09-20",
        "start_date": "2020-09-10",
        "payments": {
          "advance": 100,
          "shipment": 0,
          "accept": 0
        },
        "comm_members": [
          {
            "employee_id": 3,
            "inn": "123",
            "fullname": "testtt",
            "position": "tt",
            "email": "tt@gmail.com",
            "company": "asdf"
          }
        ],
        "chairman_id": 5,
        "data": {},
        "lots": [
          {
            "category": 1730,
            "avg_unit_price": 111,
            "unit": 3,
            "specs": [
              {"entity_id": 14, "value_id": 9},
              {"entity_id": 14, "value_id": 10}
            ],
            "dirs": [
              {"entity_id": 7, "value_id": 1},
              {"entity_id": 31, "value_id": 3}
            ],
            "params": {
              "quantity": 11,
              "unit_price": 111,
              "estimated_delivery_time": 10,
              "address": {
                "coate": 2,
                "street": "ss",
                "house": "sss",
                "apt": "sss"
              }
            }
          }
        ]
      }
    }*/
    return requester.post('pur/advert/save', params);
  }
};

export default purApi;
