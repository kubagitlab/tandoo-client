import i18n from "i18next";
import {initReactI18next} from "react-i18next";
import translationRU from "./locales/ru";
import translationKG from "./locales/kg";
import translationEN from "./locales/en";
import numeral from "numeral";
import './numeral-locale-ru-som';
import constants from "../common/constants";

i18n.use(initReactI18next).init({
  lng: "ru",
  fallbackLng: "ru",
  resources: {
    ru: {translation: translationRU},
    kg: {translation: translationKG},
    en: {translation: translationEN}
  },
  keySeparator: false,
  interpolation: {
    escapeValue: false // not needed for react as it escapes by default
  },
  debug: false //isDevMode()
});
export default i18n;

numeral.locale('ru-som');
numeral.defaultFormat(constants.FORMAT_MONEY);
