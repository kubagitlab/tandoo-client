import loadable from '@loadable/component';
import constants from '../../../common/constants';

const routes = [
  {
    order: 1,
    title: 'Каталог',
    path: '/pur/catalog',
    component: loadable(() => import('../views/purchaser/Catalog')),
    roles: [constants.ROLE_PURCHASER],
    isHeaderMenu: true,
  },
  {
    order: 1,
    title: 'Создать объявление',
    path: '/pur/announce/edit/:id?',
    component: loadable(() => import('../views/purchaser/Announce/Edit')),
    roles: [constants.ROLE_PURCHASER],
    // isHeaderMenu: true,
  },
  {
    order: 1,
    title: 'Мои объявления',
    path: '/pur/announce',
    component: loadable(() => import('../views/purchaser/Announce')),
    roles: [constants.ROLE_PURCHASER],
    isHeaderMenu: true,
  },
  {
    order: 2,
    title: 'Просмотр организации',
    path: '/pur/org/view/:id',
    component: loadable(() => import('../views/purchaser/Org/View')),
    roles: [constants.ROLE_PURCHASER],
    // isHeaderMenu: false,
  },
  {
    order: 2,
    title: 'Редактирование организации',
    path: '/pur/org/edit/:id',
    component: loadable(() => import('../views/purchaser/Org/Edit')),
    roles: [constants.ROLE_PURCHASER],
    isHeaderMenu: false,
  },
  {
    order: 2,
    title: 'Мои организации',
    path: '/pur/org',
    component: loadable(() => import('../views/purchaser/Org')),
    roles: [constants.ROLE_PURCHASER],
    isHeaderMenu: true,
  },
  {
    order: 3,
    title: 'Профиль организации',
    path: '/pur/profile',
    component: loadable(() => import('../views/purchaser/Profile')),
    roles: [constants.ROLE_PURCHASER],
    isHeaderMenu: true,
  },
  {
    order: 4,
    title: 'Сообщения',
    path: '/messages',
    roles: [constants.ROLE_PURCHASER],
    isHeaderMenu: true,
  },
  {
    order: 5,
    title: 'Договора',
    path: '/pur/agreements',
    component: loadable(() => import('../views/purchaser/Agreements')),
    roles: [constants.ROLE_PURCHASER],
    isHeaderMenu: true,
  },
];

export default routes;
