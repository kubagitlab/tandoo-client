import loadable from '@loadable/component'
import constants from '../../../common/constants';

const routes = [
  {
    order: 10,
    title: 'Описание продукции',
    path: '/adm/product',
    component: loadable(() => import('../views/admin/Products')),
    roles: [constants.ROLE_SUPER_ADMIN],
    isHeaderMenu: true,
  },
  // {
  //   order: 2,
  //   title: 'Отчеты',
  //   path: '/adm/report',
  //   component: loadable(() => import('../views/admin/Home')),
  //   roles: [constants.ROLE_SUPER_ADMIN],
  //   isHeaderMenu: true,
  // },

  {
    order: 20,
    title: 'Справочный каталог',
    path: '/adm/catalog',
    component: loadable(() => import('../views/admin/Catalog')),
    roles: [constants.ROLE_SUPER_ADMIN],
    isHeaderMenu: true,

  },
  {
    order: 30,
    title: 'Спецификации',
    path: '/adm/specs',
    component: loadable(() => import('../views/admin/Specs')),
    roles: [constants.ROLE_SUPER_ADMIN],
    isHeaderMenu: true,
  },
  {
    order: 33,
    title: 'Сообщения',
    path: '/messages',
    roles: [constants.ROLE_SUPER_ADMIN],
    isHeaderMenu: true,
  },
  {
    order: 40,
    title: 'Справочники',
    path: '/adm/directory',
    component: loadable(() => import('../views/admin/Directory')),
    roles: [constants.ROLE_SUPER_ADMIN],
    isHeaderMenu: true,
  },
  {
    order: 50,
    title: 'Организации',
    path: '/adm/org',
    component: loadable(() => import('../views/admin/Org')),
    roles: [constants.ROLE_SUPER_ADMIN],
    isHeaderMenu: true,
  },
  {
    order: 60,
    title: 'Пользователи',
    path: '/adm/users',
    component: loadable(() => import('../views/admin/Users')),
    roles: [constants.ROLE_SUPER_ADMIN],
    isHeaderMenu: true,
  },
];

export default routes;
