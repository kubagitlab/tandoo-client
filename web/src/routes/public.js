import loadable from "@loadable/component";
import constants from "../../../common/constants";

const routes = [
  {
    order: 1,
    title: 'Объявления',
    path: '/announcement',
    component: loadable(() => import('../views/public/Announcement')),
    isHeaderMenu: true
  },
  {
    order: 2,
    title: 'Каталог',
    path: '/catalog',
    component: loadable(() => import('../views/public/Catalog')),
    isHeaderMenu: true
  },
  {
    order: 3,
    title: 'Отечественный производитель',
    path: '/domestic',
    component: loadable(() => import('../views/public/Domestic')),
    isHeaderMenu: true
  },
  {
    order: 4,
    title: 'Лот',
    path: '/lot',
    component: loadable(() => import('../views/public/Lot')),
  },

];

export default routes;
