import loadable from '@loadable/component';
import constants from '../../../common/constants';

const routes = [
  {
    order: 1,
    title: 'Мои Заявки',
    path: '/sup/appl',
    component: loadable(() => import('../views/supplier/Appl')),
    roles: [constants.ROLE_SUPPLIER],
    isHeaderMenu: true,
  },
  {
    order: 2,
    title: 'Организации',
    path: '/sup/org',
    component: loadable(() => import('../views/supplier/Org')),
    roles: [constants.ROLE_SUPPLIER],
    isHeaderMenu: true,
  },
  {
    order: 3,
    title: 'Профиль',
    path: '/sup/profile',
    component: loadable(() => import('../views/supplier/Profile')),
    roles: [constants.ROLE_SUPPLIER],
    isHeaderMenu: true,
  },
  {
    order: 4,
    title: 'Сообщения',
    path: '/messages',
    roles: [constants.ROLE_SUPPLIER],
    isHeaderMenu: true,
  },
  {
    order: 5,
    title: 'Мой Каталог',
    path: '/sup/catalog',
    component: loadable(() => import('../views/supplier/Catalog')),
    roles: [constants.ROLE_SUPPLIER],
    isHeaderMenu: true,
  },
  {
    order: 6,
    title: 'Договора',
    path: '/sup/agreements',
    component: loadable(() => import('../views/supplier/Agreements')),
    roles: [constants.ROLE_SUPER_ADMIN],
    isHeaderMenu: true,
  },
  {
    order: 7,
    title: 'Добавление товара',
    path: '/sup/product/list',
    component: loadable(() => import('../views/supplier/SupProducts')),
    roles: [constants.ROLE_SUPPLIER],
    isHeaderMenu: false,
  },
  {
    order: 8,
    title: 'Добавление товара',
    path: '/sup/product/add/:id',
    component: loadable(() => import('../views/supplier/AddProduct')),
    roles: [constants.ROLE_SUPPLIER],
    isHeaderMenu: false,
  },
  {
    order: 8,
    title: 'Добавление товара',
    path: '/sup/company/qualification',
    component: loadable(() => import('../views/supplier/Qualification')),
    roles: [constants.ROLE_SUPPLIER],
    isHeaderMenu: false,
  }
];

export default routes;
