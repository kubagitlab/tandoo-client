import React from 'react';
import {Redirect, Route, Switch,} from "react-router-dom";
import routes from '../../routes/admin';


import constants from "../../../../common/constants";

export default ({user}) => {

  if (!user) {
    return <Redirect to="/"/>
  }

  if (user.role_id === constants.ROLE_SUPPLIER) {
    return <Redirect to="/sup"/>
  }

  if (user.role_id === constants.ROLE_PURCHASER) {
    return <Redirect to="/pur"/>
  }

  return (
    <Switch>
      <Redirect exact from="/adm" to="/"/>
      {routes.map(r => <Route key={r.path} path={r.path} name={r.title} component={r.component}/>)}
    </Switch>
  );
}
