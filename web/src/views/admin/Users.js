import React, {useEffect, useState} from 'react';
import {Card, Form, Modal, PageHeader, Select, Tooltip} from 'antd';
import Table from '../../components/Table';
import Button from '../../components/Button';
import {MdEdit} from 'react-icons/md';
import Input from '../../components/Input';
import adminApi from '../../../../apis/admin';

const {Option} = Select;
const layout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 18,
  },
};
const roles = [
  {
    id: 1, name: 'Поставщик',
  },
  {
    id: 2, name: 'Закупщик',
  },
  {
    id: 100, name: 'Супер Админ',
  },
  {
    id: 101, name: 'Админ',
  },
  {
    id: 102, name: 'Оператор',
  },
  {
    id: 1033, name: 'Конструктор',
  },
];

export default () => {
  const [form] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [users, setUsers] = useState([]);

  const columns = [
    {
      title: 'Имя',
      dataIndex: 'name',
      key: 'name',
      align: 'center',
    },
    {
      title: 'Фамилия',
      dataIndex: 'surname',
      key: 'surname',
      align: 'center',
    },
    {
      title: 'Логин',
      dataIndex: 'username',
      key: 'username',
      align: 'center',
    },
    {
      title: 'Роль',
      dataIndex: 'role',
      key: 'role',
      align: 'center',
    },
    {
      title: '',
      dataIndex: 'edit',
      key: 'edit',
      width: 30,
      render: (text, record) =>
        <Tooltip placement="right" title="Редактировать">
          <Button onClick={() => editUser(record)} size="small" type="default" icon={<MdEdit/>}/>
        </Tooltip>,
      align: 'center',
    },
  ];

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = () => {
    adminApi.getUsers().then(r => {
      setUsers(r.users);
    });
  };

  const addUser = () => {
    form.resetFields();
    form.setFieldsValue({
      role: 3,
    });

    setVisible(true);
  };

  const editUser = (user) => {
    form.setFieldsValue({
      id: user.id,
      name: user.name,
      surname: user.surname,
      username: user.username,
      password: '',
      role_id: parseInt(user.role_id),
    });

    setVisible(true);
  };

  const closeModal = () => {
    setVisible(false);
  };

  const onOk = () => {
    form.validateFields()
      .then(values => {
        let params = values;
        params['role_id'] = parseInt(form.getFieldValue('role'));
        adminApi.saveUser(params).then(r => {
          getUsers();
          closeModal();
        });
      })
      .catch(errorInfo => {
      });
  };

  return (
    <>
      <PageHeader title="Пользователи" extra={<Button onClick={addUser}>Добавить</Button>}/>
      <Card size="small">
        <Table bordered data={users} columns={columns}/>
      </Card>

      <Modal
        title="Создание и редактироание пользователя"
        visible={visible}
        onOk={onOk}
        okText="Сохранить"
        onCancel={closeModal}
        maskClosable={false}
        style={{top: 20}}
      >
        <Form {...layout} form={form} name="control-hooks">
          <FormHiddenInput name="id"/>
          <FormInput label="Имя" name="name" rules={[{required: true}]}/>
          <FormInput label="Фамилия" name="surname"/>
          <FormInput label="Логин" name="username" rules={[{required: true}]}/>
          <FormPassInput label="Пароль" name="password"/>
          <FormSelect label="Роль" name="role_id" placeholder="Выберите" data={roles}/>
        </Form>
      </Modal>
    </>
  );
};

const FormInput = ({label, name, disabled, rules}) => {
  return (
    <Form.Item label={label} name={name} rules={rules}>
      <Input disabled={disabled}/>
    </Form.Item>
  );
};
const FormPassInput = ({label, name, disabled}) => {
  return (
    <Form.Item label={label} name={name}>
      <Input disabled={disabled} type="password"/>
    </Form.Item>
  );
};
const FormHiddenInput = ({label, name}) => {
  return (
    <Form.Item label={label} name={name}>
      <Input type="hidden"/>
    </Form.Item>
  );
};
const FormSelect = ({label, name, data, placeholder, onChange}) => {
  return (
    <Form.Item label={label} name={name}>
      <Select
        showSearch
        placeholder={placeholder}
        optionFilterProp="children"
        filterOption={(input, option) =>
          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        onChange={onChange}
      >
        {data.map((item, idx) => (<Option key={idx} value={item.id}>{item.name}</Option>))}
      </Select>
    </Form.Item>
  );
};
