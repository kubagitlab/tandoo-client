import React from 'react';
import {Card, Modal, PageHeader, Space, Tag, Input} from 'antd';
import ReactTable from '../../components/ReactTable';
import Button from '../../components/Button';
import adminApi from "../../../../apis/admin";
import {MdSearch} from "react-icons/all";
import Qualification from "../supplier/Qualification";
import appStore from "../../../../store/AppStore";


export default () => {

  const [companies, setCompanies] = React.useState([]);
  const [company, setCompany] = React.useState(null);
  const [showReject, setShowReject] = React.useState(false);
  const [rejectReason, setRejectReason] = React.useState('');

  React.useEffect(() => {
    getCompanies();
  }, []);

  const columns = [
    {
      Header: 'Организация',
      accessor: 'name',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Статус',
      accessor: 'company_status',
      className: 'text-center',
      headerClassName: 'f-bold',
      width: 120,
      Cell: ({value}) => {
        let color = 'default';
        if (value === 'waiting') {
          color = 'processing'
        }
        if (value === 'draft') {
          color = 'warning'
        }
        if (value === 'confirmed') {
          color = 'success'
        }
        return <Tag color={color}>{value || 'неизвестен'}</Tag>
      },
    },
    {
      Header: 'Тип организации',
      accessor: 'company_type',
      className: 'text-center',
      headerClassName: 'f-bold',
      width: 120,
      Cell: ({value}) => {
        let text = 'неизвестен';
        if (value === 'supplier') {
          text = 'поставщик'
        }
        if (value === 'supplier') {
          text = 'закупщик'
        }
        return <span>{text}</span>
      },
    },
    {
      Header: 'ИНН',
      accessor: 'inn',
      className: 'text-center',
      headerClassName: 'f-bold',
      width: 180
    },
    {
      Header: '',
      accessor: 'id',
      className: 'text-center',
      width: 50,
      Cell: ({original}) => <Button size='small' onClick={() => setCompany(original)}><MdSearch/></Button>
    },
  ];

  const getCompanies = () => {
    adminApi.getCompanies().then(r => setCompanies(r.list));
  }

  const action = (data) => {
    adminApi.companyQualify(data).then(r => {
      appStore.setAlert('success', 'DONE');
      setShowReject(false);
      setRejectReason('');
      setCompany(null);
      getCompanies();
    });
  }

  return (
    <>
      <PageHeader title="Организации" extra={<Button>Добавить организацию</Button>}/>

      <Card className="card-container">
        <ReactTable data={companies} columns={columns} pageSize={10}/>
      </Card>

      <Modal
        title={company ? company.name : ''}
        visible={!!company}
        width={'100%'}
        height={'100%'}
        okText="Сохранить"
        onCancel={() => setCompany(null)}
        maskClosable={false}
        style={{top: 0}}
        footer={null}
      >
        {company && <><Qualification companyId={company.id}/>
          <Space size='large'>
            <Button onClick={() => {
              action({company_id: company.id, status: 'confirmed'});
            }}>
              Approve
            </Button>
            <Button type='danger' onClick={() => setShowReject(true)}>Reject</Button>
          </Space>

          <Modal
            title={'Причина отказа'}
            visible={showReject}
            width={500}
            onOk={() => {
              action({company_id: company.id, status: 'draft', reason: rejectReason});
            }}
            okText="Сохранить"
            onCancel={() => setShowReject(false)}
            maskClosable={false}
          >
            <Input.TextArea rows={4} onChange={e => setRejectReason(e.target.value)} value={rejectReason}/>
          </Modal>
        </>}
      </Modal>
    </>
  )
};
