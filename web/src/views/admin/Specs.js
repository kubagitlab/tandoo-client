import React from 'react';
import {Card, Col, Empty, List, PageHeader, Row, Select, Space, Tooltip} from 'antd';
import Button, {ConfirmButton} from '../../components/Button';
import {MdDelete} from 'react-icons/md';
import Input, {InputNumber} from '../../components/Input';
import Table from '../../components/Table';
import {observer} from 'mobx-react';
import {observable, runInAction} from 'mobx';
import adminApi from '../../../../apis/admin';
import Checkbox from '../../components/Checkbox';
import commonApi from '../../../../apis/common';
import appStore from '../../../../store/AppStore';
import i18n from '../../../../translate/i18n';
import Wrapper from '../../components/Wrapper';

const {Option} = Select;

const newEntity = {
  guest: true,
  mandatory: true,
  name_en: '',
  name_kg: '',
  name: '',
  order: 0,
  purchaser: true,
  spr: false,
  supplier: true,
};

const newValue = {
  order: 0,
  name: '',
  name_kg: '',
  name_en: '',
};

@observer
export default class Specs extends React.Component {

  @observable entities = [];
  @observable dirSection = [];
  @observable okgzList = [];
  @observable sectionOkgzList = [];
  @observable dirs = [];
  @observable sectionOkgz = {};
  @observable currentEntity = null;

  componentDidMount() {
    commonApi.getDir('DirSection').then(r => {
      this.dirSection = r.sort((a, b) => a.name.localeCompare(b.name));
    });
    adminApi.getDirs().then(r => this.dirs = r.list);
    commonApi.getDir('DirOkgz').then(r => this.okgzList = r);
  }

  entityAdd = () => {
    this.entities = [...this.entities, newEntity];
  };

  entityValueAdd = (entity) => {
    if (!entity.values) {
      entity.values = [];
    }
    entity.values = [...entity.values, newValue];
    this.forceUpdate();
  };

  saveEntity = () => {
    adminApi.saveEntity(this.sectionOkgz, this.entities).then(r => {
      appStore.setAlert('success', 'Success');
      this.getEntities(undefined, this.sectionOkgz.section_id, this.sectionOkgz.okgz_id);
    });
  };

  reset = () => {
    this.entities = [];
    this.currentEntity = null;
    this.sectionOkgz = {id: null, okgz_id: null};
    this.sectionOkgzList = [];
  };

  deleteEntity = (entity, index) => {
    if (entity.id) {
      adminApi.deleteEntity(entity.id).then(() => {
        this.entities.remove(entity);
        this.entities = [...this.entities];
      })
    } else {
      this.entities.remove(entity);
      this.entities = [...this.entities];
    }
  }

  deleteEntityValue = (entityValue, index) => {
    if (!this.currentEntity || this.currentEntity.spr) {
      return;
    }
    if (entityValue.id) {
      adminApi.deleteEntityValue(entityValue.id).then(() => {
        this.currentEntity.values = this.currentEntity.values.filter(v => v.id !== entityValue.id);
      })
    } else {
      this.currentEntity.values = this.currentEntity.values.splice(index, 1);
    }
  }

  getEntities = (id, section_id, okgz_id) => {
    adminApi.getSectionOkgzEntities({id, section_id, okgz_id}).then(r => this.entities = r.list);
  }

  render() {

    const columns = [
      {
        title: 'id',
        dataIndex: 'id',
        align: 'center',
        width: 50,
      },
      {
        title: '№',
        dataIndex: 'order',
        render: (value, row) => <InputNumber model={row} name='order'/>,
        align: 'center',
        width: 50,
      },
      {
        title: 'RU',
        dataIndex: 'name',
        render: (value, row) => <Input model={row} name='name'/>,
      },
      {
        title: 'KG',
        dataIndex: 'name_kg',
        key: 'kg',
        render: (value, row) => <Input model={row} name='name_kg'/>,
      },
      {
        title: 'EN',
        dataIndex: 'name_en',
        key: 'en',
        render: (value, row) => <Input model={row} name='name_en'/>,
      },
      {
        title: 'Справочник',
        dataIndex: 'spr',
        render: (value, row) =>
          <>
            <Checkbox checked={row.spr} onChange={(val) => {
              runInAction(() => row.spr = val);
              if (!val) {
                row.dir_name = null;
              }
              this.forceUpdate();
            }}/>
            {row.spr &&
            <Select
              style={{minWidth: 150}}
              value={row.dir_name}
              onChange={val => {
                row.dir_name = val;
                this.forceUpdate();
              }}>
              {this.dirs.map(item => <Option key={item.table} value={item.table}>
                {i18n.t(item.table)}
              </Option>)}
            </Select>}
          </>,
        align: 'center',
        width: 100,
      },
      {
        title: 'Наблюдатель',
        dataIndex: 'guest',
        render: (value, row) => <Checkbox model={row} name='guest'/>,
        align: 'center',
        width: 100,
      },
      {
        title: 'Закупщик',
        dataIndex: 'purchaser',
        key: 'purchaser',
        render: (value, row) => <Checkbox model={row} name='purchaser'/>,
        align: 'center',
        width: 100,
      },
      {
        title: 'Поставщик',
        dataIndex: 'supplier',
        render: (value, row) => <Checkbox model={row} name='supplier'/>,
        align: 'center',
        width: 100,
      },
      {
        title: '',
        dataIndex: '',
        key: 'x',
        align: 'center',
        width: 50,
        render: (text, record, index) => <Tooltip title="Удалить">
          <ConfirmButton icon={<MdDelete/>} size='small' onConfirm={() => this.deleteEntity(record, index)}/>
        </Tooltip>,
      },
    ];

    const childColumns = [
      {
        title: 'id',
        dataIndex: 'id',
        align: 'center',
        width: 50,
      },
      {
        title: '№',
        dataIndex: 'order',
        render: (value, row) => <InputNumber size='small' model={row} name='order'/>,
        align: 'center',
        width: 50,
      },
      {
        title: 'RU',
        dataIndex: 'name',
        render: (value, row) => <Input size='small' model={row} name='name'/>,
      },
      {
        title: 'KG',
        dataIndex: 'name_kg',
        render: (value, row) => <Input size='small' model={row} name='name_kg'/>,
      },
      {
        title: 'EN',
        dataIndex: 'name_en',
        render: (value, row) => <Input size='small' model={row} name='name_en'/>,
      },
      {
        title: '',
        dataIndex: '',
        key: 'x',
        align: 'center',
        render: (text, record, index) => <Tooltip title="Удалить">
          <ConfirmButton icon={<MdDelete/>} size='small' onConfirm={() => this.deleteEntityValue(record, index)}/>
        </Tooltip>,
      },
    ];

    return (
      <>
        <PageHeader title="Спецификации"/>
        <Wrapper>
          <Row gutter={16}>
            <Col xs={24} sm={12} md={8} lg={6} xl={4}>
              <List
                className="list-menu"
                style={{backgroundColor: 'white'}}
                size="small"
                bordered
                dataSource={this.sectionOkgzList}
                header={<div className="d-flex">
                  <Select
                    defaultValue="Выберите"
                    style={{width: '100%'}}
                    value={this.sectionOkgz.section_id}
                    showSearch
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                    onChange={(value) => {
                      commonApi.getSectionOkgz(value).then(r => this.sectionOkgzList = r.list);
                      this.entities = [];
                      this.sectionOkgz = {section_id: value, okgz_id: null};
                    }}
                  >
                    {this.dirSection.map(item => <Option key={item.id} value={item.id}>{item.name}</Option>)}
                  </Select>
                </div>}
                renderItem={item => <List.Item onClick={() => {
                  this.sectionOkgz = {id: item.id, section_id: item.section_id, okgz_id: item.okgz_id};
                  this.currentEntity = null;
                  this.entities = [];
                  this.getEntities(item.id);
                }}>
                  <List.Item.Meta
                    title={`${item.okgz.code} - ${item.okgz.name}`}
                  />
                </List.Item>}
              />
            </Col>

            <Col xs={24} sm={12} md={16} lg={18} xl={20}>
              <Select
                disabled={!this.sectionOkgz.section_id}
                showSearch
                optionFilterProp="children"
                value={this.sectionOkgz.okgz_id}
                onChange={(value) => {
                  this.sectionOkgz.okgz_id = value;
                  this.getEntities(undefined, this.sectionOkgz.section_id, value);
                }}
                filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                style={{width: '100%', marginBottom: 10}}>
                {this.okgzList.map((item) => <Option key={item.id} value={item.id}>
                    {`${item.code} : ${item.name}`}
                  </Option>,
                )}
              </Select>

              {!!this.sectionOkgz.section_id ?
                <Card size="small" title={<Button size="small"
                                                  onClick={this.entityAdd}
                                                  disabled={!this.sectionOkgz.okgz_id}
                                                  style={{marginRight: 10}}>
                  Добавить атрибут
                </Button>}>

                  <Table
                    columns={columns}
                    data={this.entities}
                    pagination={false}
                    expandable={{
                      expandedRowRender: record => !record.spr &&
                        <div style={{backgroundColor: 'rgb(0,216,255)', padding: 5}}>
                          <Space style={{marginBottom: 5}}>
                            <Button type="default" size='small' onClick={() => this.entityValueAdd(record)}>
                              Добавить значение
                            </Button>
                          </Space>
                          <Table columns={childColumns} data={record.values || []} pagination={false}/>
                        </div>,
                    }}
                    onExpand={(expanded, row) => {
                      this.currentEntity = expanded ? row : null;
                    }}
                  />
                  <br/>
                  <Button onClick={this.saveEntity}
                          disabled={!this.sectionOkgz.okgz_id || this.entities.length === 0}>
                    Сохранить
                  </Button>
                  <Space/>
                  <ConfirmButton style={{float: 'right'}}
                                 type='danger'
                                 disabled={!this.sectionOkgz.okgz_id || this.entities.length === 0}
                                 onConfirm={() => {
                                   this.getEntities(undefined, this.sectionOkgz.section_id, this.sectionOkgz.okgz_id);
                                 }}
                  >
                    Отменить
                  </ConfirmButton>
                </Card> : <Empty description='Выберите Раздел'/>}
            </Col>
          </Row>
        </Wrapper>
      </>
    );
  }
};
