import React from 'react';
import {Card, Empty, Form, Layout, Menu, Modal, PageHeader, Tooltip} from 'antd';
import Button, {ConfirmButton} from '../../components/Button';
import {MdChevronRight, MdDelete, MdEdit} from 'react-icons/md';
import adminApi from '../../../../apis/admin';

import {observable} from 'mobx';
import {observer} from 'mobx-react';
import i18n from '../../../../translate/i18n';
import commonApi from '../../../../apis/common';
import Input from '../../components/Input';
import ReactTable from '../../components/ReactTable';
import Wrapper from '../../components/Wrapper';

const {Content, Sider} = Layout;

const layout = {
  labelCol: {span: 8},
  wrapperCol: {span: 16},
};

@observer
export default class Directory extends React.Component {

  constructor(props) {
    super(props);
    this.state = {searchText: '', searchedColumn: ''};
  }

  @observable dirs = [];
  @observable selectedDir;
  @observable columns = [];
  @observable show = false;
  @observable list = [];
  @observable item = null;

  componentDidMount() {
    adminApi.getDirs().then(r => this.dirs = r.list);
  }

  setDir = (dir) => {
    let cols = [];
    dir.columns.forEach(c => {
      if (c !== 'data') {
        cols.push({Header: i18n.t(c), id: c, accessor: c});
      }
    });
    cols.push(
      {
        Header: '',
        id: 'edit',
        width: 30,
        Cell: ({original}) => (
          <Tooltip title="Редактировать">
            <Button icon={<MdEdit/>} size='small' onClick={() => this.openModal(original)}/>
          </Tooltip>
        ),
      },
      {
        Header: '',
        id: 'delete',
        width: 30,
        Cell: ({original}) => (
          <Tooltip title="Удалить">
            <ConfirmButton icon={<MdDelete/>} size='small' onConfirm={() => console.log('delete', original)}/>
          </Tooltip>
        ),
      },
    );
    this.selectedDir = dir;
    this.columns = cols;
    this.list = [];
    this.getDir();
  };

  getDir = () =>{
    commonApi.getDir(this.selectedDir.table).then(r => this.list = r);
  }

  save = () => {
    adminApi.saveDir(this.selectedDir.table, this.item).then(r => {
      this.list = r.list;
      this.closeModal();
    });
  };

  updateDir = (key) => {
    adminApi.updateDir(key).then(r => this.getDir());
  };

  closeModal = () => {
    this.item = null;
  };

  openModal = (record) => {
    this.item = record ? record : {};
  };

  render() {
    return (
      <>
        <PageHeader title="Справочники" extra={<Button type="primary" onClick={() => this.updateDir('all')}>Обновить все
          справочники</Button>}/>
        <Wrapper>
          <Sider width={250}>
            <Menu>
              {this.dirs.map((item, idx) => (
                <Menu.Item key={idx} icon={<MdChevronRight/>}
                           onClick={() => this.setDir(item)}>
                  {i18n.t(item.table)}
                </Menu.Item>
              ))}
            </Menu>
          </Sider>

          <Content style={{paddingLeft: '10px'}}>
            {this.selectedDir ? <Card size="small"
                                      title={i18n.t(this.selectedDir.table)}
                                      extra={<>
                                        <Button type="primary" size="small" onClick={() => this.openModal()}>
                                          Добавить
                                        </Button>
                                        &nbsp;
                                        &nbsp;
                                        <Button type="default" size="small"
                                                onClick={() => this.updateDir(this.selectedDir.table)}>
                                          Обновить
                                        </Button>
                                      </>
                                      }>
                <ReactTable showRowNumbers={false} data={this.list} columns={this.columns} pageSize={20}/>
              </Card> :
              <Empty description='Выберите справочник'/>}
          </Content>

          {this.selectedDir &&
          <Modal
            title={i18n.t(this.selectedDir.table)}
            visible={!!this.item}
            onOk={() => this.save()}
            okText="Сохранить"
            onCancel={this.closeModal}
            maskClosable={false}
            style={{top: 20}}
          >
            {this.item && <Form {...layout}>
              {this.columns
                .filter(c => !['id', 'data', 'edit', 'delete'].includes(c.id))
                .map(c =>
                  <Form.Item key={c.id} label={c.Header} name={c.id}>
                    <Input model={this.item} name={c.accessor}/>
                  </Form.Item>,
                )}
            </Form>
            }
          </Modal>
          }
        </Wrapper>
      </>
    );
  }
};
