import React from 'react';
import ProductList from "../../components/product/ProductListPublic";
import {PageHeader} from "antd";

export default () => {
  return <>
    <PageHeader title="Справочный каталог"/>
    <ProductList/>
  </>;
};
