import React from 'react';
import {Card, Col, Empty, Form, Modal, PageHeader, Row, Select, Space, Tooltip} from 'antd';
import Button from '../../components/Button';
import {MdCheck, MdClose, MdEdit} from 'react-icons/md';
import Input from '../../components/Input';
import Checkbox from '../../components/Checkbox';
import commonApi from '../../../../apis/common';
import {observable, runInAction} from 'mobx';
import {observer} from 'mobx-react';
import adminApi from '../../../../apis/admin';
import appStore from '../../../../store/AppStore';
import ReactTable from '../../components/ReactTable';
import Uploader from '../../components/Uploader';
import ImageView from '../../components/ImageView';
import ProductImageView from '../../components/product/ProductImageView';
import Wrapper from '../../components/Wrapper';

const {Option} = Select;
const layout = {labelCol: {span: 12}, wrapperCol: {span: 12}};
const tailLayout = {wrapperCol: {offset: 12, span: 12}};

const FormInput = ({label, model, name, disabled}) => {
  return (
    <Form.Item label={label}>
      <Input model={model} name={name} disabled={disabled}/>
    </Form.Item>
  );
};

@observer
export default class Products extends React.Component {

  @observable dirSection = [];
  @observable sectionOkgz = {};
  @observable sectionOkgzList = [];
  @observable visible = false;
  @observable products = [];
  @observable entities = [];
  @observable product = {values: []};
  @observable cProduct = null;
  @observable showImage = false;

  componentDidMount() {
    commonApi.getDir('DirSection').then(r => {
      this.dirSection = r.sort((a, b) => a.name.localeCompare(b.name));
    });
  }

  toggleModal = () => {
    this.visible = !this.visible;
  };

  onSave = () => {
    adminApi.saveProduct(this.product).then(r => {
      this.toggleModal();
      appStore.setAlert('success', 'SAVED');
      this.listProducts();
    });
  };

  onSectionSelect = (value) => {
    commonApi.getSectionOkgz(value).then(r => this.sectionOkgzList = r.list);
    this.sectionOkgz.id = null;
    this.sectionOkgz.section_id = value;
  };

  onSectionOkgzSelect = (value, options) => {
    this.sectionOkgz.id = value;
    this.sectionOkgz.name = options.children;
    this.product = {parent_id: value, local: false, values: []};
    this.listProducts();
    adminApi.getEntities({parent_id: value}).then(r => {
      this.entities = r.list;
    });
  };

  listProducts = () => {
    adminApi.getProducts({parent_id: this.sectionOkgz.id}).then(r => {
      this.products = r.list;
    });
  };

  onAddProduct = () => {
    const values = [];
    this.entities.forEach(v => {
      values.push({
        spr: v.spr,
        dir_name: v.dir_name,
        label: v.name,
        entity_id: v.id,
        value_id: null,
        values: v.values,
      });
    });
    this.product = {parent_id: this.sectionOkgz.id, local: false, values};
    this.toggleModal();
  };

  getValue = (values, entity_id) => {
    return values.find(v => v.entity_id === entity_id);
  };

  onProductEdit = (product) => {
    this.product = product;
    const values = [];
    this.entities.forEach(entity => {
      const value = this.getValue(product.values, entity.id);
      values.push({
        id: value ? value.id : null,
        spr: entity.spr,
        dir_name: entity.dir_name,
        label: entity.name,
        entity_id: entity.id,
        value_id: value ? value.value_id : null,
        values: entity.values,
      });
    });
    this.product.values = values;
    this.toggleModal();
  };

  makeColumns = () => {

    if (!this.products.length) {
      return [];
    }

    const columns = [
      {Header: 'id', accessor: 'id', width: 50, className: 'text-center', headerClassName: 'f-bold'},
      {
        id: 'edit',
        Header: '',
        filterable: false,
        width: 50,
        className: 'text-center',
        headerClassName: 'f-bold',
        Cell: ({original}) => <Tooltip placement="right" title="Редактировать">
          <Button onClick={() => this.onProductEdit(original)} size="small" type="default" icon={<MdEdit/>}/>
        </Tooltip>,
      },
      {Header: 'Штрих код', accessor: 'barcode', width: 150, className: 'text-center', headerClassName: 'f-bold'},
      {
        id: 'image',
        Header: 'Изображение',
        accessor: 'image',
        filterable: false,
        width: 80,
        headerClassName: 'f-bold',
        Cell: (props) => <ImageView id={props.value} thumbnail
                                    width={50}
                                    onClick={() => {
                                      runInAction(() => {
                                        this.cProduct = props.original;
                                        this.showImage = true;
                                      });
                                    }}/>,
      },
      {
        Header: 'Отечественный',
        accessor: 'local',
        filterable: false,
        width: 50,
        className: 'text-center',
        headerClassName: 'f-bold',
        Cell: (props) => props.value ? <MdCheck/> : <MdClose color="red"/>,
      },
    ];

    this.entities.forEach(entity => {
      const k = entity.name;
      columns.push({Header: k, accessor: k, className: 'text-center', headerClassName: 'f-bold'});
    });

    return columns;

  };

  render() {
    return (
      <>
        <PageHeader title="Описание продукции"/>
        <Wrapper>
          <Row gutter={[10, 10]}>
            <Col xs={24} sm={12}>
              <Select
                style={{width: '100%'}}
                value={this.sectionOkgz.section_id}
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                onChange={this.onSectionSelect}>
                {this.dirSection.map(item => <Option key={item.id} value={item.id}>{item.name}</Option>)}
              </Select>
            </Col>
            <Col xs={24} sm={12}>
              <Select
                disabled={!this.sectionOkgz.section_id}
                showSearch
                optionFilterProp="children"
                value={this.sectionOkgz.id}
                onChange={this.onSectionOkgzSelect}
                filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                style={{width: '100%', marginBottom: 10}}>
                {this.sectionOkgzList.map((item) => <Option key={item.id} value={item.id}>
                    {`${item.okgz.code} : ${item.okgz.name}`}
                  </Option>,
                )}
              </Select>
            </Col>
          </Row>

          {this.sectionOkgz.id ?
            <Card size="small">
              <Space style={{marginBottom: 16}}>
                <Button onClick={this.onAddProduct}> Добавить </Button>
              </Space>
              {this.products.length ? <ReactTable data={this.products} columns={this.makeColumns()} pageSize={10}/> :
                <Empty/>}
            </Card> : <Empty/>}

          <Modal
            title={this.sectionOkgz.name}
            visible={this.visible}
            onOk={this.onSave}
            okText="Сохранить"
            onCancel={this.toggleModal}
            width={1024}
            maskClosable={false}
            style={{top: 20}}
          >
            <Form {...layout}>
              <Row gutter={8}>
                <Col xs={24} sm={24} md={12}>
                  <FormInput label="Код" model={this.product} name="code" disabled/>
                </Col>
                <Col xs={24} sm={24} md={12}>
                  <FormInput label="Штрих код" model={this.product} name="barcode"/>
                </Col>
                <Col xs={24} sm={24} md={12}>
                  <Form.Item label='Изображение'>
                    <Uploader fileType={'product'} onUploaded={(img) => this.product.image = img.id}/>
                  </Form.Item>
                  {this.product.image &&
                  <Form.Item {...tailLayout}>
                    <ImageView height={80} id={this.product.image} thumbnail onRemove={() => {
                      commonApi.deleteImage(this.product.image).then(r => this.product.image = null);
                    }}/>
                  </Form.Item>
                  }
                </Col>
                <Col xs={24} sm={24} md={12}>
                  <Form.Item label='Доп. изображения'>
                    <Uploader fileType={'product'} onUploaded={img => {
                      if (!this.product.images) {
                        this.product.images = [];
                      }
                      this.product.images.push(img.id);
                    }}/>
                  </Form.Item>

                  {this.product.image &&
                  <Form.Item {...tailLayout}>
                    {this.product.images && this.product.images.map(id =>
                      <ImageView
                        height={80} key={id} id={id}
                        thumbnail
                        onRemove={() => {
                          commonApi.deleteImage(id).then(r => {
                            this.product.images = this.product.images.filter(im => im !== id);
                          });
                        }}
                      />)}
                  </Form.Item>
                  }
                </Col>
                <Col xs={24} sm={24} md={12}>
                  <Form.Item label='Отечественный'>
                    <Checkbox model={this.product} name='local'/>
                  </Form.Item>
                </Col>

                {this.product.values.map(e =>
                  <Col xs={24} sm={24} md={12} key={e.entity_id}>
                    <Form.Item label={e.label}>
                      <Select
                        showSearch
                        optionFilterProp="children"
                        value={e.value_id}
                        filterOption={(input, option) =>
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                        onChange={value => e.value_id = value}
                      >
                        {e.values.map((item, idx) => (<Option key={idx} value={item.id}>{item.name}</Option>))}
                      </Select>
                    </Form.Item>
                  </Col>,
                )}
              </Row>
            </Form>
          </Modal>

          <ProductImageView show={this.showImage}
                            product={this.cProduct}
                            onClose={() => {
                              runInAction(() => {
                                this.showImage = false;
                                this.cProduct = null;
                              });
                            }}
          />
        </Wrapper>
      </>
    );
  }
}
