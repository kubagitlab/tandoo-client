import React from 'react';
import {PageHeader} from 'antd';
import {useHistory} from 'react-router-dom';
import ProductList from "../../components/product/ProductListPublic";

export default () => {
  const history = useHistory();

  return (
    <>
      <PageHeader onBack={() => history.push('/sup/catalog')} title="Добавление товара"/>
      <ProductList/>
    </>
  );
};
