import React, {useEffect, useState} from 'react';
import {Card, Form, PageHeader} from 'antd';
import supApi from '../../../../apis/supplier';
import FormInput from '../../components/FormInput';
import Button from '../../components/Button';
import {useHistory, useParams} from 'react-router-dom';
import DatePicker from '../../components/DatePicker';
import commonApi from '../../../../apis/common';
import ProductView from "../../components/product/ProductView";
import moment from "moment";
import appStore from "../../../../store/AppStore";

export default () => {
  const history = useHistory();
  const {id} = useParams();
  const [form] = Form.useForm();
  const [product, setProduct] = useState(null);
  const [date_end, setDate] = useState(null);

  useEffect(() => {
    commonApi.getProduct(id).then((r) => {
      setProduct(r.product);
      if (r.product.company_product) {
        form.setFieldsValue({
          unit_price: r.product.company_product['unit_price'],
          date_end: r.product.company_product['date_end'],
        });
      }
    });
  }, []);

  function onDateChange(date) {
    setDate(date);
  }

  const onAddProduct = () => {
    let params = {};
    params['product_id'] = product.id;
    params['unit_price'] = form.getFieldValue('unit_price');
    let dt = form.getFieldValue('date_end');
    if (dt) {
      params['date_end'] = form.getFieldValue('date_end').format('YYYY-MM-DD HH:mm');
    }else{
      appStore.setAlert("error", "Введите срок истечения цены");
    }

    supApi.saveProduct(params).then(r => {
      history.push('/sup/catalog');
    });
  };

  const max60days = moment().add(60, 'days');

  return (
    <>
      <PageHeader onBack={() => history.push('/sup/catalog')} title="Добавление/редактирование продукции"/>
      {product &&
      <Card size="small">
        <ProductView id={product.id}/>
        <Form form={form} name="control-hooks">
          <FormInput label="Цена за единицу, сом" name="unit_price"/>
          <Form.Item label="Срок истечения цены" name="date_end">
            <DatePicker onChange={onDateChange}
                        value={date_end}
                        showTime
                        disabledDate={d => !d || d.isAfter(max60days)}/>
          </Form.Item>
          <Button onClick={() => onAddProduct()}>Сохранить и добавить в Мой каталог</Button>
        </Form>
      </Card>
      }
    </>
  );
};
