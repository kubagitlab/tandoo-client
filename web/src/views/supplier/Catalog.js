import React, {useEffect, useState} from 'react';
import Button from '../../components/Button';
import {Card, Modal, PageHeader, Tooltip} from 'antd';
import {useHistory} from 'react-router-dom';
import supApi from '../../../../apis/supplier';
import {formatDateTime} from '../../../../common/utils';
import {MdEdit} from 'react-icons/md';
import ReactTable from '../../components/ReactTable';
import ImageView from "../../components/ImageView";
import ProductView from "../../components/product/ProductView";

export default () => {
  const history = useHistory();
  const [catalog, setCatalog] = useState([]);
  const [productId, setProduct] = useState(null);

  useEffect(() => {
    supApi.getCatalog().then((r) => {
      setCatalog(r.catalog);
    });
  }, []);

  const editProduct = (product) => {
    history.push('/sup/product/add/' + product.product_id);
  };

  const columns = [
    {
      Header: 'Категория',
      accessor: 'category',
    },
    {
      Header: 'Товар (Торг.знак-Страна)',
      id: 'name',
      accessor: row => row['brand'] + (row['country'] ? (' - ' + row['country']) : '')
    },
    {
      Header: 'Ед.изм',
      accessor: 'measure_unit',
    },
    {
      Header: 'Внешний вид',
      accessor: 'image',
      Cell: ({original}) => <ImageView id={original.product.image} thumbnail width={50}
                                       onClick={() => setProduct(original.product.id)}/>,
    },
    {
      Header: 'Цена за ед.',
      accessor: 'unit_price',
    },
    {
      Header: 'Добавлен',
      accessor: 'date_add',
      Cell: ({value}) => formatDateTime(value),
    },
    {
      Header: 'Срок истечения даты',
      accessor: 'date_end',
      Cell: ({value}) => formatDateTime(value),
    },
    {
      Header: 'Статус',
      accessor: 'role',
    },
    {
      Header: '',
      accessor: 'edit',
      width: 30,
      Cell: ({original}) => <Tooltip placement="right" title="Редактировать">
        <Button onClick={() => editProduct(original)} size="small" type="default" icon={<MdEdit/>}/>
      </Tooltip>,
    },
  ];

  return (
    <>
      <PageHeader title="Мой каталог"
                  extra={<Button onClick={() => history.push('/sup/product/list')}>Добавить товар</Button>}/>
      <Card size="small">
        <ReactTable data={catalog} columns={columns} pageSize={20}/>
      </Card>
      <Modal visible={!!productId}
             onOk={() => setProduct(null)}
             onCancel={() => setProduct(null)}
             width="90%"
             style={{top: 20}}
             footer={null}
      >
        <ProductView id={productId}/>
      </Modal>
    </>
  );
};
