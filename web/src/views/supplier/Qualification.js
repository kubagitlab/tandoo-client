import React from 'react';
import {inject, observer} from "mobx-react";
import Button from "../../components/Button";
import {Card, Col, Form, Modal, PageHeader, Radio, Result, Row, Skeleton} from "antd";
import supApi from "../../../../apis/supplier";
import {observable} from "mobx";
import FormInput, {FormDate, FormHiddenInput, FormSelect, FormUpload} from "../../components/FormInput";
import commonApi from "../../../../apis/common";
import ReactTable from "../../components/ReactTable";
import {MdAdd, MdDelete} from "react-icons/md";
import {formatDate, formatDateDB} from "../../../../common/utils";
import FileView from "../../components/FileView";

const layout = {labelCol: {span: 12}, wrapperCol: {span: 12},};

@inject('appStore')
@observer
export default class Qualification extends React.Component {

  @observable residency = 'resident'
  @observable ownership_type = 'ip';
  @observable info;
  @observable ownerships = [];
  @observable data = {SvidReg: [], Ustav: [], SvedOpost: [], BuhBalance: [], DopDocs: []};

  componentDidMount() {
    const {companyId} = this.props;
    let params;
    if (companyId) {
      params = {company_id: companyId};
    }
    commonApi.getCompanyInfo(params).then(r => {
      this.info = r.info;
      if (r.info.qualification) {
        this.data = r.info.qualification.data;
        this.residency = this.data.residency;
        this.ownership_type = this.data.ownership_type;
      }
    });
    commonApi.getDir('DirOwnership').then(r => this.ownerships = r);
  }

  save = () => {
    const params = {id: this.info.id, ownership_type: this.ownership_type, residency: this.residency, ...this.data}
    supApi.qualification(params).then(r => {
      const {appStore} = this.props;
      if (appStore.isSupplier) {
        appStore.setAlert('success', 'Данные успешно отправлены на проверку');
      }
    });
  }

  render() {
    if (!this.info) {
      return <Skeleton active/>
    }
    const {appStore} = this.props;
    const {user} = appStore;

    return (
      <>
        {appStore.isSupplier &&
        <PageHeader title="Предквалификация" extra={<Button onClick={this.save}>Сохранить</Button>}/>}

        {this.data.reject_reason && <Result status="warning" title={this.data.reject_reason}/>}

        <Row gutter={[10, 10]}>
          <Col>
            <Radio.Group value={this.residency} buttonStyle="solid"
                         disabled={appStore.isAdmin}
                         onChange={e => this.residency = e.target.value}>
              <Radio.Button value='resident'>Резиденты КР</Radio.Button>
              <Radio.Button value='nonresident'>Не резидент КР</Radio.Button>
            </Radio.Group>
          </Col>
          {this.residency === 'resident' && <Col>
            <Radio.Group value={this.ownership_type} buttonStyle="solid"
                         disabled={appStore.isAdmin}
                         onChange={e => this.ownership_type = e.target.value}>
              <Radio.Button value="ip">Индивидуальный предприниматель</Radio.Button>
              <Radio.Button value="org">Организация</Radio.Button>
            </Radio.Group>
          </Col>}
        </Row>

        <Row gutter={[10, 10]}>
          <Col md={12} sm={24} lg={12} xs={24}>
            <MainInfo user={user} info={this.info} ownerships={this.ownerships} ownership_type={this.ownership_type}/>
          </Col>
          <Col md={12} sm={24} lg={12} xs={24}>
            <Rukovoditel data={this.info} ownership_type={this.ownership_type}/>
            <BankInfo bankInfo={this.info.bank}/>
          </Col>
        </Row>
        <Row gutter={[10, 10]}>
          <Col md={12} sm={24} lg={12} xs={24}>
            <SvidReg items={this.data.SvidReg} onChange={items => this.data.SvidReg = items}/>
          </Col>
          <Col md={12} sm={24} lg={12} xs={24}>
            {this.ownership_type === 'org' &&
            <Ustav items={this.data.Ustav} onChange={items => this.data.Ustav = items}/>
            }
          </Col>
        </Row>
        <Row gutter={[10, 10]}>
          <Col md={24} sm={24} lg={24} xs={24}>
            <SvedOpost items={this.data.SvedOpost} onChange={items => this.data.SvedOpost = items}/>
          </Col>
        </Row>
        <Row gutter={[10, 10]}>
          <Col md={12} sm={24} lg={12} xs={24}>
            <BuhBalance items={this.data.BuhBalance} onChange={items => this.data.BuhBalance = items}/>
          </Col>
          <Col md={12} sm={24} lg={12} xs={24}>
            <DopDocs items={this.data.DopDocs} onChange={items => this.data.DopDocs = items}/>
          </Col>
        </Row>
        {!appStore.isAdmin && <Button onClick={this.save}>Сохранить</Button>}
        <br/>
      </>
    );
  }
}

const MainInfo = ({info, ownerships, ownership_type, user}) => {

  const isIP = ownership_type === 'ip';
  let os;
  if (isIP) {
    os = ownerships.filter(o => o.type_owner === 'INDIV_BUZ');
  } else {
    os = ownerships.filter(o => o.type_owner !== 'INDIV_BUZ');
  }

  return (
    <Card title='Основные данные' size='small'>
      <Form {...layout}>
        <FormSelect label={'Организационно правовая форма'} rules={[{required: true}]} data={os} valueKey={'id'}
                    labelKey={'name'} disabled/>
        {isIP && <>
          <FormInput label='ИНН' model={user} name={'inn'} rules={[{required: true}]} disabled/>
          <FormInput label='ФИО' model={user} name={'fullname'} rules={[{required: true}]} disabled/>
        </>}
        {!isIP && <>
          <FormInput label='ИНН организации' model={info} name={'inn'} rules={[{required: true}]} disabled/>
          <FormInput label='Наименование организации' model={info} name={'titleRu'} rules={[{required: true}]}
                     disabled/>
        </>}

        <FormInput label='Сокращенное наименование' model={info} name={'short_name'} rules={[{required: true}]}
                   disabled/>
        <FormInput label='Email' model={user} name={'email'} rules={[{required: true}]} disabled/>
        <FormInput label='Контактный телефон' model={info} name={'workPhone'} disabled/>
        <FormInput label='Населенный пункт' model={info} name={'coate'} rules={[{required: true}]} disabled/>
        <FormInput label='Юридический адрес' model={info} name={'legalAddress'} rules={[{required: true}]} disabled/>
        <FormInput label='Фактический адрес' model={info} name={'factAddress'} rules={[{required: true}]} disabled/>
        <FormInput label='Улица' model={info} name={'street'} rules={[{required: true}]} disabled/>
        <FormInput label='№ дома' model={info} name={'house'} rules={[{required: true}]} disabled/>
        <FormInput label='Квартира' model={info} name={'apt'} disabled/>
      </Form>
    </Card>
  )
}

const Rukovoditel = ({data, ownership_type}) => {
  if (ownership_type === 'ip') return null;
  return (
    <Card title='Сведения о руководителе' size='small' style={{marginBottom: 10}}>
      <Form {...layout}>
        <FormInput label='ПИН' model={data} name="owner_inn" rules={[{required: true}]} disabled/>
        <FormInput label='ФИО' model={data} name="owner_fio" rules={[{required: true}]} disabled/>
        <FormInput label='Должность' model={data} name="owner_pos" rules={[{required: true}]} disabled/>
        <FormInput label='Email' model={data} name="owner_email" rules={[{required: true}]} disabled/>
        <FormInput label='Номер моб. телефона' model={data} name="owner_phone" rules={[{required: true}]} disabled/>
      </Form>
    </Card>
  )
}

const BankInfo = ({bankInfo}) => {
  const {bank} = bankInfo;
  return (
    <Card title='Банковские реквизиты' size='small'>
      <Form {...layout}>
        <FormInput label='Банк' model={bank} name="name" rules={[{required: true}]} disabled/>
        <FormInput label='Номер расчетного счета' model={bankInfo} name="accountNumber" rules={[{required: true}]}
                   disabled/>
        <FormInput label='БИК' model={bankInfo} name="bik" rules={[{required: true}]} disabled/>
        <FormInput label='Код ОКПО' model={bankInfo} name="okpo" rules={[{required: true}]} disabled/>
      </Form>
    </Card>
  )
}

const SvidReg = ({items, onChange}) => {
  const [form] = Form.useForm();
  const [visible, setVisible] = React.useState(false);
  const [file, setFile] = React.useState(null);

  const onSave = () => {
    form.validateFields()
      .then(values => {
        let data = {...values};
        data.certIssueDate = formatDateDB(data.certIssueDate)
        items.push(data);
        onChange(items);
        setFile(null);
        form.resetFields();
        setVisible(false);
      })
      .catch(errorInfo => {
      });
  };

  return (
    <Card title='Свидетельство о регистрации' size='small'
          extra={<Button onClick={() => setVisible(true)}><MdAdd/></Button>}>

      <ReactTable data={items} columns={[
        {Header: "Серия и номер", accessor: "certNumber"},
        {Header: "Дата выдачи", accessor: "certIssueDate", Cell: ({value}) => <span>{formatDate(value)}</span>},
        {Header: "Орган выдавший", accessor: "certIssuer",},
        {
          Header: "Файл",
          accessor: "file_name",
          Cell: ({original}) => <FileView id={original.file_id} fileName={original.file_name}/>
        },
        {
          Header: "",
          accessor: "qq",
          width: 50,
          Cell: ({index}) => <Button danger size='small' onClick={() => {
          }}>
            <MdDelete/>
          </Button>
        },
      ]} pageSize={5}/>

      <Modal
        title="Свидетельство о регистрации"
        visible={visible}
        onOk={onSave}
        width={800}
        okText="Сохранить"
        onCancel={() => setVisible(false)}
        maskClosable={false}
        style={{top: 20}}
      >
        <Form {...layout} form={form}>
          <FormInput label='Серия и номер Свидетельства' name="certNumber" rules={[{required: true}]}/>
          <FormDate label='Дата выдачи Свидетельства' name="certIssueDate" rules={[{required: true}]}/>
          <FormInput label='Орган выдавший Свидетельство' name="certIssuer" rules={[{required: true}]}/>
          <FormUpload label='Файл' name="file_name" rules={[{required: true}]}
                      uploaderProps={{
                        fileType: 'qualification', notImage: true, accept: '*', onUploaded: (file) => {
                          form.setFieldsValue({file_name: file.file_name});
                          form.setFieldsValue({file_id: file.id});
                          setFile(file);
                        }
                      }}/>
          {file && <FileView id={file.id} fileName={file.file_name}/>}
          <FormHiddenInput name='file_id'/>
        </Form>
      </Modal>
    </Card>
  )
}

const Ustav = ({items, onChange}) => {
  const [visible, setVisible] = React.useState(false);
  const [file, setFile] = React.useState(null);
  const [form] = Form.useForm();

  const onSave = () => {
    form.validateFields()
      .then(values => {
        let data = {...values};
        items.push(data);
        onChange(items);
        form.resetFields();
        setFile(null);
        setVisible(false);
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      });
  };

  return (
    <Card title='Устав' size='small'
          extra={<Button onClick={() => {
            setVisible(true);
            setFile(null);
          }}><MdAdd/>
          </Button>}>

      <ReactTable data={items} pageSize={5}
                  columns={[
                    {
                      Header: "Файл",
                      accessor: "file_name",
                      Cell: ({original}) => <FileView id={original.file_id} fileName={original.file_name}/>
                    },
                    {
                      Header: "",
                      accessor: "qq",
                      width: 50,
                      Cell: ({index}) => <Button danger size='small' onClick={() => {
                      }}>
                        <MdDelete/>
                      </Button>
                    },]}/>

      <Modal
        title="Устав"
        visible={visible}
        onOk={onSave}
        width={500}
        okText="Сохранить"
        onCancel={() => setVisible(false)}
        maskClosable={false}
        style={{top: 20}}
      >
        <Form {...layout} form={form}>
          <FormUpload label='Наименование документа' name="file_name" rules={[{required: true}]}
                      uploaderProps={{
                        fileType: 'qualification', notImage: true, accept: '*', onUploaded: (file) => {
                          form.setFieldsValue({file_name: file.file_name});
                          form.setFieldsValue({file_id: file.id});
                          setFile(file);
                        }
                      }}/>
          {file && <FileView id={file.id} fileName={file.file_name}/>}
          <FormHiddenInput name='file_id'/>
        </Form>
      </Modal>
    </Card>
  )
}

const SvedOpost = ({items, onChange}) => {
  const [visible, setVisible] = React.useState(false);
  const [file, setFile] = React.useState(null);
  const [form] = Form.useForm();

  const onSave = () => {
    form.validateFields()
      .then(values => {
        let data = {...values};
        data.date_contract = formatDateDB(data.date_contract)
        items.push(data);
        onChange(items);
        setFile(null);
        form.resetFields();
        setVisible(false);
      })
      .catch(errorInfo => {
      });
  };

  return (
    <Card title='Сведения о выполненных поставках товаров' size='small'
          extra={<Button onClick={() => setVisible(true)}><MdAdd/></Button>}>

      <ReactTable data={items} pageSize={5} columns={[
        {Header: "Наименование товара", accessor: "goods_type"},
        {
          Header: "Дата выполнения договора",
          accessor: "date_contract",
          Cell: ({value}) => <span>{formatDate(value)}</span>
        },
        {
          Header: "Покупатель (наименование,адрес,контактные телефоны)",
          accessor: "buyer_info",
        },
        {Header: "Стоимость договора, сом", accessor: "cost",},
        {
          Header: "Файл", accessor: "file_id",
          Cell: ({original}) => <FileView id={original.file_id} fileName={original.file_name}/>
        },
        {
          Header: "",
          accessor: "qq",
          width: 50,
          Cell: ({index}) => <Button danger size='small' onClick={() => {
            let i = [...items];
            i = i.splice(index, 1)
            setItems(i);
          }}>
            <MdDelete/>
          </Button>
        },
      ]}/>

      <Modal
        title="Сведения о выполненных поставках товаров"
        visible={visible}
        onOk={onSave}
        width={800}
        okText="Сохранить"
        onCancel={() => setVisible(false)}
        maskClosable={false}
      >
        <Form {...layout} form={form}>
          <FormInput label='Наименование договора' name="goods_type" rules={[{required: true}]}/>
          <FormDate label='Дата выполнения договора' name="date_contract" rules={[{required: true}]}/>
          <FormInput label='Покупатель (наименование, адрес, контактные телефоны)' name="buyer_info"
                     rules={[{required: true}]}/>
          <FormInput label='Стоимость договора, тыс. сом' name="cost" rules={[{required: true}]}/>

          <FormUpload label='Прикрепить договор или счет-фактуру' name="file_name" rules={[{required: true}]}
                      uploaderProps={{
                        fileType: 'qualification', notImage: true, accept: '*', onUploaded: (file) => {
                          form.setFieldsValue({file_name: file.file_name});
                          form.setFieldsValue({file_id: file.id});
                          setFile(file);
                        }
                      }}/>
          {file && <FileView id={file.id} fileName={file.file_name}/>}
          <FormHiddenInput name='file_id'/>
        </Form>
      </Modal>
    </Card>
  )
}

const BuhBalance = ({items, onChange}) => {
  const [visible, setVisible] = React.useState(false);
  const [file, setFile] = React.useState(null);
  const [form] = Form.useForm();

  const onSave = () => {
    form.validateFields()
      .then(values => {
        let data = {...values};
        items.push(data);
        onChange(items);
        setFile(null);
        form.resetFields();
        setVisible(false);
      })
      .catch(errorInfo => {
      });
  };

  return (
    <Card title='Бухгалтерский баланс и ЕНД' size='small'
          extra={<Button onClick={() => setVisible(true)}><MdAdd/></Button>}>

      <ReactTable data={items} pageSize={5} columns={[
        {Header: "Наименование документа", accessor: "fin_name"},
        {
          Header: "Файл", accessor: "file_name",
          Cell: ({original}) => <FileView id={original.file_id} fileName={original.file_name}/>
        },
        {
          Header: "",
          accessor: "qq",
          width: 50,
          Cell: ({index}) => <Button danger size='small' onClick={() => {
            let i = [...items];
            i = i.splice(index, 1)
            setItems(i);
          }}>
            <MdDelete/>
          </Button>
        },
      ]}/>

      <Modal
        title="Бухгалтерский баланс и ЕНД"
        visible={visible}
        onOk={onSave}
        width={800}
        okText="Сохранить"
        onCancel={() => setVisible(false)}
        maskClosable={false}
        style={{top: 20}}
      >
        <Form {...layout} form={form}>
          <FormInput label='Наименование документа' name="fin_name" rules={[{required: true}]}/>
          <FormUpload label='Файл' name="file_name" rules={[{required: true}]}
                      uploaderProps={{
                        fileType: 'qualification', notImage: true, accept: '*', onUploaded: (file) => {
                          form.setFieldsValue({file_name: file.file_name});
                          form.setFieldsValue({file_id: file.id});
                          setFile(file);
                        }
                      }}/>
          {file && <FileView id={file.id} fileName={file.file_name}/>}
          <FormHiddenInput name='file_id'/>
        </Form>
      </Modal>
    </Card>
  )
}

const DopDocs = ({items, onChange}) => {
  const [visible, setVisible] = React.useState(false);
  const [file, setFile] = React.useState(null);
  const [form] = Form.useForm();

  const onSave = () => {
    form.validateFields()
      .then(values => {
        let data = {...values};
        items.push(data);
        onChange(items);
        setFile(null);
        form.resetFields();
        setVisible(false);
      })
      .catch(errorInfo => {
      });
  };

  return (
    <Card title='Дополнительные документы' size='small'
          extra={<Button onClick={() => setVisible(true)}><MdAdd/></Button>}>

      <ReactTable data={items} pageSize={5} columns={[
        {Header: "Наименование документа", accessor: "doc_name"},
        {
          Header: "Файл", accessor: "file_name",
          Cell: ({original}) => <FileView id={original.file_id} fileName={original.file_name}/>
        },
        {
          Header: "",
          accessor: "qq",
          width: 50,
          Cell: ({index}) => <Button danger size='small' onClick={() => {
          }}>
            <MdDelete/>
          </Button>
        },
      ]}/>

      <Modal
        title="Дополнительные документы"
        visible={visible}
        onOk={onSave}
        width={800}
        okText="Сохранить"
        onCancel={() => setVisible(false)}
        maskClosable={false}
        style={{top: 20}}
      >
        <Form {...layout} form={form}>
          <FormInput label='Наименование документа' name="doc_name" rules={[{required: true}]}/>
          <FormUpload label='Файл' name="file_name" rules={[{required: true}]}
                      uploaderProps={{
                        fileType: 'qualification', notImage: true, accept: '*', onUploaded: (file) => {
                          form.setFieldsValue({file_name: file.file_name});
                          form.setFieldsValue({file_id: file.id});
                          setFile(file);
                        }
                      }}/>
          {file && <FileView id={file.id} fileName={file.file_name}/>}
          <FormHiddenInput name='file_id'/>
        </Form>
      </Modal>
    </Card>
  )
}
