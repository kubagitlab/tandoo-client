import React from 'react';
import {PageHeader, Tabs, Tag} from 'antd';
import ReactTable from '../../components/ReactTable';
import {_amountFormat, formatDateTime} from '../../../../common/utils';

export default () => (
  <>
    <PageHeader title="Мои заявки"/>
    <div className="card-container">
      <Tabs defaultActiveKey="1" type="card">

        <Tabs.TabPane tab="Предложения / Заявки" key="1">
          <Applications/>
        </Tabs.TabPane>

        <Tabs.TabPane tab="Проекты" key="2">
          <Projects/>
        </Tabs.TabPane>

        <Tabs.TabPane tab="Запросы" key="3">
          <Request/>
        </Tabs.TabPane>
      </Tabs>
    </div>
  </>
);

const Applications = () => {
  const columnAplications = [
    {
      Header: '№ объявления',
      accessor: 'code',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Статус',
      accessor: 'status',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => <Tag color="green">{value}</Tag>,
    },
    {
      Header: 'Наимененование объявления',
      accessor: 'dirsection',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Закупающая организация',
      accessor: 'pur_company',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Дата публикации объявления',
      accessor: 'published_date',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => formatDateTime(value),
    },
    {
      Header: 'Срок подачи заявок',
      accessor: 'deadline',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => formatDateTime(value),
    },
  ];
  const dataAplications = [
    {
      code: '20081074341',
      deadline: '2020-08-10 15:12:46',
      dirsection: 'Изделия медицинского назначения',
      published_date: '2020-08-10 14:12:12',
      pur_company: 'Общество с огранич. ответственностью Международный центр научно-технической информации',
      status: 'Evaluation',
    },

  ];
  return (
    <ReactTable data={dataAplications} columns={columnAplications} pageSize={10}/>
  );
};

const Projects = () => {
  const columnProjects = [
    {
      Header: '№ объявления',
      accessor: 'code',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Статус',
      accessor: 'status',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => <Tag color="green">{value}</Tag>,
    },
    {
      Header: 'Наимененование объявления',
      accessor: 'dirsection',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Закупающая организация',
      accessor: 'pur_company',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Дата подачи',
      accessor: 'published_date',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => formatDateTime(value),
    },
  ];
  const dataProjects = [
    {
      code: '20081074341',
      deadline: '2020-08-10 15:12:46',
      dirsection: 'Изделия медицинского назначения',
      published_date: '2020-08-10 14:12:12',
      pur_company: 'Общество с огранич. ответственностью Международный центр научно-технической информации',
      status: 'Evaluation',
    },

  ];
  return (
    <ReactTable data={dataProjects} columns={columnProjects} pageSize={10}/>
  );
};

const Request = () => {
  const columnRequest = [
    {
      Header: '№ объявления',
      accessor: 'code',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Статус',
      accessor: 'status',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => <Tag color="green">{value}</Tag>,
    },
    {
      Header: 'Метод закупок',
      accessor: 'method',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Организация',
      accessor: 'pur_company',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Планируемая сумма',
      accessor: 'amount_plan',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => _amountFormat(value),
    },
    {
      Header: 'Дата публикации',
      accessor: 'published_date',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => formatDateTime(value),
    },
    {
      Header: 'Срок подачи заявки',
      accessor: 'deadline',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => formatDateTime(value),
    },
  ];
  const dataRequest = [
    {
      code: '20081074341',
      deadline: '2020-08-10 15:12:46',
      dirsection: 'Изделия медицинского назначения',
      published_date: '2020-08-10 14:12:12',
      pur_company: 'Общество с огранич. ответственностью Международный центр научно-технической информации',
      status: 'Evaluation',
      amount_plan: 200000,
      method: 'Прямой',
    },

  ];
  return (
    <ReactTable data={dataRequest} columns={columnRequest} pageSize={10}/>
  );
};
