import React, {useEffect, useState} from 'react';
import {Avatar, Card, Col, Divider, Form, PageHeader, Row, Select, Tabs, Typography, Upload} from 'antd';
import Table from '../../components/Table';
import Input from '../../components/Input';
import Button from '../../components/Button';
import supApi from '../../../../apis/supplier';
import {dateTimeFormat} from '../../../../common/utils';

const {Text, Title} = Typography;
const {Meta} = Card;
const {TabPane} = Tabs;

const columnsEmployees = [
  {
    title: 'Инн',
    dataIndex: 'inn',
    key: 'inn',
  },
  {
    title: 'ФИО',
    dataIndex: 'fio',
    key: 'fio',
  },
  {
    title: 'Роль',
    dataIndex: 'role',
    key: 'role',
  },
  {
    title: 'Дата окончание',
    dataIndex: 'dateEnd',
    key: 'dateEnd',
  },
];

const dataEmployees = [];
for (let i = 0; i < 20; i++) {
  dataEmployees.push({
    key: i,
    inn: `${i}2106201010103`,
    fio: 'Жамакеев Тимур Калысбекович\n',
    role: 'Генеральный директор',
    dateEnd: '15.06.2020 17:23',
  });
}

export default () => {
  return (
    <>
      <PageHeader title="Профиль"/>
      <Card size="small">
        <Tabs defaultActiveKey="1" tabPosition="left">
          <TabPane tab="Основные данные" key="1">
            <MainInfo/>
          </TabPane>
          <TabPane tab="Информация о задолженности" key="2">
            <DebtInfo/>
          </TabPane>
          <TabPane tab="Банковские реквизиты" key="3">
            <BankInfo/>
          </TabPane>
          <TabPane tab="Сотрудники" key="4">
            <Employees/>
          </TabPane>
          <TabPane tab="Архив" key="5">
            <Archive/>
          </TabPane>
        </Tabs>
      </Card>
    </>
  );
};

const MainInfo = () => {
  const [info, setInfo] = useState({});

  useEffect(() => {
    supApi.getSupMainInfo({'companyInn': '02106201010103', 'userPin': '21410197100489'}).then((r) => {
      setInfo(r.sup_info);
    });
  }, []);

  const saveInfoChanges = () => {
    supApi.updateSupMainInfo(info).then((r) => {
      setInfo(r.sup_info);
    });
  };

  const changeHandler = () => {
    setInfo(state => ({...state}));
  };

  return (
    <>
      <Meta
        avatar={
          <Avatar
            size={64}
            src="https://sigalartstudio.ru/wp-content/uploads/2018/10/noavatar.png"
          />
        }
        title='Almetico" ("Алметико")'
        description="ОсОО"
      />
      <Divider/>
      <Form layout="vertical">
        <Row gutter={24}>
          <Col sm={24} md={8}>
            <Form.Item label="ИНН организации">
              {info && <Input disabled value={info.inn}/>}
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label="Наименование организации">
              {info && <Input disabled value={info.name}/>}
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item
              label="Свидетельство о регистрации юр. лица"
              valuePropName="fileList">
              <Upload name="logo" action="#" listType="picture">
                <Button type="default">Выбрать</Button>
              </Upload>
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label="Email">
              {info.data && <Input model={info.data} name={'email'} onChange={changeHandler}/>}
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label="Номер моб. телефона">
              {info.data && <Input model={info.data} name={'mobilePhone'} onChange={changeHandler} type={'number'}/>}
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label="Населенный пункт">
              {info.data && <Input model={info.data} name={'district'} onChange={changeHandler}/>}
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label="Улица">
              {info.data && <Input model={info.data} name={'street'} onChange={changeHandler}/>}
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label="№ дома">
              {info.data && <Input model={info.data} name={'house'} onChange={changeHandler}/>}
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label="Квартира">
              {info.data && <Input model={info.data} name={'flat'} onChange={changeHandler}/>}
            </Form.Item>
          </Col>
        </Row>
        <Button onClick={saveInfoChanges}>Сохранить</Button>
      </Form>
    </>
  );
};

const DebtInfo = () => {
  const [debts, setDebts] = useState([]);

  useEffect(() => {
    supApi.getDebts({inn: '02106201010103'}).then((r) => {
      setDebts(r.debts);
    });
  }, []);

  const sendDebtRequest = () => {
    supApi.sendDebtRequest({inn: '02106201010103'}).then((r) => {
      setDebts(r.debts);
    });
  };

  const columns = [
    {
      title: 'Дата подачи',
      dataIndex: 'date_start',
      key: 'date_start',
      render: (dt) => <>{dateTimeFormat(dt)}</>,
    },
    {
      title: 'ИНН',
      dataIndex: 'inn',
      key: 'inn',
    },
    {
      title: 'Наименование организации',
      dataIndex: 'company',
      key: 'company',
    },
    {
      title: 'Орган выдавший',
      dataIndex: 'issuer',
      key: 'issuer',
    },
    {
      title: 'Дата',
      dataIndex: 'date_end',
      key: 'date_end',
    },
    {
      title: 'Задолжность',
      dataIndex: 'debt_status',
      key: 'debt_status',
    },
  ];

  return (
    <>
      <Row>
        <Col md={18}>
          <Title level={4}>Информация о задолженности</Title>
        </Col>
        <Col md={6} style={{textAlign: 'right'}}>
          <Button style={{marginBottom: 16}} onClick={() => sendDebtRequest()}>
            Запросить
          </Button>
        </Col>
      </Row>
      <Table data={debts} columns={columns}/>
    </>
  );
};

const BankInfo = () => {
  return (
    <>
      <Title level={4}>Банковские реквизиты</Title>
      <Form layout="vertical">
        <Row gutter={24}>
          <Col sm={24} md={8}>
            <Form.Item label="Банк">
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }>
                <Select.Option value="1">
                  ОАО "Коммерческий банк Кыргызстан"
                </Select.Option>
                <Select.Option value="2">
                  Первомайский ФОАО "Коммерческий банк Кыргызстан"
                </Select.Option>
                <Select.Option value="3">
                  Кара-Балтинский ФОАО "Коммерческий банк Кыргызстан"
                </Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label="Номер расчетного счета">
              <Input/>
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label="БИК">
              <Input/>
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label="Код ОКПО">
              <Input/>
            </Form.Item>
          </Col>
          <Col sm={24} md={8}>
            <Form.Item label=" ">
              <Button>Сохранить</Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </>
  );
};

const Employees = () => {
  return (
    <>
      <Title level={4}>Сотрудники</Title>
      <Form layout="vertical">
        <Row gutter={24}>
          <Col sm={24} md={12} lg={6}>
            <Form.Item label="Сотрудник">
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                <Select.Option value="1">Супер Админ uzenov.ilyas@gmail.com</Select.Option>
                <Select.Option value="2">Оператор Администрации operator1@mail.ru</Select.Option>
                <Select.Option value="3">Исабекова Айгуль Шаршеналиевна ai0011@mail.ru</Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col sm={24} md={12} lg={6}>
            <Form.Item label="Роль сотрудника">
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                <Select.Option value="1">Генеральный директор</Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col sm={24} md={12} lg={6}>
            <Form.Item label="Дата окончания">
              <Input/>
            </Form.Item>
          </Col>
          <Col sm={24} md={12} lg={6}>
            <Form.Item label=" ">
              <Button>Добавить</Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <Divider/>
      <Table data={dataEmployees} columns={columnsEmployees}/>
    </>
  );
};

const Archive = () => {
  return (
    <>
      <Title level={4}>Архив</Title>
    </>
  );
};
