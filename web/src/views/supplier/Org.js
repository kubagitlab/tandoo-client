import React from 'react';
import {Card, PageHeader, Tabs, Tag} from 'antd';
import ReactTable from '../../components/ReactTable';
import Button from '../../components/Button';


export default () => (
  <>
    <PageHeader title="Организации"
                extra={<Button>Добавить организацию</Button>}/>

    <div className="card-container">
      <Tabs defaultActiveKey="1" type="card">

        <Tabs.TabPane tab="Активные" key="1">
          <Active/>
        </Tabs.TabPane>

        <Tabs.TabPane tab="Неактивные" key="2">
          <Inactive />
        </Tabs.TabPane>
      </Tabs>

    </div>
  </>
);

const Active = () => {
  const columnsActive = [
    {
      Header: 'Организация',
      accessor: 'name',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Статус',
      accessor: 'company_status',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => <Tag color="green">{value}</Tag>,
    },
    {
      Header: 'Тип организации',
      accessor: 'company_type',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'ИНН',
      accessor: 'inn',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
  ];
  const dataActive = [
    {
      company_status: 'confirmed',
      company_type: 'supplier',
      data: {phone: '996700202020', email: 'shakitov@mail.ru ', address: {house: '', street: '', apt: ''}},
      inn: '01101201710147',
      main_doc_img: [],
      name: 'Общество с ограниченной ответственностью',
      owner_data: {email: 'shakitov@mail.ru ', inn: '21502197600617', fio: 'Джунушов Бакыт Аманжанович'},
    },
    {
      company_status: 'confirmed',
      company_type: 'supplier',
      data: {phone: '996700202020', email: 'shakitov@mail.ru ', address: {house: '', street: '', apt: ''}},
      inn: '01101201710147',
      main_doc_img: [],
      name: 'Общество с ограниченной ответственностью',
      owner_data: {email: 'shakitov@mail.ru ', inn: '21502197600617', fio: 'Джунушов Бакыт Аманжанович'},
    },
  ];

  return (
    <ReactTable data={dataActive} columns={columnsActive} pageSize={10}/>
  );
};

const Inactive = () => {
  const columnsInactive = [
    {
      Header: 'Организация',
      accessor: 'name',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'Статус',
      accessor: 'company_status',
      className: 'text-center',
      headerClassName: 'f-bold',
      Cell: ({value}) => <Tag color="green">{value}</Tag>,
    },
    {
      Header: 'Тип организации',
      accessor: 'company_type',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
    {
      Header: 'ИНН',
      accessor: 'inn',
      className: 'text-center',
      headerClassName: 'f-bold',
    },
  ];
  const dataInactive = [
    {
      company_status: 'confirmed',
      company_type: 'supplier',
      data: {phone: '996700202020', email: 'shakitov@mail.ru ', address: {house: '', street: '', apt: ''}},
      inn: '01101201710147',
      main_doc_img: [],
      name: 'Общество с ограниченной ответственностью',
      owner_data: {email: 'shakitov@mail.ru ', inn: '21502197600617', fio: 'Джунушов Бакыт Аманжанович'},
    },
    {
      company_status: 'confirmed',
      company_type: 'supplier',
      data: {phone: '996700202020', email: 'shakitov@mail.ru ', address: {house: '', street: '', apt: ''}},
      inn: '01101201710147',
      main_doc_img: [],
      name: 'Общество с ограниченной ответственностью',
      owner_data: {email: 'shakitov@mail.ru ', inn: '21502197600617', fio: 'Джунушов Бакыт Аманжанович'},
    },
  ];

  return (
    <ReactTable data={dataInactive} columns={columnsInactive} pageSize={10}/>
  );
};
