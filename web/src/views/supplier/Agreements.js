import React from 'react';
import {Card, PageHeader, Tag} from 'antd';
import ReactTable from '../../components/ReactTable';
import {_amountFormat} from '../../../../common/utils';

const columns = [
  {
    Header: '№ Договора',
    accessor: 'number',
    className:'text-center',
    headerClassName:'f-bold',
  },
  {
    Header: 'Закупающая организация',
    accessor: 'organization',
    className:'text-center',
    headerClassName:'f-bold',
  },
  {
    Header: 'Предмет закупки',
    accessor: 'product',
    className:'text-center',
    headerClassName:'f-bold',
  },
  {
    Header: 'Поставщик',
    accessor: 'supplier',
    className:'text-center',
    headerClassName:'f-bold',
  },
  {
    Header: 'Статус',
    accessor: 'status',
    className:'text-center',
    headerClassName:'f-bold',
    Cell: ({value}) => <Tag color="green">{value}</Tag>,
  },
  {
    Header: 'Сумма',
    accessor: 'amount',
    className:'text-center',
    headerClassName:'f-bold',
  },
];
const docs = [
  {
    number: '12341234',
    organization: 'Osoo Vim Pil Dan',
    product: 'Apples',
    supplier:'MP Tazalyk',
    status:'active',
    amount: _amountFormat(10000)
  },
  {
    number: '23423523',
    organization: 'Osoo Bishkek sut',
    product: 'Moloko',
    supplier:'CHP Custom Boy',
    status:'pending',
    amount: _amountFormat(10000)
  },
];

export default () => (

  <>
    <PageHeader title="Мои договора"/>
    <Card size="small">
      <ReactTable data={docs} columns={columns} pageSize={10}/>
    </Card>
  </>
)
