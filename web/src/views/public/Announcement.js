import React from 'react';
import {Col, Descriptions, Layout, PageHeader, Pagination, Row, Tabs, Tag, Typography} from 'antd';
import Input from '../../components/Input';
import {MdSearch} from 'react-icons/md';

const {Title, Text} = Typography;

const published = [];
for (let i = 0; i < 4; i++) {
  published.push({
    number: `${i}006022${i}305081${i}`,
    status: 'Опубликовано',
    amount: `22${i} 600 000`,
    organization: 'Учебный центр Министерства Финансов КР',
    buyType: 'Канцелярские товары',
    releaseDate: `${i} мая 2020 (21:${i})`,
    lotCount: i,
    endDate: `${i + 2} мая 2020 (10:41)`,
    method: 'Прямого заключения договора (ст. 21., ч.4., п3)',
  });
}

const rating = [];
for (let i = 0; i < 10; i++) {
  rating.push({
    number: `${i}006022${i}305081${i}`,
    status: 'Опубликовано',
    amount: `1${i} 600 000`,
    organization: 'Учебный центр Министерства Финансов КР',
    buyType: 'Канцелярские товары',
    releaseDate: `${i} август 2020 (09:${i})`,
    lotCount: i,
    endDate: `${i + 2} сентябрь 2020 (23:41)`,
    method: 'Прямого заключения договора (ст. 21., ч.4., п3)',
  });
}

const summary = [];
for (let i = 0; i < 2; i++) {
  summary.push({
    number: `${i}006022${i}305081${i}`,
    status: 'Опубликовано',
    amount: `2${i} 600 000`,
    organization: 'Учебный центр Министерства Финансов КР',
    buyType: 'Канцелярские товары',
    releaseDate: `${i} апрель 2020 (21:${i})`,
    lotCount: i,
    endDate: `${i + 2} июнь 2020 (10:41)`,
    method: 'Прямого заключения договора (ст. 21., ч.4., п3)',
  });
}

const contract = [];
for (let i = 0; i < 20; i++) {
  contract.push({
    number: `${i}006022${i}305081${i}`,
    status: 'Опубликовано',
    amount: `4${i} 734 000`,
    organization: '',
    buyType: 'Канцелярские товары',
    releaseDate: `${i} мая 2020 (21:${i})`,
    lotCount: i,
    endDate: `${i + 2} мая 2020 (10:41)`,
    method: 'Прямого заключения договора (ст. 21., ч.4., п3)',
  });
}

const canceled = [];
for (let i = 0; i < 3; i++) {
  canceled.push({
    number: `${i}006022${i}305081${i}`,
    status: 'Опубликовано',
    amount: `4${i} 250 550`,
    organization: 'Государственная служба регулирования и надзора за финансовым рынком при Правительстве КР',
    buyType: 'Канцелярские товары',
    releaseDate: `${i} мая 2020 (10:${i})`,
    lotCount: i,
    endDate: `${i + 2} мая 2020 (20:41)`,
    method: 'Прямого заключения договора (ст. 21., ч.4., п3)',
  });
}

export default () => {
  return (
    <Layout>
      <PageHeader
        title="Все объявления товаров, работ, услуг"
        extra={<Input placeholder="Поиск" addonAfter={<MdSearch/>}/>}
      />
      <div className="card-container">
        <Tabs defaultActiveKey="1" type="card">

          <Tabs.TabPane tab="Опубликованные" key="1">
            <Lots data={published}/>
          </Tabs.TabPane>

          <Tabs.TabPane tab="Оценка" key="2">
            <Lots data={rating}/>
          </Tabs.TabPane>

          <Tabs.TabPane tab="Итоги" key="3">
            <Lots data={summary}/>
          </Tabs.TabPane>

          <Tabs.TabPane tab="Договоры" key="4">
            <Lots data={contract}/>
          </Tabs.TabPane>

          <Tabs.TabPane tab="Отмененные" key="5">
            <Lots data={canceled}/>
          </Tabs.TabPane>
        </Tabs>
      </div>
    </Layout>
  );
};

const Lots = ({data}) => {
  return (
    <>
      <Row gutter={16}>
        {data.map((item, idx) => (
          <Col sm={24} md={12} key={idx}>

            <PageHeader
              ghost
              className="lot-item"
              title={<Title copyable level={4}>№: {item.number}</Title>}
              tags={<Tag color="green">{item.status}</Tag>}
              extra={<div className="text-right">
                <Text type="secondary">Сумма закупок</Text>
                <h2>{item.amount} сом</h2>
              </div>}
            >
              <Text type="secondary">Закупающая организация:</Text>
              <h3>"{item.organization}"</h3>
              <br/>
              <Descriptions size="small" column={2}>
                <Descriptions.Item label="Наименование закупки"><a>{item.buyType}</a></Descriptions.Item>
                <Descriptions.Item label="Опубликовано">{item.releaseDate}</Descriptions.Item>
                <Descriptions.Item label="Количество лотов">{item.lotCount}</Descriptions.Item>
                <Descriptions.Item label="Завершение">{item.endDate}</Descriptions.Item>
                <Descriptions.Item label="Метод закупок">{item.method}</Descriptions.Item>
              </Descriptions>
            </PageHeader>
          </Col>
        ))}
      </Row>
      <Pagination defaultCurrent={1} total={100}/>
      <br/>
    </>
  );
};
