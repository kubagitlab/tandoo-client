import React from 'react';
import {Layout, PageHeader} from 'antd';
import Input from '../../components/Input';
import {MdSearch} from 'react-icons/md';
import ProductList from '../../components/product/ProductListPublic';

export default () => {
  return (
    <>
      <PageHeader
        title="Каталог товаров, работ, услуг"
        extra={<Input placeholder="Поиск" addonAfter={<MdSearch/>}/>}
      />
      <Layout style={{padding: '0 15px'}}>
        <ProductList/>
      </Layout>
    </>
  );
};
