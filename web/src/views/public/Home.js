import React from 'react';
import {Button, Card, Col, Row, Statistic, Typography} from 'antd';
import Slider from 'react-slick';
import ImageView from "../../components/ImageView";

const {Title} = Typography;

const news = [
  {
    title: 'Сооронбай Жээнбеков пригласил компании из ОАЭ к инвестированию в Кыргызстан',
    img: 'http://bizexpert.kg/wp-content/uploads/2019/12/81719.jpg_w780_h500_resize-696x471.jpg',
  },
  {
    title: 'Евросоюз хочет ввести новое правило для водителей фур',
    img: 'http://bizexpert.kg/wp-content/uploads/2019/12/1051832t1hfdc9.jpg',
  },
  {
    title: 'Минэконом предлагает ввести запрет на вывоз пшеницы и муки',
    img: 'http://bizexpert.kg/wp-content/uploads/2019/12/muka-i-pshenitsa-696x465.jpg',
  },
  {
    title: 'Поддержка государства должна выражаться хотя бы в том, чтобы не создавать препятствия',
    img: 'http://bizexpert.kg/wp-content/uploads/2019/11/crop.jpg',
  },
  {
    title: 'Перечень нарушений требований к производству ювелирных изделий государств-членов ЕАЭС',
    img: 'http://bizexpert.kg/wp-content/uploads/2020/05/%D0%94%D0%BB%D1%8F-%D1%81%D0%B0%D0%B9%D1%82%D0%B0-%D0%91%D0%B8%D0%B7%D0%AD%D0%BA%D1%81%D0%BF%D0%B5%D1%80%D1%82-696x392.png',
  },
  {
    title: 'За 10 месяцев Кыргызстан отправил в другие страны для покупки ювелирных изделий минимум 10 млн долларов',
    img: 'http://bizexpert.kg/wp-content/uploads/2019/11/16730091_352924071767747_3491699684068901968_n-696x696.jpg',
  },
];
const settings = {
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 1,
  initialSlide: 0,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

export default () => {
  return (
    <div>
      <Hero/>
      <News/>
      <Postavshikam/>
      <Zakupshikam/>
      <Obshestvennost/>
      <Proizvoditel/>
      <Stat/>
      <Footer/>
    </div>
  );
};

const Hero = () => {
  return (
    <div className="bg-img1">
      <div className="container">
        <div className="pv-100">
          <Title className="f-300 text-center text-white">Электронный каталог товаров, работ и
            услуг <br/> государственных закупок Кыргызской Республики</Title>
          <Title level={4} className="f-300 text-center text-white">здесь вы можете купить, продать, арендовать товары
            и имущества, или
            участвовать в аукционах</Title>
        </div>
        <div style={{height: 150}}/>
      </div>
    </div>
  );
};
const News = () => {
  return (
    <div className="container ph-40" style={{marginTop: -200}}>
      <Slider {...settings}>
        {news.map((item, idx) => (
          <div className="news-carousel" key={idx}>
            <Card
              key={idx}
              cover={<div className="news-img"><ImageView alt="" url={item.img}/></div>}
              className="text-center"
            >
              <h5 className="slick-title" ellipsis={{rows: 2}}>{item.title}</h5>

              <Button type="dashed">
                Подробнее
              </Button>
            </Card>
          </div>
        ))}
      </Slider>
      <br/><br/>
    </div>
  );
};
const Postavshikam = () => {
  return (
    <Box
      bgGray
      title="Поставщикам"
      img="../assets/img/landing/img3.svg"
      desc="Разнообразный и богатый опыт укрепление и развитие структуры в значительной степени
              обуславливает создание модели развития. Идейные соображения высшего порядка, а также постоянное
              информационно-пропагандистское обеспечение нашей деятельности влечет за собой процесс внедрения и
              модернизации дальнейших направлений развития."
    />
  );
};
const Zakupshikam = () => {
  return (
    <Box
      imgRight
      title="Закупщикам"
      img="../assets/img/landing/img3.svg"
      desc="Разнообразный и богатый опыт укрепление и развитие структуры в значительной степени
              обуславливает создание модели развития. Идейные соображения высшего порядка, а также постоянное
              информационно-пропагандистское обеспечение нашей деятельности влечет за собой процесс внедрения и
              модернизации дальнейших направлений развития."
    />
  );
};
const Obshestvennost = () => {
  return (
    <Box
      bgGray
      title="Представителям общественные"
      img="../assets/img/landing/img3.svg"
      desc="Разнообразный и богатый опыт укрепление и развитие структуры в значительной степени
              обуславливает создание модели развития. Идейные соображения высшего порядка, а также постоянное
              информационно-пропагандистское обеспечение нашей деятельности влечет за собой процесс внедрения и
              модернизации дальнейших направлений развития."
    />
  );
};
const Proizvoditel = () => {
  return (
    <Box
      imgRight
      title="Отечественным производителям"
      img="../assets/img/landing/img3.svg"
      desc="Разнообразный и богатый опыт укрепление и развитие структуры в значительной степени
              обуславливает создание модели развития. Идейные соображения высшего порядка, а также постоянное
              информационно-пропагандистское обеспечение нашей деятельности влечет за собой процесс внедрения и
              модернизации дальнейших направлений развития."
    />
  );
};
const Stat = () => {
  return (
    <div className="bg-img1 text-center pv-50">
      <div className='container'>
        <Row>
          <Col xs={24} sm={12} md={6}>
            <Statistic valueStyle={{color: 'white'}} title="Active Users" value={112893}/>
          </Col>
          <Col xs={24} sm={12} md={6}>
            <Statistic valueStyle={{color: 'white'}} title="Active Users" value={112893}/>
          </Col>
          <Col xs={24} sm={12} md={6}>
            <Statistic valueStyle={{color: 'white'}} title="Active Users" value={112893}/>
          </Col>
          <Col xs={24} sm={12} md={6}>
            <Statistic valueStyle={{color: 'white'}} title="Active Users" value={112893}/>
          </Col>
        </Row>
      </div>
    </div>
  );
};
const Footer = () => {
  return (
    <div className="footer">
      <div className="container">
        <Row justify="space-between">
          <Col span={12}>© {(new Date().getFullYear())} Tandoo</Col>
          <Col span={12} className="text-right">Powered by Ltd. <a href="http://ictlab.kg/" target="_blank">ICTLAB
            project.</a></Col>
        </Row>
      </div>
    </div>
  );
};

const Box = ({bgGray, title, desc, img, imgRight}) => {
  return (
    <div className={bgGray ? 'bg-gray' : 'bg-white'}>
      <div className='container'>
        <Row gutter={24} align="middle">
          {!imgRight && <Col xs={24} sm={24} md={12}>
            <img alt="" className="img-responsive" src={img}/>
          </Col>}
          <Col sm={12}>
            <Title>{title}</Title>
            <p className="lead">{desc}</p>
            <a href="#" className="ant-btn ant-btn-primary">Подробнее</a>
          </Col>
          {imgRight && <Col sm={12}>
            <img alt="" className="img-responsive" src={img}/>
          </Col>}
        </Row>
      </div>
    </div>
  );
};
