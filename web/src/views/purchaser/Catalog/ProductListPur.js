import React, {useEffect, useState} from 'react';
import purApi from '../../../../../apis/purchaser';
import ProductList from "../../../components/product/ProductListView";
import find from "lodash/find";

export default ({onAdd, lockSection}) => {
  const [sections, setSections] = useState([]);
  const [disableSections, setDisableSections] = useState(false);
  const [categories, setCategories] = useState([]);
  const [products, setProducts] = useState([]);
  const [section, setSection] = useState(null);
  const [category, setCategory] = useState(null);
  const [filters, setFilters] = useState(null);
  const [dirFilters, setDirFilters] = useState([]);
  const [specFilters, setSpecFilters] = useState([]);
  const [productId, setProductId] = useState(null);

  useEffect(() => {
    purApi.getSections().then(r => {
      console.log('get sections', r);
      setSections(r.list)
    });
  }, []);

  useEffect(() => {
    if (lockSection) {
      setSection(lockSection);
      setDisableSections(true);
    } else {
      setDisableSections(false);
    }
  }, [lockSection])

  const getProducts = (category_id, filters) => {
    console.log('get products>', {category_id, filters})
    purApi.getProducts({category_id, filters}).then(r => {
      console.log('get products<', r)
      setProducts(r.data.products);
      setFilters(r.data.filters);
    }).catch(e => {
      if (e.message === 'RECORD_NOT_FOUND') {
        setProducts([]);
      }
    });
  };

  const onSectionSelect = (selSection) => {
    setSection(selSection);
    setCategories([]);
    setCategory(null);
    setProducts([]);
    setFilters(null);
    if (selSection) {
      setCategories(selSection.categories)
    }
  };

  const onOkgzSelect = (selCategory) => {
    setCategory(selCategory);
    setProducts([]);
    setFilters(null);
    if (selCategory) {
      getProducts(selCategory.id);
    }
  };

  const showProduct = (product) => {
    setProductId(product.id);
  };

  const hideProduct = () => {
    setProductId(null);
  };

  const onFilterClick = (type, entity_id, value_id) => {
    let fs = type === 'dirs' ? [...dirFilters] : [...specFilters];
    const f = fs.find(item => item.entity_id === entity_id && item.value_id === value_id);
    if (f) {
      fs = fs.filter(item => !(item.entity_id === entity_id && item.value_id === value_id));
    } else {
      fs.push({entity_id, value_id});
    }
    let ff;
    if (type === 'dirs') {
      setDirFilters(fs);
      ff = {specs: specFilters, dirs: fs};

    } else {
      setSpecFilters(fs);
      ff = {specs: fs, dirs: dirFilters};
    }
    getProducts(category.id, ff);
  };

  const filterExists = (type, entity_id, value_id) => {
    let fs = type === 'dirs' ? [...dirFilters] : [...specFilters];
    const f = fs.find(item => item.entity_id === entity_id && item.value_id === value_id);
    return !!f;
  };

  function handleAdd(id) {
    let product = find(products, {id})
    let activeFilters = {dirs: dirFilters, specs: specFilters};

    hideProduct();
    onAdd && onAdd({product, section, category, activeFilters, filters})
  }

  let viewProps = {sections, section, onSectionSelect, categories, category, onOkgzSelect, filters,
    products, showProduct, productId, hideProduct, filterExists, onFilterClick,
    onAdd: onAdd && handleAdd, disableSections};

  return (
    <ProductList {...viewProps}/>
  )
}
