import React from 'react';
import {PageHeader} from "antd";
import ProductList from "./ProductListPur";

export default ({match}) => {
  return (
    <>
      <PageHeader title="Каталог"/>
      <ProductList/>
    </>
  )
}
