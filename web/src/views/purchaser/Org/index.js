import React from 'react';
import {PageHeader, Tabs, Card} from 'antd';
import Button from '../../../components/Button';
import OrgList from './List';

export default () => (
  <>
    <PageHeader title="Организации" extra={<Button>Добавить организацию</Button>}/>

    <Card>
      <Tabs defaultActiveKey="1">
        <Tabs.TabPane tab="Активные" key="1">
          <OrgList/>
        </Tabs.TabPane>
        <Tabs.TabPane tab="Неактивные" key="2">
          <OrgList/>
        </Tabs.TabPane>
      </Tabs>
    </Card>
  </>
);

