import {Tag, Tooltip} from "antd";
import ReactTable from "../../../components/ReactTable";
import React from "react";
import {MdRemoveRedEye, MdModeEdit} from 'react-icons/md';
import Button from "../../../components/Button";

const columns = [
  {
    Header: 'Организация',
    accessor: 'name',
    className: 'text-center',
    headerClassName: 'f-bold',
  },
  {
    Header: 'Статус',
    accessor: 'company_status',
    className: 'text-center',
    headerClassName: 'f-bold',
    Cell: ({value}) => <Tag color="green">{value}</Tag>,
  },
  {
    Header: 'Тип организации',
    accessor: 'company_type',
    className: 'text-center',
    headerClassName: 'f-bold',
  },
  {
    Header: 'ИНН',
    accessor: 'inn',
    className: 'text-center',
    headerClassName: 'f-bold',
  },
  {
    Header: '',
    Cell: ({row}) => (
      <>
        <Tooltip title="Просмотр">
          <Button size="small" type="link"><MdRemoveRedEye/></Button>
        </Tooltip>
        <Tooltip title="Редактировать">
          <Button size="small" type="link"><MdModeEdit/></Button>
        </Tooltip>
      </>
    )
  }
];
const data = [
  {
    id: 1,
    company_status: 'confirmed',
    company_type: 'supplier',
    data: {phone: '996700202020', email: 'shakitov@mail.ru ', address: {house: '', street: '', apt: ''}},
    inn: '01101201710147',
    main_doc_img: [],
    name: 'Общество с ограниченной ответственностью',
    owner_data: {email: 'shakitov@mail.ru ', inn: '21502197600617', fio: 'Джунушов Бакыт Аманжанович'},
  },
  {
    id: 2,
    company_status: 'confirmed',
    company_type: 'supplier',
    data: {phone: '996700202020', email: 'shakitov@mail.ru ', address: {house: '', street: '', apt: ''}},
    inn: '01101201710147',
    main_doc_img: [],
    name: 'Общество с ограниченной ответственностью',
    owner_data: {email: 'shakitov@mail.ru ', inn: '21502197600617', fio: 'Джунушов Бакыт Аманжанович'},
  },
];

export default () => {
  return (
    <ReactTable data={data} columns={columns} pageSize={10}/>
  );
};
