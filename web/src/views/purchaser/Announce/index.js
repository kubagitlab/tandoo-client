import React from 'react';
import {PageHeader, Card, Tabs, Typography} from 'antd';
import Button from '../../../components/Button';
import AnnList from './List';

const {TabPane} = Tabs;
const {Text} = Typography;

const ButtonAddAnn =
  <Button href={`/#/pur/announce/edit`}>Добавить объявление</Button>;

export default () => (
  <>
    <PageHeader title="Мои объявления" extra={ButtonAddAnn}/>

    <Card>
      <Tabs defaultActiveKey="1">
        <TabPane tab="Все объявления" key="1">
          <AnnList/>
        </TabPane>
        <TabPane tab="Проекты" key="2">
          <AnnList status="Draft"/>
        </TabPane>
        <TabPane tab="Опубликовано" key="3">
          <AnnList status="Published"/>
        </TabPane>
        <TabPane tab="Оценка" key="4">
          <AnnList/>
        </TabPane>
        <TabPane tab="Итоги" key="5">
          <AnnList/>
        </TabPane>
        <TabPane tab="Договора" key="6">
          <AnnList/>
        </TabPane>
        <TabPane key="7" tab={<Text type="danger">Бюджет</Text>}>
          <AnnList/>
        </TabPane>
      </Tabs>
    </Card>
  </>
)

