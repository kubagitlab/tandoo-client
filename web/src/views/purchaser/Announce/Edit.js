import React from "react";
import {Card, PageHeader, Divider} from "antd";
import {Steps} from 'antd';
import Basket from "./components/Basket";
import Details from './components/Details';
import Preview from "./components/Preview";
import Publish from "./components/Publish";
import {observer} from "mobx-react";
import {action, computed, observable, observe, toJS} from "mobx";
import purApi from "../../../../../apis/purchaser";
import appStore from "../../../../../store/AppStore";

const {Step} = Steps;


// Step status: wait process finish error

@observer
export default class AnnEdit extends React.Component {
  @observable annId = undefined;
  @observable ready = false;
  @observable step = 0;
  @observable publishStatus = 'process';
  @observable tehnEnabled = false;
  @observable lotsSaved = false;
  @observable announce = {
    section: null, // validate: section exists
    procurement: null, // validate: DirProcurement exists
    guarantee: 0, // validate: 0 <= x <= 10, step=0.01
    concession: 0, // validate: 0 <= x <= 20, step=0.01
    tehadress: '',
    guarant_day: 0, // >= 0
    defect_day: 0, // >= 0
    deadline: '', // validate: if procurement.day_count, x >= (today + day_count business days)
    start_date: '', // validate: x >= deadline, isBusinessDay
    payments: {
      advance: 0, // 0 <= x <= 100
      shipment: 0, // 0 <= x <= 100
      accept: 0, // 0 <= x <= 100, (advance + shipment + accept == 100)
    },
    comm_members: [],
    chairman_id: null, // employee_id
    data: {},
    lots: [
      /*{
        category: {id, name},
        avg_unit_price: float,
        unit: {value_id, value_name},
        dirs: [
          {entity_id, value_id, entity_name, value_name}
        ],
        specs: [
          {entity_id, value_id, entity_name, value_name}
        ],
        params: {
          quantity: int,
          unit_price: float,
          estimated_delivery_time: int,
          address: {
            coate: {id, name},
            street: string,
            house: string,
            apt: string,
          },
        }
      }*/
    ],
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.setAnnounce();
    this.disposers = [
      observe(this.announce.lots, this.validateLots)
    ]
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.setAnnounce();
  }

  componentWillUnmount() {
    this.disposers.forEach(dispose => dispose());
  }

  async setAnnounce() {
    let id = this.props.match.params.id;
    if (id)
      id = parseInt(id);

    if (id !== this.annId) {
      this.annId = id;
      this.ready = false;

      if (id) {
        if (!this.announce || this.announce.id !== id) {
          console.log('this.announce.id, id', this.announce.id, id)
          await this.load(id)
        }
      } else {
        this.reset();
      }

      this.ready = true;
    }
  }

  async load(id) {
    // TODO: load
    console.warn('load ann', id);

    // example announce
    this.announce.section = {id: 1, name: "Медикаменты"};
    this.announce.lots = [
      {
        // category: 10044,
        // unit: 6,
        id: null,
        section: {id: 1, name: "Медикаменты"},
        category: {id: 10044, name: "Парацетамол"},
        unit: {entity_id: 31, value_id: 6, entity_name: "Единица измерения", value_name: "Метр", dir_name: "DirMeasureUnit"},
        avg_unit_price: 213,
        dirs: [
          {entity_id: 29, value_id: 71, entity_name: "Производитель", value_name: "ОсОО \"БЕКПР\"", dir_name: "DirManufacture"},
          {entity_id: 30, value_id: 28, entity_name: "Марка", value_name: "Reebok", dir_name: "DirBrand"},
          {entity_id: 31, value_id: 6, entity_name: "Единица измерения", value_name: "Метр", dir_name: "DirMeasureUnit"},
        ],
        specs: [],
        params: {
          address: {coate: "hh", street: "55", house: "66", apt: "88"},
          estimated_delivery_time: 1,
          quantity: 1,
          unit_price: 213,
        }
      }
    ];

    this.lotsSaved = true;
  }

  reset() {
    // TODO: reset
    console.warn('TODO: reset')
    this.lotsSaved = false;
  }

  @action.bound
  onSelectStep(nextStep) {
    this.step = nextStep
  }

  async saveAnnounce() {
    if (!this.canSaveLots)
      throw 'No lots';

    let announce = toJS(this.announce);

    announce.section = announce.section && announce.section.id;
    announce.procurement = announce.procurement && announce.procurement.id;
    announce.lots = announce.lots.map(lot => {
      let {category, unit, ...rest} = lot;
      return {
        ...rest,
        unit: unit.value_id,
        category: category.id
      }
    })

    console.log('announce>', announce);

    try {
      let {id} = await purApi.saveAnnounce({announce})
      appStore.setAlert("success", `Сохранено!`);
      this.announce.id = id;
      this.props.history.replace(`/pur/announce/edit/${id}`);

    } catch (e) {
      console.warn(e);
      throw e;
    }
  }

  saveLots = async (next) => {
    try {
      await this.saveAnnounce();

      if (next) {
        this.step++;
      }
    } catch (e) {
    }
  }

  onSaveDetails = async (next) => {
    try {
      await this.saveAnnounce();

      if (next) {
        this.step++;
      }
    } catch (e) {
    }
  }

  onSubmitPreview = async () => {
    try {
      await this.saveAnnounce();

      if (next) {
        this.step++;
      }
    } catch (e) {
    }
  }

  onSubmitPublish = async () => {
    try {
      await this.saveAnnounce();

      this.publishStatus = 'finish';

    } catch (e) {
    }
  }

  @computed get canSaveLots() {
    return this.announce.lots.length > 0;
  }

  @action.bound
  validateLots() {
    this.lotsSaved = false;

    if (this.announce.lots.length) {
      this.announce.section = this.announce.lots[0].section;
    } else {
      this.announce.section = null
    }
  }

  render() {
    let {
      step, announce, onSelectStep, publishStatus, saveLots, onSaveDetails,
      onSubmitPreview, onSubmitPublish
    } = this;

    return (
      <>
        <PageHeader title="Создание объявления"/>

        <Card>
          <Steps current={step} type="navigation" className="site-navigation-steps"
                 onChange={onSelectStep}>
            <Step title="Позиции" description="Перечень закупаемых товаров"/>
            <Step title="Детали" description="Данные объявления"/>
            <Step title="Предпросмотр" description="Проверка перед публикацией"/>
            <Step title="Публикация" description="Статус:"
                  status={step === 3 ? publishStatus : 'wait'}/>
          </Steps>

          <Divider/>

          {step === 0 &&
          <>
            <Basket section={announce.section} lots={announce.lots} onSave={saveLots}/>
          </>
          }

          {step === 1 &&
          <>
            <Details announce={announce} onSave={onSaveDetails}/>
          </>
          }

          {step === 2 &&
          <>
            <Preview announce={announce} onSubmit={onSubmitPreview}/>
          </>
          }

          {step === 3 &&
          <>
            <Publish onSubmit={onSubmitPublish}/>
          </>
          }

          {/*<pre>
            {JSON.stringify(announce, null, '  ')}
          </pre>*/}

        </Card>
      </>
    )
  }
}
