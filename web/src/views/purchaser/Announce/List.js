import ReactTable from '../../../components/ReactTable';
import {Tag, Tooltip} from 'antd';
import React from 'react';
import {formatDate, formatDateTime, formatMoney} from '../../../../../common/utils';
import {MdFlag, MdFormatListNumbered, MdInsertDriveFile, MdModeEdit, MdPlaylistAddCheck} from 'react-icons/md';
import Button from '../../../components/Button';
import purApi from "../../../../../apis/purchaser";

const STATUS = {
  0: {label: 'Опубликовано', color: 'green'},
  1: {label: 'Оценка', color: 'blue'},
  2: {label: 'Черновик', color: 'volcano'},
  3: {label: 'Формирование Договора', color: 'geekblue'},
  'Draft': {label: 'Черновик', color: 'volcano'},
  'Published': {label: 'Опубликовано', color: 'green'},
};

const columns = [
  {
    Header: '№ объявления',
    accessor: 'code',
    Cell: ({value}) => <a>{value}</a>,
  },
  {
    Header: 'Наименование',
    accessor: 'dir_section',
    Cell: ({value}) => <a>{value}</a>,
  },
  {
    Header: 'Планируемая сумма',
    accessor: 'budget',
    Cell: ({value}) => formatMoney(value),
  },
  {
    Header: 'Статус',
    accessor: 'status',
    Cell: ({value}) =>
      <Tag color={STATUS[value] ? STATUS[value].color : 'yellow'}>
        {STATUS[value] ? STATUS[value].label : value}
      </Tag>,
  },
  {
    Header: 'Создано',
    accessor: 'created_date',
    Cell: ({value}) => formatDateTime(value),
  },
  {
    Header: 'Опубликовано',
    accessor: 'published_date',
    Cell: ({value}) => value && formatDateTime(value),
  },
  {
    Header: 'Срок подачи заявок',
    accessor: 'deadline',
    Cell: ({value}) => value && formatDate(value),
  },
  {
    Header: 'Действия',
    Cell: ({row}) =>
      <span className="table-actions">
        {row.status === 2 &&
        <Tooltip title="Редактировать">
          <Button size="small" type="link"><MdModeEdit/></Button>
        </Tooltip>}
        <Tooltip title="Таблица цен">
          <Button size="small" type="link"><MdFormatListNumbered/></Button>
        </Tooltip>
        <Tooltip title="Оценка предложений">
          <Button size="small" type="link"><MdFlag/></Button>
        </Tooltip>
        <Tooltip title="Итоги">
          <Button size="small" type="link"><MdPlaylistAddCheck/></Button>
        </Tooltip>
        <Tooltip title="Договора">
          <Button size="small" type="link"><MdInsertDriveFile/></Button>
        </Tooltip>
      </span>,
  },
];

/*
const data = [];
for (let i = 0; i < 15; i++) {
  data.push({
    number: `${i}006022${i}305081${i}`,
    status: i % Object.keys(STATUS).length,
    amount: `22${i}600000`,
    announceName: 'Канцелярские товары',
    releaseDate: `2020-05-${i + 3} 21:${i % 6}0`,
    lotCount: i,
    endDate: `2020-06-${i + 3}`,
    edit: 'Edit',
  });
}
*/

// AnnounceTable
export default ({status}) => {
  let [annList, setAnnList] = React.useState(null);

  React.useEffect(() => {
    let params;
    if (status) {
      params = {filter: {status}};
    }
    purApi.getAnnounceList(params).then(r => {
      console.log('ann<', r);
      setAnnList(r.announces);
    })
  }, []);

  if (!annList) return null;

  return (
    <ReactTable columns={columns}
                data={annList}
                pageSize={10}
                className={'m-2 text-center'}/>
  );
};
