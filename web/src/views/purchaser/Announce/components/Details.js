import React from 'react';
import moment from "moment";
import Button from "../../../../components/Button";
import {observer} from "mobx-react";
import {action, observable, observe, runInAction} from "mobx";
import commonApi from "../../../../../../apis/common";
import {Form, Space, Switch, Typography} from "antd";
import Input, {InputNumber} from "../../../../components/Input";
import ReactSelect from "../../../../components/ReactSelect";
import DatePicker from "../../../../components/DatePicker";

const {Text} = Typography;

const layout = {
  labelCol: {span: 8},
  wrapperCol: {span: 16},
};
const tailLayout = {
  wrapperCol: {offset: 8, span: 16},
};


@observer
export default class AnnEditDetails extends React.Component {
  // @observable error;
  // @observable canSubmit;
  // @observable.ref procurements;
  // @observable procurement;
  // @observable announce;
  // @observable start_date;
  // @observable step;
  // @observable.ref deadline;
  // @observable minDLDays;
  // @observable minDLDate;
  // @observable concession;
  // @observable guarantee;
  // @observable tehadress;
  // @observable guarant_day;
  // @observable defect_day;
  // @observable payments = {
  //   advanceEnabled: false,
  //   shipmentEnabled: false,
  //   acceptEnabled: false,
  //   advance: '',
  //   shipment: '',
  //   accept: '',
  // };
  // @observable comisMembers = [];
  // @observable showAuction = false;
  // @observable chairman = '';
  @observable tehnEnabled = false;
  @observable isCommission = false;
  @observable advanceEnabled = false;
  @observable shipmentEnabled = false;
  @observable acceptEnabled = false;
  @observable procurements = [];

  componentDidMount() {
    // this.load();
    let {announce} = this.props;

    commonApi.getDir('DirProcurement').then(p => {
      this.procurements = p;
    })

    this.tehnEnabled = !!announce.tehadress;
    this.isCommission = !!announce.comm_members.length;
    this.advanceEnabled = !!announce.payments.advance;
    this.shipmentEnabled = !!announce.payments.shipment;
    this.acceptEnabled = !!announce.payments.accept;

    // set observers
    this.disposers = [
      observe(announce, 'procurement', ({newValue: procurement}) => {
        if (procurement) {
          // TODO: minDLdays = procurement.day_count
        }
      }),
      observe(this, 'tehnEnabled', ({newValue: val}) => {
        if (!val) {
          announce.tehadress = ''
        }
      }),
      observe(this, 'advanceEnabled', ({newValue: val}) => {
        if (!val) {
          announce.payments.advance = 0
        }
      }),
      observe(this, 'shipmentEnabled', ({newValue: val}) => {
        if (!val) {
          announce.payments.shipment = 0
        }
      }),
      observe(this, 'acceptEnabled', ({newValue: val}) => {
        if (!val) {
          announce.payments.accept = 0
        }
      }),
    ]

  }

  componentWillUnmount() {
    this.disposers.forEach(dispose => dispose());
  }


  async load() {
    // let id = this.props.match.params.id; // from route
    //
    // let [procurements, announce] = await Promise.all([
    //   this.props.dictStore.getDictData2({type: 'DirProcurement'}),
    //   announceApi.get({id})
    // ]);
    //
    // let procurements = await this.props.dictStore.getDictData2({type: 'DirProcurement'}),
    // procurements = procurements.sort((a, b) => a.order - b.order);

    let {announce} = this.props;
    let procurements = await commonApi.getDir('DirProcurement');

    runInAction(() => {
      Object.assign(this, {
        procurements,
        announce,
        procurement: announce.dirprocurement,
        deadline: announce.deadline,
        // hide: announce.hide || false,
        concession: 20 || announce.concession,
        guarantee: announce.guarantee,
        tehnEnabled: announce.tehnEnabled,
        minDLDays: announce.dirprocurement ? announce.dirprocurement.day_count : null,
        minDLDate: announce.dirprocurement ? momentbd().businessAdd(announce.dirprocurement.day_count) : null,
        payments: {
          advanceEnabled: announce.data && announce.data.payments && announce.data.payments.advanceEnabled || false,
          shipmentEnabled: announce.data && announce.data.payments && announce.data.payments.shipmentEnabled || false,
          acceptEnabled: announce.data && announce.data.payments && announce.data.payments.acceptEnabled || false,
          advance: announce.data && announce.data.payments && announce.data.payments.advance || '',
          shipment: announce.data && announce.data.payments && announce.data.payments.shipment || '',
          accept: announce.data && announce.data.payments && announce.data.payments.accept || '',
        }
      });

      this.updateError();
    });
  }

  @action
  setProcurement = procurement => {
    let minDLDays = null;
    let minDLDate = null;

    if (procurement) {
      minDLDays = procurement.day_count;
      minDLDate = momentbd().businessAdd(minDLDays);
    }

    Object.assign(this, {
      procurement,
      minDLDays,
      minDLDate,
    });

    this.showAuction = procurement._id === '62e43b5e-3cca-4d3f-9680-c106c91ed7cf';

    this.updateError();
  };

  @action
  setDeadline = deadline => {
    this.deadline = deadline;
    this.updateError();
  };

  @action
  setPayment = (type, value) => {
    if (['advance', 'shipment', 'accept'].includes(type))
      value = parseFloat(value);

    // TODO: make auto calculations
    this.payments[type] = value;
    this.updateError();
  };

  @action
  setConcession = (concession) => {
    this.concession = concession;
    this.updateError();
  };

  @action
  updateError() {
    let {procurement, deadline, payments, minDLDays, concession, guarantee} = this;

    if (!procurement) {
      this.setError('Укажите Метод закупок');
      return;
    }

    if (procurement.with_concession) {
      if (concession === null || concession === '' || isNaN(parseFloat(concession)) || !isFinite(concession)) {
        this.setError('Укажите Значение льготы');
        return;
      }

      concession = parseFloat(concession);

      if (concession < 0 || concession > 20) {
        this.setError('Значение льготы должно быть от 0 до 20');
        return;
      }

      if (guarantee === null || guarantee === '' || isNaN(parseFloat(guarantee)) || !isFinite(guarantee)) {
        this.setError('Укажите сумму гарантийного обеспечения');
        return;
      }

      guarantee = parseFloat(guarantee);

      if (guarantee < 0 || guarantee > 10) {
        this.setError('Значение суммы гарантийного обеспечения должно быть от 0 до 10');
        return;
      }

    }
    if (this.tehnEnabled && !this.tehadress) {
      this.setError('Введите адрес технического испытания');
      return;
    }

    if (!deadline) {
      this.setError('Укажите Срок подачи заявок');
      return;
    }

    deadline = momentbd(deadline);

    // if (deadline.hour() < 9 || deadline.hour() > 18 || (deadline.hour() === 18 && deadline.minute() !== 0)) {
    //   this.setError('Укажите Время (с 9 до 18)');
    //   return;
    // }

    let minDLDate = momentbd().businessAdd(minDLDays);
    this.minDLDate = minDLDate;

    if (deadline.isBefore(minDLDate)) {
      this.setError('Укажите Срок подачи не менее {{minDLDays}} рабочих дней');
      return;
    }

    let {advanceEnabled, shipmentEnabled, acceptEnabled} = payments;

    if (!(advanceEnabled || shipmentEnabled || acceptEnabled)) {
      this.setError('Укажите хотя бы один вид платежа');
      return;
    }

    let {advance, shipment, accept} = this.payments;

    if (advanceEnabled && !advance) {
      this.setError('Укажите Авансовый платеж');
      return;
    }

    if (shipmentEnabled && !shipment) {
      this.setError('Укажите платеж После отгрузки');
      return;
    }

    if (acceptEnabled && !accept) {
      this.setError('Укажите платеж После приемки');
      return;
    }

    let sum = +(advanceEnabled && advance || 0) +
      (shipmentEnabled && shipment || 0) +
      (acceptEnabled && accept || 0);

    if (sum !== 100) {
      this.setError('Сумма платежей должна быть равна 100%');
      return;
    }

    if (this.isCommission && this.comisMembers.length < 3) {
      this.setError('Необходимо выбрать не менее 3-х человек для комиссии');
      return;
    }

    if (this.isCommission && !this.chairman) {
      this.setError('Необходимо выбрать председателя комиссии');
      return;
    }

    this.setError(null)
  }

  setError(error) {
    Object.assign(this, {
      error,
      canSubmit: error === null
    })
  }

  @action.bound
  setGuarantee(guarantee) {
    this.guarantee = parseFloat(guarantee);
  }

  sumbit = async () => {
    if (!this.canSubmit) return;

    let {announce, procurement, deadline, payments, concession, guarantee, tehnEnabled, tehadress, guarant_day, defect_day, comisMembers, chairman} = this;

    let params = {
      advert: {
        _id: announce._id,
        dirprocurement_id: procurement._id,
        guarantee: guarantee,
        deadline: formatDate(deadline, FORMAT_DATE_DB),
        start_date: formatDate(this.start_date, FORMAT_DATE_DB),
        step: this.step,
        // hide : announce.hide,
        data: {
          chairman_id: chairman,
          payments: payments,
          tehadress: tehadress,
          guarant_day: guarant_day,
          defect_day: defect_day
        },
        comm_members: comisMembers,
      }
    };

    if (procurement && procurement.with_concession) {
      params['advert']['concession'] = concession;
    }

    await announceApi.update_draft(params);

    this.props.history.push(`/purchaser/announce/preview/${announce._id}`);
  };

  onMembersHandler = (list) => {
    list.length >= 3 ? this.comisMembers = list : this.comisMembers = [];
    this.updateError();
  };

  onCommissionSet = (event) => {
    if (!event.target.checked)
      this.comisMembers = [];
    this.chairman = '';
    this.isCommission = !this.isCommission;
    this.updateError();
  };

  chairmanSet = (chair) => {
    this.chairman = chair;
    this.updateError();
  };

  disabledDate(current) {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
  }

  render() {
    let {announce, onSave} = this.props;

    function onFinish() {
      onSave(true)
    }

    function onFinishFailed() {
    }

    // if (!announce.section)
    //   return null;

    return (
      <>
        <Form
          {...layout}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          initialValues={announce}
        >
          <Form.Item label="Наименование объявления">
            <Text>{announce.section && announce.section.name}</Text>
          </Form.Item>
          <Form.Item name="procurement" label="Метод закупок"
                     extra={announce.procurement && announce.procurement.description}
                     rules={[{required: true, message: 'Пожалуйста, выберите метод закупок'}]}
          >
            <ReactSelect
              placeholder="Выберите"
              labelKey="name"
              valueKey="id"
              data={this.procurements}
              value={announce.procurement}
              onChange={v => announce.procurement = v}
              clearable={false}
            />
          </Form.Item>

          <h3 style={{textAlign: 'center'}}>Особые условия Договора</h3>

          <Form.Item name="guarantee" label="Гарантийное обеспечение (%)"
                     extra="Сумма гарантийного обеспечения исполнения Договора (не более 10% от цены Договора)"
                     rules={[/*{required: true, message: 'Пожалуйста, введите число'}*/]}
          >
            <InputNumber model={announce} name="guarantee" min={0} max={10} step={0.01} placeholder="число"/>
          </Form.Item>

          <>
            {/*with_concession*/}
            <Form.Item name="concession" label="Льгота внутренним поставщикам (%)"
                       extra="не более 20% от цены Договора"
                       rules={[{required: true, message: 'Пожалуйста, введите льготу'}]}
            >
              <InputNumber model={announce} name="concession" min={0} max={20} step={0.01} placeholder="число"/>
            </Form.Item>
          </>

          <Form.Item label="Технический контроль испытаний">
            <Switch checked={this.tehnEnabled} onChange={v => this.tehnEnabled = v}/>
          </Form.Item>

          {this.tehnEnabled &&
          <Form.Item name="tehadress" label="Адрес проведения контроля"
                     extra="Введите страну/город/улицу/дом"
                     rules={[{required: true, message: 'Пожалуйста, введите адрес'}]}
          >
            <Input model={announce} name="tehadress" autoFocus/>
          </Form.Item>
          }

          <Form.Item name="guarant_day" label="Гарантийный период на товар, не менее"
                     extra="Введите кол-во дней (с момента поставки)"
                     rules={[{required: true, message: 'Пожалуйста, введите гарантийный период'}]}
          >
            <InputNumber model={announce} name="guarant_day" placeholder="число"/>
          </Form.Item>
          <Form.Item name="defect_day" label="Замена бракованного товара производится в течение"
                     extra="Введите кол-во рабочих дней (с момента получения уведомления от покупателя)"
                     rules={[{required: true, message: 'Пожалуйста, введите кол-во дней'}]}
          >
            <InputNumber model={announce} name="defect_day" placeholder="число"/>
          </Form.Item>

          <Form.Item name="deadline" label="Срок подачи заявок"
                     rules={[{required: true, message: 'Пожалуйста, укажите дату'}]}
          >

            <DatePicker model={announce} name="deadline"
                        disabledDate={this.disabledDate}
                        allowClear={false}/>
          </Form.Item>

          <h3 style={{textAlign: 'center'}}>Платежи</h3>
          <Form.Item label="Авансовый платёж (%)">
            <Switch checked={this.advanceEnabled} onChange={v => this.advanceEnabled = v}/>
            {this.advanceEnabled &&
            <Form.Item noStyle name={["payments", "advance"]}
                       rules={[{required: true, message: 'Пожалуйста, введите размер авансового платежа'}]}>

              <InputNumber model={announce.payments} name="advance" step={0.01} min={0} max={100}
                           autoFocus style={{marginLeft: 5}} placeholder="число"
              />
            </Form.Item>
            }
          </Form.Item>
          <Form.Item label="После отгрузки (%)">
            <Switch checked={this.shipmentEnabled} onChange={v => this.shipmentEnabled = v}/>
            {this.shipmentEnabled &&
            <Form.Item noStyle name={["payments", "shipment"]}
                       rules={[{required: true, message: 'Пожалуйста, введите размер платежа после отгрузки'}]}>

              <InputNumber model={announce.payments} name="shipment" step={0.01} min={0} max={100}
                           autoFocus style={{marginLeft: 5}} placeholder="число"
              />
            </Form.Item>
            }
          </Form.Item>
          <Form.Item label="После приемки (%)">
            <Switch checked={this.acceptEnabled} onChange={v => this.acceptEnabled = v}/>
            {this.acceptEnabled &&
            <Form.Item noStyle name={["payments", "accept"]}
                       rules={[{required: true, message: 'Пожалуйста, введите размер платежа после приемки'}]}>

              <InputNumber model={announce.payments} name="accept" step={0.01} min={0} max={100}
                           autoFocus style={{marginLeft: 5}} placeholder="число"
              />
            </Form.Item>
            }
          </Form.Item>

          <h3 style={{textAlign: 'center'}}>Комиссия</h3>
          <Form.Item label="Добавить Конкурсную комиссию">
            <Switch checked={this.isCommission} onChange={v => this.isCommission = v}/>
          </Form.Item>


          <Form.Item {...tailLayout}>
            <Space>
              <Button type="primary" htmlType="submit">
                Далее
              </Button>
            </Space>
          </Form.Item>
        </Form>
      </>
    )

    /*<Select options={procurements}
                          valueKey="_id"
                          labelKey={name_key} //"name"
                          value={procurement}
                          placeholder={t('Выберите')}
                          onChange={setProcurement}
                          optionRenderer={CustomOption}/>
                  {procurement &&
                  <FormText color="muted">
                    {procurement.description}
                  </FormText>*/

    /*<DatePicker showTimeSelect
                              timeFormat="HH:mm"
                              timeIntervals={15}
                              timeCaption={t("Время")}
                              dateFormat={FORMAT_DATE_TIME}
                              value={deadline}
                              placeholder={t('Дата, время')}
                              onChange={setDeadline}
                              disabled={!procurement}
                              minDate={minDLDate}
                              filterDate={date => date.isBusinessDay()}/>
                  {minDLDays ?
                    <FormText color="muted">
                      {t('Не менее')} {minDLDays} {t('рабочих дней')}
                    </FormText> : ''}*/

    /*const CustomOption = (option, i, inputValue) => (
      <div title={option.description}>
        {option[name_key] || option.name}
      </div>
    );

    if (typeof deadline == "string")
      deadline = moment(deadline, 'YYYY-MM-DD HH:mm:ss');
    if (typeof this.start_date == 'string')
      this.setState({start_date: moment(this.start_date, 'YYYY-MM-DD HH:mm:ss')})*/

  }
}
