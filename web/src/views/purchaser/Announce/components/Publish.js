import React from 'react';
import Button from "../../../../components/Button";

export default class AnnEditPublish extends React.Component {
  render() {
    let {onSubmit} = this.props;

    return (
      <>
        <Button onClick={onSubmit}>Опубликовать</Button>
      </>
    )
  }
}
