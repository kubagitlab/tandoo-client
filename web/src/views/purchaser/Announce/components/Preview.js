import React from 'react';
import Button from "../../../../components/Button";
import {Form, Space, Typography} from "antd";
import {observer} from "mobx-react";
import {formatMoney} from "../../../../../../common/utils";
import ReactTable from "../../../../components/ReactTable";
import compact from "lodash/compact";

const {Text} = Typography;


const layout = {
  labelCol: {span: 8},
  wrapperCol: {span: 16},
};
const tailLayout = {
  wrapperCol: {offset: 8, span: 16},
};

export default class AnnEditPreview extends React.Component {
  render() {
    let {announce, onSubmit} = this.props;

    return (
      <>
        <Form
          {...layout}
          // onFinish={this.onFinish}
          // onFinishFailed={this.onFinishFailed}
        >
          <h3 style={{textAlign: 'center'}}>Основные данные</h3>

          <Form.Item label="№ Объявления">
            <Text>{announce.code}</Text>
          </Form.Item>
          <Form.Item label="Закупающая организация">
            <Text>{announce.company && announce.company.name}</Text>
          </Form.Item>
          <Form.Item label="Раздел">
            <Text>{announce.section && announce.section.name}</Text>
          </Form.Item>
          <Form.Item label="Метод закупок">
            <Text>{announce.procurement && announce.procurement.name}</Text>
          </Form.Item>
          {/*with_concession*/}
          <Form.Item label="Гарантийное обеспечение">
            <Text>{announce.guarantee}%</Text>
          </Form.Item>
          <Form.Item label="Льгота для внутренних поставщиков">
            <Text>{announce.concession}%</Text>
          </Form.Item>
          <Form.Item label="Наименование закупки">
            <Text>{announce.section && announce.section.name}</Text>
          </Form.Item>
          <Form.Item label="Статус">
            <Text>{announce.status}</Text>
          </Form.Item>
          <Form.Item label="Планируемая сумма">
            <Text>{announce.budget}</Text>
          </Form.Item>
          <Form.Item label="Дата публикации">
            <Text>{announce.published_date}</Text>
          </Form.Item>
          <Form.Item label="Срок подачи предложения">
            <Text>{announce.deadline}</Text>
          </Form.Item>
          <Form.Item label="Сумма гарантийного обеспечения исполнения Договора">
            <Text>{announce.guarantee}%</Text>
          </Form.Item>
          {announce.tehadress &&
          <Form.Item label="Технический контроль испытаний">
            <Text>{announce.tehadress}</Text>
          </Form.Item>
          }
          <Form.Item label="Гарантийный период на товар не менее">
            <Text>{announce.guarant_day} дней с момента поставки</Text>
          </Form.Item>
          <Form.Item label="Замена бракованного товара производится в течении">
            <Text>{announce.defect_day} рабочих дней с момента получения уведомления с покупателя</Text>
          </Form.Item>

          <h3 style={{textAlign: 'center'}}>Позиции</h3>
          <LotsList lots={announce.lots}/>

          <Form.Item label="Замена бракованного товара производится в течении">
            <Text>{announce.defect_day} рабочих дней с момента получения уведомления с покупателя</Text>
          </Form.Item>

          <h3 style={{textAlign: 'center'}}>Платежи</h3>

          {announce.payments.advance &&
          <Form.Item label="Авансовый платёж">
            <Text>{announce.payments.advance}%</Text>
          </Form.Item>
          }
          {announce.payments.shipment &&
          <Form.Item label="После отгрузки">
            <Text>{announce.payments.shipment}%</Text>
          </Form.Item>
          }
          {announce.payments.accept &&
          <Form.Item label="После приемки">
            <Text>{announce.payments.accept}%</Text>
          </Form.Item>
          }

          {/*<div>error || Готово к публикации</div>*/}

          <Form.Item {...tailLayout}>
            <Space>
              <Button className="m-2" onClick={this.handleClickBasket}>
                Редактировать позиции
              </Button>
              <Button className="m-2">Редактировать</Button>
              <Button onClick={() => onSubmit(true)}>Далее</Button>
            </Space>
          </Form.Item>

        </Form>
      </>
    )

    return (
      <>
        <h3 className="text-center">
          {t('Объявление')} {announce.code && `№ ${announce.code}`}
        </h3>

        <AnnounceMainData announce={announce}/>

        <AnnounceLotsList lots={announce.lots}/>

        <Row>
          <Col md={6}>
            <AnnouncePayments payments={announce.data && announce.data.payments}/>
          </Col>
        </Row>

        <Row>
          <Col>
            {error ?
              <Alert color="danger">{error}</Alert> :
              <Alert color="success">{t('Готово к публикации')}</Alert>
            }
            <Button className="m-2" to={`/purchaser/announce/draft`} color="secondary">{t('Назад')}</Button>

            {announce.status === 'Draft' &&
            <Fragment>
              <Button className="m-2" onClick={this.handleClickBasket}>
                {t('Редактировать позиции')}
              </Button>
              <Button className="m-2" to={`/purchaser/announce/edit/${announce._id}`}>{t('Редактировать')}</Button>
            </Fragment>
            }

            {announce.status === 'Draft' &&
            <Button className="m-2" color="success" disabled={!this.canPublish()}
                    onClick={this.publish}>{t('Опубликовать')}</Button>}
          </Col>
        </Row>

        {/*{(announce.status === 'Evaluation' || announce.status === 'Results') &&
          <div>
            <div className="d-flex justify-content-center">
              <h3>{t('Доп. инфо')}</h3>
            </div>

            <Nav tabs>
              <NavItem>
                <NavLink active={this.state.activeTab === 'Quotation'}
                         onClick={() => this.toggleTab('Quotation')}>
                  {t('Котировка цен')}
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink active={this.state.activeTab === 'Draft'}
                         onClick={() => this.toggleTab('Draft')}>
                  {t('История')}
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="Quotation">
                <Row>
                  <Col sm="12">
                    {this.state.activeTab === 'Quotation' && this.renderQuotation()}
                  </Col>
                </Row>
              </TabPane>

              <TabPane tabId="History">
                <Row>
                  <Col sm="12">
                    {this.state.activeTab === 'History' &&
                    <span>{t('History')}</span>}
                  </Col>
                </Row>
              </TabPane>

            </TabContent>
          </div>
          }*/}

      </>
    )
  }
}

@observer
class LotsList extends React.Component {
  render() {
    let {lots} = this.props;

    function getDeliveryPlace(address) {
      return compact([address.coate, address.street, address.house, address.apt]).join(', ')
    }

    const columns = [
      {Header: "Категория", accessor: "category.name"},
      {Header: "Количество", id: "quantity", accessor: "params.quantity"},
      {Header: "Ед. изм.", accessor: 'unit.value_name'},
      {
        Header: "Цена за ед.",
        accessor: "params.unit_price",
        Cell: ({value}) => formatMoney(value)
      },
      {
        Header: "План. сумма",
        id: "budget",
        accessor: lot => lot.params.unit_price * lot.params.quantity,
        Cell: ({value}) => formatMoney(value)
      },
      {
        Header: "Адрес и место поставки",
        accessor: "params.address",
        Cell: ({value}) => <span title={getDeliveryPlace(value)}>{getDeliveryPlace(value)}</span>
      },
      {Header: "Сроки пост. (дней)", accessor: "params.estimated_delivery_time"},
    ];

    return (
      <>
        <ReactTable data={lots}
                    pageSize={10}
                    minRows={1}
                    filterable={false}
                    showPagination={lots.length > 10}
                    showRowNumbers={true}
                    columns={columns}/>
      </>
    )
  }
}
