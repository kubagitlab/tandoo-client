import React from 'react';
import {Button, Form, Space, Switch, Tooltip, Typography} from "antd";
import {formatMoney} from "../../../../../../common/utils";
import {BsQuestionCircleFill} from "react-icons/bs";
import {action, computed, observable, runInAction} from "mobx";
import {observer} from "mobx-react";
import Input, {InputNumber} from "../../../../components/Input";
import {ConfirmButton} from "../../../../components/Button";

const {Text} = Typography;

const layout = {
  labelCol: {span: 8},
  wrapperCol: {span: 16},
};
const tailLayout = {
  wrapperCol: {offset: 8, span: 16},
};

@observer
export default class AnnEditLotParams extends React.Component {
  @observable section = null;
  @observable lot = null;
  @observable annEditMode = false;
  @observable changePrice = false;

  @observable plans = [];


  componentDidMount() {
    this.changePrice = (this.props.lot.params.unit_price !== this.props.lot.avg_unit_price);
  }

  async load(ann_id) {
    this.ann_id = ann_id;

    if (ann_id) {
      this.annEditMode = true;
      // await this.loadAnnounceLots(ann_id);

    } else {
      this.annEditMode = false;
      // await this.loadStorageLots();
    }

    await this.loadSelectedLot();

    this.ready = true;
  }

  async loadSelectedLot() {
    let fi = await storageGet('basketLot');

    if (!(fi && fi.section)) {
      return;
    }

    if (this.section) {
      if (this.section._id !== fi.section._id)
        return;
    } else {
      this.section = fi.section;
    }

    let lp = await storageGet('basketLotParams') || {};

    let {unit_price, dircategory, specifications: specs, dictionaries: dicts} = fi;
    let {estimated_delivery_time = '', address = {}} = lp;
    let {coate, street = '', house = '', apt = ''} = address;

    let dirunit_name, dirunits_id;
    let unit = dicts.find({dirname: 'DirUnits'});
    if (unit) {
      let {name, _id} = unit.values[0];
      dirunit_name = name;
      dirunits_id = _id
    } else {
      showError(this.props.t('Не выбрана единица измерения!'));
      debugger
    }

    runInAction(() => {
      this.lot = {
        category: dircategory.dircategory,
        specs,
        dicts,
        dirunit_name,
        dirunits_id,
        params: {
          quantity: '',
          unit_price,
          estimated_delivery_time,
          address: {coate, street, house, apt},
        }
      };

      this.getPlan();
    });
  }

  getPlan() {
    this.plans = [];
    if (this.lot) {
      request.post('plan/listing',).then(r => {
        this.plans = r.data;
      });
    }
  }

  @computed
  get lot_budget() {
    return this.props.lot.params.unit_price * this.props.lot.params.quantity;
    // let lot = this.props.params.lot;
    // if (lot && lot.params.quantity && (lot.params.quantity > 0)) {
    //   if (this.changePrice) {
    //     if (!this.priceError) {
    //       let price = parseNumber(this.price);
    //       return price * lot.params.quantity;
    //     }
    //
    //   } else if (lot.params.unit_price) {
    //     return lot.params.unit_price * lot.params.quantity;
    //   }
    // }
  }

  async storeLots() {
    let {lots, lot, section} = this;

    if (!this.annEditMode) {
      let basket = {lots, section};
      await storageSave('basket', basket);
    }

    if (lot) {
      let lp = omit(lot.params, ['quantity', 'unit_price']);
      await storageSave('basketLotParams', lp);
    }
  }

  @computed
  get canSaveAdvert() {
    return this.lots.length > 0 && !this.lot;
  }

  // saveAdvert = async (redirect) => {
  //   let _id = this.ann_id;
  //
  //   let params = {
  //     advert: {_id, dirsection_id: this.section._id,},
  //     advert_lots: this.lots.map(lot => ({
  //       _id: lot._id,
  //       planid: lot.planid,
  //       dircategory_id: lot.category.id,
  //       quantity: lot.params.quantity,
  //       unit_price: lot.params.unit_price,
  //       delivery_place: lot.delivery_place,
  //       estimated_delivery_time: lot.params.estimated_delivery_time,
  //       specs: lot.specs,
  //       dicts: lot.dicts,
  //       dirunits_id: lot.dirunits_id,
  //       data: {
  //         address: lot.params.address
  //       }
  //     }))
  //   };
  //
  //   if (this.annEditMode) {
  //     await announceApi.update_lots(params);
  //
  //   } else {
  //     _id = await announceApi.create(params);
  //     await storageRemove('basket');
  //   }
  //
  //   showSuccess(this.props.t('Успешно сохранено'));
  //
  //   if (redirect)
  //     this.props.history.push(`/purchaser/announce/edit/${_id}`);
  // };

  @action.bound
  setLotParam(path, value, type) {
    if (type === 'num') {
      value = parseInt(value)
    }

    set(this.lot.params, path, value);
  }

  canAddLot = () => {
    if (this.lot) {
      let {quantity, address, estimated_delivery_time} = this.lot.params;
      let {coate, street, house} = address;

      return !this.priceError && quantity && (quantity > 0) && street && house && coate && estimated_delivery_time &&
        estimated_delivery_time > 0;
    }
  };

  addLot = async () => {
    if (this.changePrice && !this.priceError) {
      this.lot.params.unit_price = parseNumber(this.price);
    }

    this.lots.push({
      ...this.lot,
      delivery_place: this.lotAddressToString(),
    });

    await this.storeLots();

    this.deleteLot(null, 'current', false);
  };

  lotAddressToString() {
    if (this.lot) {
      let {coate, street, house, apt} = this.lot.params.address;
      return reject([coate && coate.name, street, house, apt], isEmpty).join(', ');
    }
  }

  async deleteLot(e, index, confirm = true) {
    e && e.preventDefault();
    const {t} = this.props;

    if (confirm) {
      let {value} = await Swal({
        title: t('Вы действительно хотите удалить позицию?'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: t('Да'),
        cancelButtonText: t('Отмена')
      });

      if (!value) return;
    }

    if (index === 'current') {
      this.lot = null;
      this.announcePlan = [];
      storageRemove('basketLot');
      this.toggleChangePrice(false);

    } else {
      this.lots.splice(index, 1);
    }

    await this.storeLots();
  }

  async editLot(e, index) {
    e.preventDefault();
    const {t} = this.props;

    if (this.lot) {
      let {value} = await Swal({
        title: t('Сейчас вы редактируете не сохранённую позицию.'),
        text: t('Хотите удалить её?'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: t('Да'),
        cancelButtonText: t('Отмена')
      });

      if (!value) return;

      this.deleteLot(null, 'current', false)
    }

    this.lot = this.lots[index];
    this.getPlan();

    this.lots.splice(index, 1);
  }

  confirmReset = async () => {
    const {t} = this.props;
    let {value} = await Swal({
      title: t('Вы действительно хотите очистить перечень товаров?'),
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: t('Да'),
      cancelButtonText: t('Отмена')
    });

    if (!value) return;

    if (this.annEditMode) {
      this.lots = [];

    } else {
      await storageRemove('basket');
      await storageRemove('basketLot');

      this.props.history.push("/purchaser/catalog");
    }
  };

  @action
  toggleChangePrice(changePrice) {
    this.changePrice = changePrice;
    if (changePrice) {
      this.price = this.lot.params.unit_price
    }

    this.priceError = null;
  }

  setPrice(price) {
    this.price = price;
    price = parseNumber(this.price);

    if (price && price > 0 && price <= this.lot.params.unit_price) {
      // ok
      this.priceError = null;
    } else {
      // err
      this.priceError = 'Цена должна быть больше 0 и меньше {{max}}';
    }
  }

  onFinish = () => {
    let {onSave, lot} = this.props;

    if (!this.changePrice) {
      lot.params.unit_price = lot.avg_unit_price;
    }
    onSave && onSave()
  }

  onFinishFailed = () => {

  }

  render() {
    let {lot, canDelete, onDelete, onCancel} = this.props;

    if (!lot) return null;

    return (
      <>
        <Form
          {...layout}
          onFinish={this.onFinish}
          onFinishFailed={this.onFinishFailed}
          initialValues={lot}
        >
          <Form.Item name="sectionName" label="Раздел">
            <Text>{lot.section.name}</Text>
          </Form.Item>
          <Form.Item name="categoryName" label="Категория">
            <Text>{lot.category.name}</Text>
          </Form.Item>
          {lot.dirs.map((v, i) => (
            <Form.Item name={`dirs[${i}]`} key={v.value_id} label={v.entity_name}>
              <Text>{v.value_name}</Text>
            </Form.Item>
          ))}
          {lot.specs.map((v, i) => (
            <Form.Item name={`specs[${i}]`} key={v.value_id} label={v.entity_name}>
              <Text>{v.value_name}</Text>
            </Form.Item>
          ))}
          <Form.Item name="avgPrice" label="Цена за единицу:">
            <Text>
              {this.changePrice ? (
                <s>{formatMoney(lot.avg_unit_price)}</s>
              ) : formatMoney(lot.avg_unit_price)}
              {' '}
              (Средняя цена товара из Каталога)
            </Text>
          </Form.Item>

          <Form.Item label={
            <span>
               <Tooltip title={
                 'Если средняя цена, на данный товар в Каталоге оказалась выше чем стоимость ' +
                 'товара по Плану закупок, Закупающая организация может указать стоимость ' +
                 'для данной позиции согласно своему плану закупок'
               }>
                 <BsQuestionCircleFill/>
               </Tooltip>
              &nbsp;
              Указать свою цену
            </span>
          }>
            <Switch checked={this.changePrice} onChange={v => this.changePrice = v}/>

            {this.changePrice &&
            <Form.Item noStyle name={["params", "unit_price"]}
                       rules={[{required: true, message: 'Пожалуйста, введите цену'}]}>

              <InputNumber model={lot.params} name="unit_price" step={0.01} min={0} autoFocus
                           style={{marginLeft: 5}} placeholder="число"/>
            </Form.Item>
            }
          </Form.Item>

          <Form.Item name="lot_budget" label="Планируемая сумма">
            <Text>
              {
                this.lot_budget ?
                  formatMoney(this.lot_budget) :
                  this.changePrice ?
                    'Укажите цену и количество' :
                    'Укажите количество'
              }
            </Text>
          </Form.Item>
          <Form.Item name="plan" label="План">
            <Text>TODO</Text>
          </Form.Item>
          <Form.Item name={['params', 'quantity']} label="Количество" rules={[{required: true}]}>
            <InputNumber model={lot.params} name={'quantity'} min={0} autoFocus placeholder="число"/>
          </Form.Item>

          <Form.Item name="unit" label="Единица измерения">
            <Text>{lot.unit.value_name}</Text>
          </Form.Item>
          <Form.Item name={['params', 'address', 'coate']} label="Адрес и место поставки"
                     rules={[{required: true}]}>
            <Input model={lot.params.address} name="coate"/>
          </Form.Item>
          <Form.Item name={['params', 'address', 'street']} label="Улица"
                     rules={[{required: true, message: 'Пожалуйста, укажите улицу'}]}>
            <Input model={lot.params.address} name="street"/>
          </Form.Item>
          <Form.Item name={['params', 'address', 'house']} label="№ дома" rules={[{required: true}]}>
            <Input model={lot.params.address} name="house"/>
          </Form.Item>
          <Form.Item name={['params', 'address', 'apt']} label="Квартира">
            <Input model={lot.params.address} name="apt"/>
          </Form.Item>
          <Form.Item name={['params', 'estimated_delivery_time']} label="Срок поставки (дней)"
                     rules={[{required: true, message: 'Пожалуйста, укажите срок поставки'}]}>
            <InputNumber model={lot.params} name="estimated_delivery_time" min={0} placeholder="число"/>
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Space>
              <Button type="primary" htmlType="submit">
                Сохранить
              </Button>
              <Button onClick={onCancel}>
                Закрыть
              </Button>
              {canDelete &&
              <ConfirmButton danger onConfirm={onDelete} placement="top"
                             question="Вы хотите удалить эту позицию?">
                Удалить
              </ConfirmButton>
              }
            </Space>
          </Form.Item>
        </Form>

      </>
    )

    /*
            <Select options={this.plans}
                    valueKey={"id"}
                    simpleValue
                    value={this.lot && this.lot.planid}
                    labelKey={'accountTitle'}
                    optionComponent={PlanList}
                    valueComponent={PlanValue}
                    onChange={item => {
                      if (this.lot) {
                        this.lot.planid = item;
                      }
                    }}
            />

              <CoateSelect placeholder={t('Выберите')}
                           valueKey="id"
                           value={address.coate}
                           onChange={coate => setLotParam('address.coate', coate)}/>

              <InputMask className="form-control"
                         mask="99999999"
                         placeholder={t('Введите число')}
                         maskChar={null}
                         value={lot.params.estimated_delivery_time}
                         onChange={e => setLotParam('estimated_delivery_time', e.target.value, 'num')}/>
    */


  }
}
