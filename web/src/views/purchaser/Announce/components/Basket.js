import React from 'react';
import {formatMoney} from "../../../../../../common/utils";
import ReactTable from "../../../../components/ReactTable";
import {observer} from "mobx-react";
import {Divider, Modal, Space, Tooltip} from "antd";
import Button, {ConfirmButton} from "../../../../components/Button";
import {MdModeEdit, MdDelete, MdAdd, MdSave} from "react-icons/md";
import {action, observable, toJS} from "mobx";
import find from "lodash/find";
import appStore from "../../../../../../store/AppStore";
import {IconContext} from "react-icons";
import ProductListPur from "../../Catalog/ProductListPur";
import LotParams from "./LotParams";
import confirmModal from '../../../../components/confirmModal';
import compact from "lodash/compact";
import commonApi from "../../../../../../apis/common";

const actionsIconParams = {size: '1.3em', style: {verticalAlign: 'text-bottom', marginRight: '0.2em'}};

@observer
export default class AnnEditBasketTableView extends React.Component {
  @observable plVisible = false;
  @observable lpVisible = false;
  @observable lot = null;
  @observable lotIndex = null;

  @action.bound
  togglePl() {
    this.plVisible = !this.plVisible;
  }

  @action.bound
  toggleLp() {
    this.lpVisible = !this.lpVisible;
    if (!this.lpVisible) {
      this.lot = null;
      this.lotIndex = null;
    }
  }

  onSelectProduct = async (data) => {
    // console.log('add p', data);

    let {product, category, section, filters, activeFilters} = data;
    let {dirs, specs} = activeFilters;

    dirs = dirs.map(({entity_id, value_id}) => {
      let dir = find(filters.dirs, {id: entity_id});
      let val = find(dir.values, {id: value_id});
      let dir_name = val.name;
      return {entity_id, value_id, entity_name: dir.name, value_name: val.value, dir_name}
    });

    specs = specs.map(({entity_id, value_id}) => {
      let spec = find(filters.specs, {id: entity_id});
      let val = find(spec.values, {id: value_id});
      return {entity_id, value_id, entity_name: spec.name, value_name: val.name}
    })

    let unit = find(dirs, {dir_name: 'DirMeasureUnit'});

    if (!unit || (dirs.length + specs.length) < 2) {
      appStore.setAlert("warn", 'Укажите единицу измерения и хотя бы один параметр');
      return false;
    }

    let lp = appStore.user.data && appStore.user.data.basketLotParams || {};
    let {estimated_delivery_time = '', address = {}} = lp;
    let {coate, street = '', house = '', apt = ''} = address;

    let avg_unit_price = product.avg_price;

    let lot = {
      id: null,
      category,
      section: {
        id: section.id,
        name: section.name
      },
      avg_unit_price,
      unit,
      dirs,
      specs,
      params: {
        quantity: '',
        unit_price: avg_unit_price,
        estimated_delivery_time,
        address: {coate, street, house, apt},
      }
    };

    // console.log('lot', lot);
    this.lot = lot;
    this.lpVisible = true;
  }

  onSaveLp = async lp => {
    try {
      if (this.lotIndex !== null) {
        this.props.lots[this.lotIndex] = this.lot;
      } else {
        this.props.lots.push(this.lot);

        let {estimated_delivery_time, address} = this.lot.params;
        let lp = {estimated_delivery_time, address};
        await commonApi.saveUserData({basketLotParams: lp});
      }

      this.plVisible = false;
      this.lpVisible = false;
      this.lotIndex = null;
      this.lot = null;
    } catch (e) {
      console.warn(e)
    }
  }

  onReset = async () => {
    try {
      await confirmModal('Вы уверены?', 'Это действие удалит все позиции');
      this.props.lots.clear();
    } catch (e) {
    }
  }

  @action.bound
  editLot(index) {
    // clone
    this.lot = toJS(this.props.lots[index]);
    this.lotIndex = index;
    this.lpVisible = true;
  }

  @action
  deleteLot(index) {
    this.props.lots.remove(this.props.lots[index]);
  }

  @action.bound
  lpDeleteLot() {
    this.deleteLot(this.lotIndex);
    this.lotIndex = null;
    this.lot = null;
    this.lpVisible = false;
  }

  render() {
    let {section, lots, onSave} = this.props;

    function getDeliveryPlace(address) {
      return compact([address.coate, address.street, address.house, address.apt]).join(', ')
    }

    const columns = [
      {Header: "Категория", accessor: "category.name"},
      {Header: "Количество", id: "quantity", accessor: "params.quantity"},
      {Header: "Ед. изм.", accessor: 'unit.value_name'},
      {
        Header: "Цена за ед.",
        accessor: "params.unit_price",
        Cell: ({value}) => formatMoney(value)
      },
      {
        Header: "План. сумма",
        id: "budget",
        accessor: lot => lot.params.unit_price * lot.params.quantity,
        Cell: ({value}) => formatMoney(value)
      },
      {
        Header: "Адрес и место поставки",
        accessor: "params.address",
        Cell: ({value}) => <span title={getDeliveryPlace(value)}>{getDeliveryPlace(value)}</span>
      },
      {Header: "Сроки пост. (дней)", accessor: "params.estimated_delivery_time"},
      {
        Header: "",
        Cell: ({index}) =>
          <span className="table-actions">
            <Tooltip title="Редактировать">
              <Button size="small" type="link" onClick={() => this.editLot(index)}>
                <MdModeEdit/>
              </Button>
            </Tooltip>
            <Tooltip title="Удалить">
              <ConfirmButton size="small" type="link" placement="bottom"
                             question="Вы хотите удалить эту позицию?"
                             onConfirm={() => this.deleteLot(index)}>
                <MdDelete/>
              </ConfirmButton>
            </Tooltip>
          </span>
      }
    ];

    return (
      <>
        <ReactTable data={lots}
                    pageSize={10}
                    minRows={1}
                    filterable={false}
                    showPagination={lots.length > 10}
                    showRowNumbers={true}
                    columns={columns}/>

        <Divider/>


        <IconContext.Provider value={actionsIconParams}>
          <Space>
            {/*<Button color="secondary" className="mr-2" to={`/purchaser/announce/preview/${this.ann_id}`}>
                {t('Назад')}
              </Button>*/}

            <Button onClick={this.onReset} danger icon={<MdDelete/>} disabled={!lots.length}>
              Удалить все
            </Button>

            <Button icon={<MdAdd/>} onClick={this.togglePl}>
              Добавить товар (позицию)
            </Button>

            <Button onClick={() => onSave()} disabled={!lots.length} type="primary" icon={<MdSave/>}>
              Сохранить
            </Button>

            <Button onClick={() => onSave(true)} disabled={!lots.length} type="primary" icon={<MdSave/>}>
              Сохранить и продолжить
            </Button>
          </Space>
        </IconContext.Provider>

        <Modal visible={this.plVisible}
               onCancel={this.togglePl}
               width="100%"
               style={{top: 0}}
               footer={null}
        >
          <ProductListPur lockSection={section} onAdd={this.onSelectProduct}/>
        </Modal>

        <Modal visible={this.lpVisible}
               onCancel={this.toggleLp}
               width="100%"
               style={{top: 0}}
               footer={null}
        >
          {this.lpVisible &&
          <LotParams lot={this.lot} onSave={this.onSaveLp} canDelete={this.lotIndex !== null}
                     onCancel={this.toggleLp} onDelete={this.lpDeleteLot}/>
          }
        </Modal>
      </>
    )
  }
}
