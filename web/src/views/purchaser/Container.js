import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import constants from '../../../../common/constants';
import routes from '../../routes/purchaser';
import Wrapper from '../../components/Wrapper';

export default ({user}) => {

  if (!user) {
    return <Redirect to="/"/>;
  }

  if ([constants.ROLE_ADMIN, constants.ROLE_SUPER_ADMIN, constants.ROLE_OPERATOR].includes(user.role_id)) {
    return <Redirect to="/adm"/>;
  }

  if (user.role_id === constants.ROLE_SUPPLIER) {
    return <Redirect to="/sup"/>;
  }

  return (
    <Wrapper>
      <Switch>
        <Redirect exact from="/sup" to="/"/>
        {routes.map(r => <Route key={r.path} path={r.path} name={r.title} component={r.component}/>)}
      </Switch>
    </Wrapper>
  );
}
