import React from 'react';
import {formatDateTime} from "../../../../../common/utils";
import ReactTable from "../../../components/ReactTable";
import {Typography} from "antd";

const {Title} = Typography;

const columns = [
  {
    Header: '№',
    accessor: 'id',
    className: 'text-center',
    headerClassName: 'f-bold',
  },
  {
    Header: 'Дата',
    accessor: 'date',
    className: 'text-center',
    headerClassName: 'f-bold',
    Cell: ({value}) => formatDateTime(value),
  },

  {
    Header: 'Текст сообщения',
    accessor: 'text',
    className: 'text-center',
    headerClassName: 'f-bold',
  },
];

const data = [
  {
    id: '20081074341',
    date: '2020-08-10 15:12:46',
    text: 'Изделия медицинского назначения',
  },
  {
    id: '20081074341',
    date: '2020-08-10 15:12:46',
    text: 'Изделия медицинского назначения',
  },
  {
    id: '20081074341',
    date: '2020-08-10 15:12:46',
    text: 'Изделия медицинского назначения',
  },
];

export default () => {
  return (
    <>
      <Title level={4}>По срокам истечения цен на продукцию</Title>
      <ReactTable data={data} columns={columns} pageSize={10}/>
    </>
  );
};
