import React from 'react';
import {List} from 'antd';
import {Link} from "react-router-dom";

const data = [
  {
    id: 1,
    title: 'New 1',
    description: 'Item 1 desc'
  },
  {
    id: 2,
    title: 'New 2',
    description: 'Item 2 desc'
  },
  {
    id: 3,
    title: 'New 3',
    description: 'Item 3 desc'
  },
  {
    id: 4,
    title: 'New 4',
    description: 'Item 4 desc'
  },
];

export default () => {
  return (
    <List
      dataSource={data}
      renderItem={({id, title, description}) => (
        <List.Item>
          <List.Item.Meta
            title={<Link to={`/pur/msg/${id}`}>{title}</Link>}
            description={description}
          />
        </List.Item>
      )}
    />
  )
}
