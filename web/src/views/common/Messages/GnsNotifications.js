import React from 'react';
import {formatDateTime} from "../../../../../common/utils";
import ReactTable from "../../../components/ReactTable";
import {Typography} from "antd";

const {Title} = Typography;

const columns = [
  {
    Header: 'Дата',
    accessor: 'datetime',
    className: 'text-center',
    headerClassName: 'f-bold',
    width: 150,
    Cell: ({value}) => formatDateTime(value),
  },

  {
    Header: 'Текст сообщения',
    accessor: 'description',
    className: 'text-center',
    headerClassName: 'f-bold',
    Cell: ({value}) => <a>{value}</a>,
  },
];
const data = [
  {
    company_id: 'ec313b8d-2472-4f8b-bb60-17f79ca99ea0',
    data: {},
    datetime: '2020-08-11 00:00:00+06',
    description: 'Срок действия справки Справка УГНС истекает через -1 дней, просим Вас обновить',
    description_en: 'STS Information - Reference expires in -1  days',
    description_kg: 'МСК маалымдамасы - Маалымат кагазынын жарактуу мөөнөтү -1 күндөн кийин аяктайт',
    id: 2568,
    notification_status: 'active',
    title: 'Требуется обновления Справка УГНС',
    title_en: 'Update required STS Information',
    title_kg: 'Жаңылоо талап кылынат МСК маалымдамасы',
    type: 'CompanyDocs',
  },
  {
    company_id: 'ec313b8d-2472-4f8b-bb60-17f79ca99ea0',
    data: {},
    datetime: '2020-08-11 00:00:00+06',
    description: 'Срок действия справки Справка СФ истекает через -1 дней, просим Вас обновить',
    description_en: 'SF Information - Reference expires in -1  days',
    description_kg: 'СФ маалымдамасы - Маалымат кагазынын жарактуу мөөнөтү -1 күндөн кийин аяктайт',
    id: 2569,
    notification_status: 'active',
    title: 'Требуется обновления Справка СФ',
    title_en: 'Update required SF Information',
    title_kg: 'Жаңылоо талап кылынат СФ маалымдамасы',
    type: 'CompanyDocs',
  },
];

export default () => {
  return (
    <>
      <Title level={4}>Справки ГНС, СФ</Title>
      <ReactTable data={data} columns={columns} pageSize={10}/>
    </>
  );
};
