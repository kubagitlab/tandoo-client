import React from 'react';
import {Card, PageHeader, Tabs} from 'antd';
import AdmNotifications from './AdmNotifications';
import Requests from './Requests';
import News from './News';
import RequestsToAdd from './RequestsToAdd';
import AnnNotifications from './AnnNotifications';
import GnsNotifications from './GnsNotifications';
import PriceNotifications from './PriceNotifications';
import {inject, observer} from "mobx-react";

const {TabPane} = Tabs;

@inject('appStore')
@observer
export default class Messages extends React.Component {
  render() {
    let {isPurchaser, isSupplier} = this.props.appStore;

    return (
      <>
        <PageHeader title="Сообщения"/>

        <Card>
          <Tabs tabPosition="left">
            <TabPane tab="Уведомления от администрации" key="1">
              <AdmNotifications/>
            </TabPane>

            <TabPane tab="Запросы / Разъяснения" key="2">
              <Requests/>
            </TabPane>

            {isSupplier &&
            <TabPane tab="Запросы на добавление продукции" key="3">
              <RequestsToAdd/>
            </TabPane>}

            <TabPane tab="Новости" key="4">
              <News/>
            </TabPane>

            {isSupplier &&
            <>
              <TabPane tab="По поданным объявлениям" key="5">
                <AnnNotifications/>
              </TabPane>

              <TabPane tab="Справки ГНС, СФ" key="6">
                <GnsNotifications/>
              </TabPane>

              <TabPane tab="По срокам истечения цен на продукцию" key="7">
                <PriceNotifications/>
              </TabPane>
            </>}

          </Tabs>
        </Card>
      </>
    )
  }
}
