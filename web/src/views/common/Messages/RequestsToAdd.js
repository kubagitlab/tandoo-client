import React from 'react';
import {formatDateTime} from "../../../../../common/utils";
import {Tag, Typography} from "antd";
import ReactTable from "../../../components/ReactTable";

const {Title} = Typography;

const column = [
  {
    Header: 'Наименование товара',
    accessor: 'name',
    className: 'text-center',
    headerClassName: 'f-bold',
  },
  {
    Header: 'Дата подачи',
    accessor: 'date_start',
    className: 'text-center',
    headerClassName: 'f-bold',
    width: 150,
    Cell: ({value}) => formatDateTime(value),
  },
  {
    Header: 'Дата ответа',
    accessor: 'date_end',
    className: 'text-center',
    headerClassName: 'f-bold',
    Cell: ({value}) => formatDateTime(value),
  },
  {
    Header: 'Статус',
    accessor: 'status',
    className: 'text-center',
    headerClassName: 'f-bold',
    Cell: ({value}) => <Tag color="green">{value}</Tag>,
  },
];
const data = [
  {
    date_start: '2020-08-11 00:00:00+06',
    date_end: '2020-00-09 10:00:00+06',
    status: 'active',
    name: 'Требуется обновления Справка УГНС',
  },
  {
    date_start: '2020-08-11 00:00:00+06',
    date_endt: '2020-00-15 10:00:00+06',
    status: 'active',
    name: 'Требуется обновления Справка УГНС',
  },
];

export default () => {
  return (
    <>
      <Title level={4}>Запросы на добавление товара</Title>
      <ReactTable data={data} columns={column} pageSize={10}/>
    </>
  )
}
