import React from 'react';
import {List} from 'antd';
import {Link} from "react-router-dom";

const data = [
  {
    id: 1,
    title: 'Req 1',
    description: 'Item 1 desc'
  },
  {
    id: 2,
    title: 'Req 2',
    description: 'Item 2 desc'
  },
  {
    id: 3,
    title: 'Req 3',
    description: 'Item 3 desc'
  },
  {
    id: 4,
    title: 'Req 4',
    description: 'Item 4 desc'
  },
];

export default () => {
  return (
    <List
      dataSource={data}
      renderItem={({id, title, description}) => (
        <List.Item>
          <List.Item.Meta
            title={<Link to={`/pur/msg/${id}`}>{title}</Link>}
            description={description}
          />
        </List.Item>
      )}
    />
  )
}
