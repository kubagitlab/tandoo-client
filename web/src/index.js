import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'mobx-react';
import {I18nextProvider} from 'react-i18next';
import {ConfigProvider} from 'antd';
import ruRu from 'antd/es/locale/ru_RU';
import 'numeral/locales/ru';
import 'moment/locale/ru';
import i18n from '../../translate/i18n';
import 'antd/dist/antd.css';
import './styles/style.scss';
import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';
import stores from '../../store/stores';
import Root from './Root';
import {HashRouter as Router} from 'react-router-dom';
import {IconContext} from 'react-icons';

const App = () => (
  <Provider {...stores}>
    <I18nextProvider i18n={i18n}>
      <ConfigProvider locale={ruRu}>
        <IconContext.Provider value={{className: 'react-icons'}}>
          <Router>
            <Root/>
          </Router>
        </IconContext.Provider>
      </ConfigProvider>
    </I18nextProvider>
  </Provider>
)

ReactDOM.render(<App/>, document.getElementById('root'));
