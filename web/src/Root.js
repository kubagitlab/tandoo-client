import React from 'react';
import {Route, withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import {BackTop, notification, Skeleton, Spin} from 'antd';
import Home from './views/public/Home';
import AdminContainer from './views/admin/Container';
import SupplierContainer from './views/supplier/Container';
import PurchaserContainer from './views/purchaser/Container';
import Header from './components/Header';
import publicRoutes from './routes/public';
import KC from './views/public/KC';
import loadable from "@loadable/component";
import appStore from "../../store/AppStore";

import {KeycloakProvider} from "@react-keycloak/web";
import Keycloak from 'keycloak-js'

const TEST_PORTAL = 'https://cs.zakupki.gov.kg/auth/';
const PROD_PORTAL = 'https://eds.zakupki.gov.kg/auth/';

let serverUrl = PROD_PORTAL

const origin = window.location.origin;

if (origin === "http://tandoo.university.kg" || origin === "http://localhost:8081") {
  serverUrl = TEST_PORTAL;
}

const keycloak = new Keycloak({
  url: serverUrl,
  realm: 'etender',
  clientId: 'tandoo-service-frontend',
  onLoad: 'login-required',
  "ssl-required": "all",
  "resource": "tandoo-service-frontend",
  "public-client": true,
  "confidential-port": 0,
});

const redirectUrl = window.location.origin + '/#/keycloak';

const keycloakInit = {
  // onLoad: 'login-required',
  onLoad: 'check-sso',
  // redirectUri: redirectUrl,
  enableLogging: true,
  responseMode: 'query',
};

@inject('appStore')
@observer
class Root extends React.Component {
  constructor(props) {
    super(props);
    this.state = {loading: true};
  }

  async componentDidMount() {
    const {appStore} = this.props;
    try {
      await appStore.loadData();
    } catch (e) {
    }
    this.setState({loading: false});
  }

  onKeycloakEvent = (event, error) => {
    // console.log('onKeycloakEvent', event, error)
  };

  onKeycloakTokens = (tokens) => {
    if (tokens.token) {
      appStore.keycloakLogin(tokens.token).then(res => {
        if (res.company && res.company.company_status === "draft" && res.company.company_type === "supplier") {
          this.props.history.replace('/sup/company/qualification')
        }
      });
    } else {
      appStore.logout();
    }
  };

  render() {
    const {appStore} = this.props;
    const {level, user, isBusy} = appStore;
    const {loading} = this.state;

    if (appStore.message) {
      this.renderAlert(level, appStore.message);
    }

    if (loading) {
      return <Skeleton active/>;
    }

    return (
      <KeycloakProvider
        keycloak={keycloak}
        initConfig={keycloakInit}
        onEvent={this.onKeycloakEvent}
        onTokens={this.onKeycloakTokens}
      >
        <Spin spinning={isBusy}>
          <Header/>
          <Route exact path="/" name="Home" component={Home}/>
          <Route path="/keycloak" name="Keycloak" component={KC}/>
          {!user && publicRoutes.map(r => <Route exact key={r.path} path={r.path} name={r.title}
                                                 component={r.component}/>)}
          <Route path="/adm" name="Admin">
            <AdminContainer user={user}/>
          </Route>
          <Route path="/sup" name="Supplier">
            <SupplierContainer user={user}/>
          </Route>
          <Route path="/pur" name="Purchaser">
            <PurchaserContainer user={user}/>
          </Route>
          <Route path="/messages" name="messages" component={loadable(() =>
            import('./views/common/Messages'))}/>
          {/*<Redirect from="*" to="/"/>*/}
          <BackTop/>
        </Spin>
      </KeycloakProvider>
    );
  }

  renderAlert = (level, msg) => {
    if (msg) {
      notification[level]({message: msg});
      this.props.appStore.clearAlert();
    }
  };
}

export default withRouter(Root);

