import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

export default ({value, onChange, labelKey, valueKey, data, placeholder, optionRenderer, valueRenderer, ...rest}) => (
  <Select
    className="custom-select"
    value={value}
    valueKey={valueKey}
    labelKey={labelKey}
    placeholder={placeholder || 'выберите...'}
    loadingPlaceholder={'загрузка...'}
    noResultsText="пусто"
    options={data || []}
    optionRenderer={optionRenderer}
    valueRenderer={valueRenderer}
    onChange={onChange}
    {...rest}
  />
);
