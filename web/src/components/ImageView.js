import React, {useEffect, useState} from 'react';
import {BASE_URL} from '../../../common/requester';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import {Img} from 'react-image'
import {Spin} from "antd";
import {ConfirmButton} from './Button';

const path = BASE_URL + 'file/';
const thumbUrl = BASE_URL + 'thumb/';

export default ({id, thumbnail, url, onRemove, onClick, ...rest}) => {

  const [show, toggle] = React.useState(false);
  const [src, setSrc] = React.useState(null);
  const [images, setImages] = React.useState([]);

  useEffect(() => {
    let imgs = [];
    if (id) {
      setSrc(`${thumbnail ? thumbUrl : path}${id}`);
      imgs.push(`${path}${id}`);
    } else if (url) {
      setSrc(url);
      imgs.push(url);
    }
    setImages(imgs);
  }, [id, url]);

  if (!src) return <img src={'../assets/img/no_image.png'} {...rest} alt=''/>;

  const {width, height} = rest;

  return <>
    <div style={{position: 'relative', width: width, float: 'left', marginRight: 8, marginBottom: 8}}>
      <Img src={[src, '../assets/img/no_image.png']}
           className={'img'}
           {...rest}
           width={width}
           height={height}
           loader={<Spin/>}
           onClick={() => {
             if (onClick) {
               onClick(id, url, src);
             } else {
               toggle(true)
             }
           }}/>
      {onRemove && <ConfirmButton size="small"
                                  danger
                                  style={{position: 'absolute', right: 5, top: 5}}
                                  shape="circle"
                                  onConfirm={() => {
                                    onRemove(id, url, src);
                                  }}>
        X
      </ConfirmButton>}
    </div>
    {!onClick && <ImagePreview show={show} urls={images} onClose={() => toggle(false)}/>}
  </>;
}

export const ImagePreview = ({ids, urls, show, onClose}) => {
  const [images, setImages] = useState([]);
  const [photoIndex, setPhotoIndex] = useState(0);

  useEffect(() => {
    let imgs = [];
    if (ids) {
      ids.forEach(id => imgs.push(`${path}${id}`));
      setImages(imgs);
    } else if (urls) {
      setImages(urls);
    }
  }, [ids]);

  if (!images || images.length === 0) return null;

  return (
    show &&
    <Lightbox
      mainSrc={images[photoIndex]}
      nextSrc={images[(photoIndex + 1) % images.length]}
      prevSrc={images[(photoIndex + images.length - 1) % images.length]}
      onCloseRequest={() => {
        onClose();
        setPhotoIndex(0);
      }}
      onMovePrevRequest={() => setPhotoIndex((photoIndex + images.length - 1) % images.length)}
      onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % images.length)}
    />
  )
};
