import React from 'react';
import InputFiles from 'react-input-files';
import Button from "./Button";
import commonApi from "../../../apis/common";
import appStore from "../../../store/AppStore";
import {observer} from "mobx-react";
import {MdFileUpload} from 'react-icons/md'
import ImgCrop from "antd-img-crop";
import {Upload} from "antd";

export default observer(({fileType, onUploaded, title, accept, onFailed, notImage, notCrop}) => {

  const onFilePicked = async (files) => {
    for (let i = 0; i < files.length; i++) {
      const file = files[i];

      let fd = new FormData();
      fd.append('file', file, file.name);
      if (fileType) {
        fd.append('file_type', fileType);
      }

      try {
        const ret = await commonApi.upload(fd);
        if (onUploaded) {
          onUploaded(ret.file);
        }
      } catch (e) {
        if (onFailed) {
          onFailed(e);
        }
      }
    }
  };

  const uploadRequest = ({file, onSuccess, onError}) => {

    let fd = new FormData();
    fd.append('file', file, file.name);
    if (fileType) {
      fd.append('file_type', fileType);
    }
    commonApi.upload(fd).then(ret => {
      onSuccess(ret.file);
      if (onUploaded) {
        onUploaded(ret.file);
      }
    }).catch(e => {
      onError(e);
      if (onFailed) {
        onFailed(e);
      }
    });
  };

  const onPreview = async file => {
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  const button = (
    <Button disabled={!appStore.token} icon={<MdFileUpload style={{marginRight: 10}}/>}>
      {title || 'Загрузить'}
    </Button>
  );

  if (notImage) {
    return (
      <InputFiles accept={accept} onChange={onFilePicked}>
        {button}
      </InputFiles>
    )
  }

  const uploadButton = (
    <Upload data={{file_type: fileType}}
            customRequest={uploadRequest}
            showUploadList={false}
            onPreview={onPreview}
    >
      {button}
    </Upload>
  );

  if (notCrop) {
    return uploadButton
  }

  return (
    <ImgCrop rotate grid modalTitle={'Crop Image'} modalOk={'OK'} modalCancel={'Отмена'}>
      {uploadButton}
    </ImgCrop>
  );
})

