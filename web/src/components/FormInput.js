import React from 'react';
import {Form} from "antd";
import Input from "./Input";
import ReactSelect from "./ReactSelect";
import Uploader from "./Uploader";
import DatePicker from "./DatePicker";

const FormInput = ({label, model, name, disabled, rules}) => (
  <Form.Item label={label} model={model} name={name} rules={rules}>
    <Input disabled={disabled} model={model} name={name}/>
  </Form.Item>
);

export default FormInput

export const FormHiddenInput = ({label, name}) => {
  return (
    <Form.Item label={label} name={name}>
      <Input type="hidden"/>
    </Form.Item>
  );
};

export const FormSelect = ({label, model, name, disabled, rules, ...rest}) => (
  <Form.Item label={label} model={model} name={name} rules={rules}>
    <ReactSelect disabled={disabled} model={model} name={name} {...rest}/>
  </Form.Item>
);

export const FormUpload = ({label, model, name, disabled, rules, uploaderProps}) => (
  <Form.Item label={label} model={model} name={name} rules={rules}>
    <Uploader {...uploaderProps}/>
  </Form.Item>
);

export const FormDate = ({label, model, name, disabled, rules, ...rest}) => (
  <Form.Item label={label} model={model} name={name} rules={rules}>
    <DatePicker {...rest}/>
  </Form.Item>
);
