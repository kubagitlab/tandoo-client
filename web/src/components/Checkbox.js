import React from 'react';
import {observer} from 'mobx-react';
import {runInAction} from 'mobx';
import {Checkbox as AntCheckbox} from 'antd';

@observer
export default class Checkbox extends React.Component {
  onChange = val => {
    const {model, name, onChange, disabled} = this.props;
    if (disabled) return;
    if (model && name) {
      runInAction(() => (model[name] = val));
    }
    if (onChange) {
      onChange(val);
    }
  };

  render() {
    const {model, name, checked, label, disabled} = this.props;
    let ch = checked;
    if (model && name) {
      ch = model[name];
    }

    return (
      <AntCheckbox
        disabled={disabled}
        checked={ch}
        onChange={e => this.onChange(e.target.checked)}>
        {label}
      </AntCheckbox>
    );
  }
}
