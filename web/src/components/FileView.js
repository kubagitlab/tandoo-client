import React, {useEffect} from 'react';
import {BASE_URL} from '../../../common/requester';
import {getFileExtension} from "../../../common/utils";

const path = BASE_URL + 'file/';

export default ({id, url, fileName}) => {

  const [src, setSrc] = React.useState(null);

  useEffect(() => {
    if (id) {
      setSrc(`${path}${id}`);
    } else if (url) {
      setSrc(url);
    }
  }, [id, url]);

  const ext = getFileExtension(fileName);

  return <>
    <span className={`fiv-sqo fiv-icon-${ext}`}/>
    &nbsp;&nbsp;<a href={src}>{fileName}</a>
  </>;
}
