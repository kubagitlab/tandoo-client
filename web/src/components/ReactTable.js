import React from 'react';
import RT from 'react-table-v6'
import 'react-table-v6/react-table.css'

const translations = {
  previousText: '⇦',
  nextText: '⇨',
  loadingText: 'ЗАГРУЗКА...',
  rowsText: 'шт',
  pageText: 'стр',
  ofText: 'из',
  noDataText: 'нет данных',
};

export default ({columns, data, sortable, filterable, pageSize, onClick, showRowNumbers, ...rest}) => {

  let c = [];
  if (showRowNumbers === undefined || showRowNumbers) {
    c.push({
      Header: "№",
      Cell: row => <div className="text-center">{row.index + 1}</div>, width: 50
    });
  }

  c.push(...columns || []);

  return (
    <RT
      {...translations}
      defaultFilterMethod={(filter, row) =>
        String(row[filter.id]).toLocaleLowerCase().includes(filter.value.toLocaleLowerCase())}
      data={data || []}
      columns={c || []}
      defaultPageSize={pageSize || 25}
      className="-striped -highlight"
      sortable={sortable === undefined ? true : sortable}
      filterable={filterable === undefined ? true : filterable}
      getTdProps={(state, rowInfo, column, instance) => ({
        onClick: (e, handleOriginal) => {
          if (column.expander)
            handleOriginal();

          else if (onClick && rowInfo) {
            onClick(rowInfo.row._original, column, handleOriginal);
          }
        }
      })}
      {...rest}
    />
  )
}
