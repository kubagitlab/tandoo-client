import {Modal} from 'antd';

// TODO: level: info, success, error, warning, confirm
export default function confirmModal(title, content, throws = true) {
  return new Promise((resolve, reject) => {
    const modal = Modal.confirm({
      title,
      content,
      maskClosable: true,
      onOk: () => {
        modal.destroy();
        resolve(true);
      },
      onCancel: () => {
        modal.destroy();

        if (throws) {
          reject('NOT_CONFIRMED')
        } else {
          resolve(false);
        }
      },
    })
  })
}
