import React from 'react';
import {Layout} from 'antd';

export default ({children}) => {
  return (
    <Layout style={{padding: '0 15px'}}>
      {children}
    </Layout>
  );
};
