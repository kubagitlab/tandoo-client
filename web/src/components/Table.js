import React from "react"
import {Table as AntTable} from "antd";
import {observer} from "mobx-react";

const Table = ({columns, data, loading, bordered, dontShowTableWhenEmpty, ...rest}) => {
    if ((!data || data.length === 0) && dontShowTableWhenEmpty) return null;
    let cols = [{
      title: '#',
      key: 'order_no',
      width: 30,
      render: (text, record, index) => <span>{index + 1}</span>,
    }, ...columns];
    return (
      <AntTable
        columns={cols}
        dataSource={data}
        size='small'
        bordered={bordered === undefined ? true : bordered}
        loading={loading}
        rowKey={(record, index) => index}
        scroll={{x: 1024}}
        {...rest}
      />
    )
  }
;

export default observer(Table);
