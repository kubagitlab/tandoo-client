import React, {useState} from 'react';
import {Col, Dropdown, Form, Layout, Menu, Modal, Row} from 'antd';
import {MdAccountBox, MdAccountCircle, MdExitToApp, MdExpandMore, MdLock, MdPerson, MdSettings} from 'react-icons/md';
import {Link, useHistory} from 'react-router-dom';
import {useKeycloak} from '@react-keycloak/web';
import Button from './Button';
import appStore from '../../../store/AppStore';
import Input from './Input';
import adminRoutes from '../routes/admin';
import publicRoutes from '../routes/public';
import purchaserRoutes from '../routes/purchaser';
import supplierRoutes from '../routes/supplier';
import constants from '../../../common/constants';
import {observer} from 'mobx-react';

const Header = observer(() => {
  const [visible, setVisible] = useState(false);
  const {user, isAdmin, isPurchaser, isSupplier} = appStore;

  const {keycloak} = useKeycloak();
  const history = useHistory();

  const themeColor = () => {
    if (isAdmin) {return 'header-dark'}
    if (isPurchaser) {return 'header-purchaser'}
    if (isSupplier) {return 'header-supplier'}
    return 'header-light'
  }

  const renderUser = () => {
    if (user) {
      return (
        <Dropdown.Button
          type="primary"
          placement="bottomRight"
          icon={<MdExpandMore/>}
          overlay={() =>
            <Menu>
              <Menu.Item key="1" icon={<MdSettings/>}>
                {user.username || user.name || user.email}
              </Menu.Item>
              <Menu.Item key="2" icon={<MdExitToApp/>} onClick={() => {
                if (keycloak.authenticated) {
                  keycloak.logout();
                }
                appStore.logout();
                history.replace('/');
              }}>
                Exit
              </Menu.Item>
            </Menu>
          }>
          <MdPerson/>
        </Dropdown.Button>
      );
    }

    return (
      <Dropdown.Button
        type="primary"
        placement="bottomRight"
        icon={<MdExpandMore/>}
        onClick={() => keycloak.login()}
        overlay={() =>
          <Menu onClick={() => setVisible(true)}>
            <Menu.Item key="1" icon={<MdAccountCircle/>}>
              Администратор
            </Menu.Item>
          </Menu>
        }>
        Войти
      </Dropdown.Button>
    );
  };

  return (
    <Layout.Header className={themeColor()}>
        <Link to="/" className="logo" title="Тандоо - Закупки"/>
        <Row justify="space-between" align="middle">
          <Col xs={20}>
            <Menus user={user} theme={isAdmin && 'dark'}/>
          </Col>
          <Col xs={4} style={{textAlign: 'right'}}>
            {renderUser()}
          </Col>
        </Row>

        <LoginModal visible={visible}
                    onCancel={() => setVisible(false)}
                    onLogin={() => {
                      setVisible(false);
                      history.replace('/');
                    }}/>
    </Layout.Header>
  );
});

export default Header;

const Menus = ({user, theme}) => {
  const history = useHistory();
  let menus;
  if (user) {
    if ([constants.ROLE_SUPER_ADMIN, constants.ROLE_ADMIN, constants.ROLE_OPERATOR].includes(user.role_id)) {
      menus = adminRoutes.filter(r => r.roles.includes(user.role_id));
    } else if (user.role_id === constants.ROLE_PURCHASER) {
      menus = purchaserRoutes;
    } else if (user.role_id === constants.ROLE_SUPPLIER) {
      menus = supplierRoutes;
    }
    menus = menus.sort((a, b) => a.order - b.order);
  } else {
    menus = publicRoutes;
  }

  return (
    <Menu mode="horizontal" theme={theme}>
      {menus
        .filter(m => m.isHeaderMenu)
        .map(r => <Menu.Item key={r.path} onClick={() => history.push(r.path)}>{r.title}</Menu.Item>)
      }
    </Menu>
  );
};

const LoginModal = ({visible, onCancel, onLogin}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const login = () => {
    appStore.login(username, password).then(r => {
      onLogin();
    });
  };

  return (
    <Modal title="Войти" visible={visible} onCancel={onCancel} footer={null} width={320}>
      <Form>
        <Form.Item name="username">
          <Input prefix={<MdAccountBox/>} placeholder="Логин" type='email' onChange={v => setUsername(v)}/>
        </Form.Item>
        <Form.Item name="password">
          <Input prefix={<MdLock/>} type="password" placeholder="Пароль" onChange={v => setPassword(v)}/>
        </Form.Item>

        <Form.Item>
          <Button size="large" block disabled={!username || !password} onClick={login}>
            Войти
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

