import React, {useEffect, useState} from 'react';
import commonApi from '../../../../apis/common';
import {Col, Descriptions, Row} from 'antd';
import ImageView from '../ImageView';
import Button from "../Button";
import {formatMoney} from "../../../../common/utils";

export default ({id, onAdd}) => {

  const [product, setProduct] = useState(null);

  useEffect(() => {
    if (id) {
      commonApi.getProduct(id).then(r => setProduct(r.product));
    } else {
      setProduct(null);
    }
  }, [id]);

  if (!product) {
    return null;
  }

  return (
    <Row gutter={16}>
      <Col xs={24} sm={12} md={8} lg={8} xl={6}>
        <ImageView id={product.image} style={{width: '100%'}}/>
        {product.images && Array.isArray(product.images) && product.images.map(i =>
          <ImageView key={i} id={i} style={{width: 80}} thumbnail/>
        )}
      </Col>
      <Col xs={24} sm={12} md={16} lg={16} xl={18}>
        <Descriptions size="small" bordered column={1}>
          {!!product.barcode && <Descriptions.Item label='Штрих код'>{product.barcode}</Descriptions.Item>}
          {product.values.map((v, i) => <Descriptions.Item key={i} label={v.name}>{v.value}</Descriptions.Item>)}
          {product.avg_price &&
          <Descriptions.Item key="price" label="Средняя цена товара">
            {formatMoney(product.avg_price)}
          </Descriptions.Item>
          }
        </Descriptions>
        {onAdd &&
        <>
          <div style={{height: 10}}/>
          <Button type="primary" onClick={onAdd}>Добавить</Button>
        </>
        }
      </Col>
    </Row>
  );
}
