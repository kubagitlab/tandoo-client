import React from 'react';
import {Card, Col, Divider, Empty, Modal, Row} from 'antd';
import Product from './Product';
import ReactSelect from '../ReactSelect';
import Checkbox from '../Checkbox';
import ProductView from './ProductView';

export default props => {
  let {
    sections, section, onSectionSelect, categories, category, onOkgzSelect, filters,
    products, showProduct, productId, hideProduct, filterExists, onFilterClick, onAdd,
    disableSections
  } = props;

  return (
    <>
      <Row gutter={16}>
        <Col xs={24} sm={24} md={8} lg={5} xl={4}>
          <Card size="small">
            <ReactSelect
              placeholder="Выберите раздел"
              labelKey="name"
              valueKey="id"
              data={sections}
              value={section}
              onChange={onSectionSelect}
              disabled={disableSections}
            />
            <div style={{height: 10}}/>
            <ReactSelect
              disabled={!section}
              placeholder="Выберите категорию"
              labelKey="name"
              valueKey="id"
              data={categories}
              value={category}
              onChange={onOkgzSelect}
            />

            {filters && filters.dirs.map(d =>
              <div key={d.id} style={{marginBottom: 8}}>
                <Divider orientation="left" dashed style={{marginBottom: 3, fontSize: 11}}>{d.name}</Divider>
                {d.values.map(v =>
                  <div key={v.id}>
                    <Checkbox label={v.value}
                              checked={filterExists('dirs', d.id, v.id)}
                              onChange={val => onFilterClick('dirs', d.id, v.id)}/>
                  </div>
                )}
              </div>
            )}

            {filters && filters.specs.map(d =>
              <div key={d.id} style={{marginBottom: 8}}>
                <Divider orientation="left" dashed style={{marginBottom: 3, fontSize: 11}}>{d.name}</Divider>
                {d.values.map(v =>
                  <div key={v.id}>
                    <Checkbox label={v.name}
                              checked={filterExists('spec', d.id, v.id)}
                              onChange={() => onFilterClick('spec', d.id, v.id)}/>
                  </div>
                )}
              </div>
            )}

          </Card>
        </Col>
        <Col xs={24} sm={24} md={16} lg={19} xl={20}>
          {products.length > 0 ?
            <Row gutter={[10, 10]}>
              {products.map(p =>
                <Product key={p.id} product={p} onImageClick={() => showProduct(p)}
                         onAdd={onAdd && (() => onAdd(p.id))}/>
              )}
            </Row> :
            <Empty description='Нет данных'/>}
        </Col>

        <Modal visible={!!productId}
               onOk={hideProduct}
               onCancel={hideProduct}
               width="90%"
               style={{top: 20}}
               footer={null}
        >
          <ProductView id={productId} onAdd={onAdd && (() => onAdd(productId))}/>
        </Modal>

      </Row>
    </>
  )
}
