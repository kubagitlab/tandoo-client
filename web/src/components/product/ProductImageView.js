import React, {useEffect, useState} from 'react';
import {ImagePreview} from "../ImageView";

export default ({product, show, onClose}) => {
  if (!product) return null;

  const [ids, setIds] = useState([]);

  useEffect(() => {
    let aa = [];
    if (product.image) {
      aa.push(product.image);
    }

    if (product.images && Array.isArray(product.images) && product.images.length) {
      product.images.forEach(i => aa.push(i))
    }
    setIds(aa);
  }, []);

  return (
    <ImagePreview onClose={onClose} show={show} ids={ids}/>
  )

}
