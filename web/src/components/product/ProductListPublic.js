import React, {useEffect, useState} from 'react';
import commonApi from '../../../../apis/common';
import ProductListView from './ProductListView';

export default () => {
  const [sections, setSections] = useState([]);
  const [categories, setCategories] = useState([]);
  const [products, setProducts] = useState([]);
  const [section, setSection] = useState(null);
  const [category, setCategory] = useState(null);
  const [filters, setFilters] = useState(null);
  const [dirFilters, setDirFilters] = useState([]);
  const [specFilters, setSpecFilters] = useState([]);
  const [productId, setProductId] = useState(null);

  useEffect(() => {
    commonApi.getDir('DirSection').then((r) => {
      const s = r.sort((a, b) => a.name.localeCompare(b.name));
      setSections(s);
    });
  }, []);

  const getProducts = (okgz_id, filters) => {
    commonApi.getProductsByCategory({okgz_id, filters}).then((r) => {
      setProducts(r.products);
      setFilters(r.filters);
    });
  };

  const onSectionSelect = (selSection) => {
    setSection(selSection);
    setCategories([]);
    setCategory(null);
    setProducts([]);
    setFilters(null);
    if (selSection) {
      commonApi.getOkgzBySection(selSection.id).then((r) => {
        setCategories(r.list);
      });
    }
  };

  const onOkgzSelect = (selCategory) => {
    setCategory(selCategory);
    setProducts([]);
    setFilters(null);
    if (selCategory) {
      getProducts(selCategory.id);
    }
  };

  const showProduct = (product) => {
    setProductId(product.id);
  };

  const hideProduct = () => {
    setProductId(null);
  };

  const onFilterClick = (type, entity_id, value_id) => {
    let fs = type === 'dirs' ? [...dirFilters] : [...specFilters];
    const f = fs.find(item => item.entity_id === entity_id && item.value_id === value_id);
    if (f) {
      fs = fs.filter(item => !(item.entity_id === entity_id && item.value_id === value_id));
    } else {
      fs.push({entity_id, value_id});
    }
    let ff;
    if (type === 'dirs') {
      setDirFilters(fs);
      ff = {specs: specFilters, dirs: fs};

    } else {
      setSpecFilters(fs);
      ff = {specs: fs, dirs: dirFilters};
    }
    getProducts(category.id, ff);
  };

  const filterExists = (type, entity_id, value_id) => {
    let fs = type === 'dirs' ? [...dirFilters] : [...specFilters];
    const f = fs.find(item => item.entity_id === entity_id && item.value_id === value_id);
    return !!f;
  };

  let viewProps = {
    sections, section, onSectionSelect, categories, category, onOkgzSelect, filters,
    products, showProduct, productId, hideProduct, filterExists, onFilterClick
  };

  return (
    <ProductListView {...viewProps}/>
  );
}
;
