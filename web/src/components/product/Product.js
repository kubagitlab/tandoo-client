import React from 'react';
import {Card, Col, Typography} from 'antd';
import {useHistory} from 'react-router-dom';
import {observer} from 'mobx-react';
import Button from "../Button";
import ImageView from "../ImageView";
import appStore from "../../../../store/AppStore";
import compact from 'lodash/compact'
import {formatMoney} from "../../../../common/utils";

const {Text} = Typography;

export default observer((({product, onImageClick, onAdd}) => {
  const history = useHistory();

  const addProductSup = (productId) => {
    history.push('/sup/product/add/' + productId);
  };

  const addProductPur = (productId) => {
    onAdd && onAdd(productId)
  };

  return (
    <Col xs={12} sm={8} md={8} lg={6} xl={4}>
      <Card className="product"
            hoverable
            cover={<ImageView width={'100%'} id={product.image} thumbnail onClick={onImageClick}/>}
            actions={compact([
              appStore.isSupplier && (
                product.exist ?
                  <Text type="success">Уже добавлен</Text> :
                  <Button outline onClick={() => addProductSup(product.id)}>Добавить</Button>),
              appStore.isPurchaser && onAdd &&
              <Button outline onClick={() => addProductPur(product.id)}>Добавить</Button>
            ])}
      >
        {product.avg_price &&
        <p className="price">{formatMoney(product.avg_price)}</p>
        }
        <Text>Код: {product.barcode}</Text>
      </Card>
    </Col>
  );
}));
